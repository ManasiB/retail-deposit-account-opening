package au.com.voltbank.termdeposit.exception;

public class CustomerNotFoundException extends Exception {

  public CustomerNotFoundException(String voltId) {
    super(String.format("Customer record not found for voltId: %s", voltId));
  }
}
