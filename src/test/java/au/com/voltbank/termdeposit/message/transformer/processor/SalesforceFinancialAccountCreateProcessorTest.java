package au.com.voltbank.termdeposit.message.transformer.processor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import au.com.voltbank.termdeposit.dao.SavingsAccountApplicationRepository;
import au.com.voltbank.termdeposit.entity.SavingsAccountApplication;
import au.com.voltbank.termdeposit.exception.SalesforceArragementCreationException;
import au.com.voltbank.termdeposit.exception.SavingsAccountNotFoundException;
import au.com.voltbank.termdeposit.message.processor.SalesforceFinancialAccountCreateProcessor;
import au.com.voltbank.termdeposit.service.SalesforceArrangementService;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.queue.CloudQueueMessage;
import com.tyro.oss.randomdata.RandomString;
import java.util.Optional;
import org.apache.camel.CamelContext;
import org.apache.camel.Message;
import org.apache.camel.impl.DefaultExchange;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;


@RunWith(MockitoJUnitRunner.class)
public class SalesforceFinancialAccountCreateProcessorTest {

  @Autowired
  CamelContext context;

  @Mock
  private Message message;

  @InjectMocks
  private SalesforceFinancialAccountCreateProcessor processor;

  @Mock
  private SavingsAccountApplicationRepository savingsAccountApplicationRepository;

  @Mock
  private SalesforceArrangementService salesforceArrangementService;

  private DefaultExchange exchange;

  @Before
  public void setup() {

    exchange = new DefaultExchange(context);
    reset(savingsAccountApplicationRepository);
    reset(salesforceArrangementService);
    reset(message);
  }


  @Test
  public void shouldCallSFSavingsArrangementService() {
    String voltId = RandomString.randomAlphabeticString();
    Optional<SavingsAccountApplication> savingsAccountApplication = Optional.of(new SavingsAccountApplication());
    try{
      when(message.getBody()).thenReturn(new CloudQueueMessage(voltId));
      doReturn(savingsAccountApplication).when(savingsAccountApplicationRepository).findOneByVoltId(any());
      doReturn(null).when(salesforceArrangementService).createSavingsAccount(any());
      processor.createFinancialAccountForSalesforceCustomer(message, exchange);
      verify(salesforceArrangementService).createSavingsAccount(savingsAccountApplication.get());
    }
    catch (Exception ex){
      Assert.fail("Exception should not have occurred " + ex);
    }

  }

  @Test(expected = SavingsAccountNotFoundException.class )
  public void shouldThrowSavingsAccountNotFoundException() throws SavingsAccountNotFoundException, StorageException {
    String voltId = RandomString.randomAlphabeticString();
    Optional<SavingsAccountApplication> savingsAccountApplication = Optional.of(new SavingsAccountApplication());
    try {
      when(message.getBody()).thenReturn(new CloudQueueMessage(voltId));
      doReturn(Optional.empty()).when(savingsAccountApplicationRepository).findOneByVoltId(any());
      processor.createFinancialAccountForSalesforceCustomer(message, exchange);
      verify(salesforceArrangementService).createSavingsAccount(any());
    }
    catch (SalesforceArragementCreationException ex){
      Assert.fail("SalesforceArragementCreationException exception thrown, not expected " + ex);
    }

  }

  @Test(expected = NullPointerException.class)
  public void shouldThrowNullpointerExceptionIfQueueMessageisNull() throws Exception {
    processor.createFinancialAccountForSalesforceCustomer(null, exchange);
  }
}
