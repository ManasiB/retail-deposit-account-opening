package au.com.voltbank.termdeposit.mapper.t24;

import au.com.voltbank.Constants;
import au.com.voltbank.termdeposit.mapper.t24.annotations.T24Date;
import au.com.voltbank.termdeposit.mapper.t24.annotations.T24TDTerm;
import liquibase.util.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class T24QuirkMapper {

  private T24QuirkMapper() {}

  @T24Date
  public static String getT24Date(String date) {
    if (StringUtils.isEmpty(date)) return null;
    LocalDate localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern(Constants.DATE_FORMAT));
    return localDate.format(DateTimeFormatter.ofPattern(Constants.T24_DATE_FORMAT));
  }

  @T24Date
  public static String getT24Date(LocalDate date) {
    return date.format(DateTimeFormatter.ofPattern(Constants.T24_DATE_FORMAT));
  }

  @T24TDTerm
  public static String getT24Term(String term) {
    return new StringBuilder(term).append("M").toString();
  }
}
