package au.com.voltbank.termdeposit.exception;

public class AuthIdException extends RuntimeException {

  public AuthIdException(String message) {
    super(message);
  }

  public AuthIdException(String message, Throwable cause) {
    super(message, cause);
  }
}
