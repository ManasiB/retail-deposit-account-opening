package au.com.voltbank.termdeposit.service;

import au.com.voltbank.RetailDepositAccountOpeningApplication;
import au.com.voltbank.account.model.RetailTermDepositRateDto;
import au.com.voltbank.termdeposit.dao.RetailTermDepositRatesRepository;
import au.com.voltbank.termdeposit.entity.RetailTermDepositRate;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.lang.Collections;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailDepositAccountOpeningApplication.class)
@Transactional
public class RetailTermDepositRatesServiceTest {

  private static final String MOCK_INDEX_CODE = "test";
  private static final String MOCK_RATES_JSON =
      "[{\"id\":20222,\"percentRate\":2.0,\"termInMonths\":1,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20223,\"percentRate\":2.3,\"termInMonths\":2,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20224,\"percentRate\":2.6,\"termInMonths\":3,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20225,\"percentRate\":2.65,\"termInMonths\":4,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20226,\"percentRate\":2.7,\"termInMonths\":5,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20227,\"percentRate\":2.8,\"termInMonths\":6,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20228,\"percentRate\":2.8,\"termInMonths\":7,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20229,\"percentRate\":2.8,\"termInMonths\":8,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20230,\"percentRate\":2.85,\"termInMonths\":9,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20231,\"percentRate\":2.85,\"termInMonths\":10,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20232,\"percentRate\":2.85,\"termInMonths\":11,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20233,\"percentRate\":3.0,\"termInMonths\":12,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20234,\"percentRate\":3.05,\"termInMonths\":24,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20235,\"percentRate\":3.1,\"termInMonths\":36,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20236,\"percentRate\":3.15,\"termInMonths\":48,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20237,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20238,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20239,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20240,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20241,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20242,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20243,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20244,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20245,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20246,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20247,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20248,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20249,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20250,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20251,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20252,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20253,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20254,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20255,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20256,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20257,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20258,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20259,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20260,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20261,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20262,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20263,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20264,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20265,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20266,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20267,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20268,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20269,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20270,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20271,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20272,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20273,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20274,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20275,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20276,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20277,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20278,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20279,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20280,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20281,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20282,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20283,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20284,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"},{\"id\":20285,\"percentRate\":3.2,\"termInMonths\":60,\"indexCode\":\"01\",\"effectiveDateTime\":\"2019-01-13 22:41:54\"}]";
  private static final String ONE_THOUSAND_YEARS_LATER = "3019-01-15 00:00:01";
  private static final String ONE_THOUSAND_YEARS_AGO = "1000-01-15 00:00:01";
  @Autowired private RetailTermDepositRatesService ratesService;
  @Autowired private RetailTermDepositRatesRepository repository;

  @Before
  public void setUp() throws IOException {
    ObjectMapper mapper = new ObjectMapper();
    List<RetailTermDepositRate> insertRatesList =
        mapper.readValue(
            MOCK_RATES_JSON,
            mapper
                .getTypeFactory()
                .constructCollectionType(List.class, RetailTermDepositRate.class));
    insertRatesList.stream()
        .forEach(
            item -> {
              item.setId(null);
              item.setIndexCode(MOCK_INDEX_CODE);
              item.setEffectiveDateTime(ONE_THOUSAND_YEARS_LATER);
            });
    repository.saveAll(insertRatesList);

    List<RetailTermDepositRate> insertRatesList2 =
        mapper.readValue(
            MOCK_RATES_JSON,
            mapper
                .getTypeFactory()
                .constructCollectionType(List.class, RetailTermDepositRate.class));

    insertRatesList2.stream()
        .forEach(
            item -> {
              item.setId(null);
              item.setIndexCode(MOCK_INDEX_CODE);
              item.setEffectiveDateTime(ONE_THOUSAND_YEARS_AGO);
            });

    repository.saveAll(insertRatesList2);
  }

  @Test
  public void shouldNotReturnTheMockedRatesTimedThousandYearsLater() throws VoltCustomException {
    List<RetailTermDepositRateDto> ratesList = ratesService.fetchLatestRates();
    assertFalse(Collections.isEmpty(ratesList));
    assertNotEquals(MOCK_INDEX_CODE, ratesList.get(0).getIndexCode());
  }

  @Test
  public void shouldReturnTargetedRateTimedThousandYearsAgo() {
    RetailTermDepositRateDto dto = new RetailTermDepositRateDto();
    dto.setIndexCode(MOCK_INDEX_CODE);
    dto.setPercentRate(2.0);
    dto.setTermInMonths(1);
    assertTrue(ratesService.validateRate(dto));
  }

  @Test
  public void shouldReturnFalseAsNoTargetRateFound() {
    RetailTermDepositRateDto dto = new RetailTermDepositRateDto();
    dto.setIndexCode(MOCK_INDEX_CODE);
    dto.setPercentRate(20000.0);
    dto.setTermInMonths(11111111);
    assertFalse(ratesService.validateRate(dto));
  }

  @Test
  public void identicalRatesAreEqual() {
    RetailTermDepositRate rate1 = new RetailTermDepositRate();
    rate1.setId(1L);
    rate1.setPercentRate(2.0);
    rate1.setTermInMonths(1);

    RetailTermDepositRate rate2 = new RetailTermDepositRate();
    rate2.setId(1L);
    rate2.setPercentRate(2.0);
    rate2.setTermInMonths(1);

    assertTrue(rate1.equals(rate2));
  }

  @Test
  public void differentRatesAreNotEqual() {
    RetailTermDepositRate rate1 = new RetailTermDepositRate();
    rate1.setId(1L);
    rate1.setPercentRate(2.0);
    rate1.setTermInMonths(1);

    RetailTermDepositRate rate2 = new RetailTermDepositRate();
    rate2.setId(2L);
    rate2.setPercentRate(2.0);
    rate2.setTermInMonths(1);

    // Id different
    assertFalse(rate1.equals(rate2));

    // percentRate different
    rate2.setPercentRate(rate1.getPercentRate() + 0.5);
    assertFalse(rate1.equals(rate2));

    // term different
    rate2.setPercentRate(rate1.getPercentRate());
    rate2.setTermInMonths(rate1.getTermInMonths() + 1);
    assertFalse(rate1.equals(rate2));
  }
}
