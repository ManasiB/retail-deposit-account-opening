package au.com.voltbank.termdeposit.service;

import au.com.voltbank.termdeposit.dto.AzureTokenRequestParam;
import au.com.voltbank.termdeposit.dto.AzureTokenResponse;
import au.com.voltbank.termdeposit.exception.AzureActiveDirectoryAccessTokenException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class AzureActiveDirectoryService {

  protected static final String AZURE_TOKEN_ENDPOINT =
      "https://login.microsoftonline.com/62c58725-7947-4a6b-b212-bf29cdabef7b/oauth2/token";

  @Autowired private AzureTokenRequestParam azureTokenRequestParam;

  @Autowired private RestTemplate secureRestTemplate;

  public String getAccessToken(String resource) {
      try {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("grant_type", azureTokenRequestParam.getGrantType());
        map.add("client_id", azureTokenRequestParam.getClientId());
        map.add("client_secret", azureTokenRequestParam.getClientSecret());
        map.add("resource", resource);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        return secureRestTemplate
            .postForEntity(AZURE_TOKEN_ENDPOINT, request, AzureTokenResponse.class)
            .getBody()
            .getAccessToken();
      } catch (Exception e) {
      throw new AzureActiveDirectoryAccessTokenException(azureTokenRequestParam.getClientId(), e);
    }
  }
}
