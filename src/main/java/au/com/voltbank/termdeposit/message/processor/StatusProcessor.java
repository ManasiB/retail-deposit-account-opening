package au.com.voltbank.termdeposit.message.processor;

import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.queue.CloudQueueMessage;

import au.com.voltbank.salesforce.arrangement.model.SuccessResponseDto;
import au.com.voltbank.termdeposit.dto.SalesforceUpdateDto;
import au.com.voltbank.termdeposit.exception.SalesforceArragementCreationException;
import au.com.voltbank.termdeposit.exception.SalesforceFinancialAccountException;
import au.com.voltbank.termdeposit.service.SalesforceFinancialAccountService;

import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component



public class StatusProcessor {
	@Autowired
	SalesforceFinancialAccountService salesForceFinService;

	public void RetrieveMessageFromAccountManagement(Message in, Exchange exchange)
			throws StorageException, SalesforceFinancialAccountException {
		CloudQueueMessage cloudQueueMessage = (CloudQueueMessage) in.getBody();
		if (cloudQueueMessage != null) {
			System.out.println(cloudQueueMessage.getMessageContentAsString());
			if (cloudQueueMessage.getMessageContentAsString() != null) {
				System.out.println("Message is not Empty");
				/*
				 * SuccessResponseDto successResponseDto = new SuccessResponseDto();
				 * successResponseDto = salesForceFinService.updateSalesforce();
				 * System.out.println(successResponseDto.getSuccess() + "::::" +
				 * successResponseDto.getId());
				 */
				salesForceFinService.updateSalesforce();
			}

		}
	}
}
