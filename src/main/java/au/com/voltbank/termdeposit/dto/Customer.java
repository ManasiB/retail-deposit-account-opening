package au.com.voltbank.termdeposit.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Builder(builderClassName = "CustomerBuilder", toBuilder = true)
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Data
public class Customer {
  // TODO immutable
  private String status;
  private String t24CustomerId;
  private String voltId;
}
