package au.com.voltbank.termdeposit.controller;

import au.com.voltbank.account.model.TransactionAccountListDto;
import au.com.voltbank.sis.model.VoltAuthIdPairDto;
import au.com.voltbank.sis.model.VoltT24IdPairDto;
import au.com.voltbank.termdeposit.exception.T24IdException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.exception.VoltIdException;
import au.com.voltbank.termdeposit.service.AccountListService;
import au.com.voltbank.termdeposit.service.SystemIdStoreService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Slf4j
@RestController
public class AccountListController {

  @Autowired private AccountListService accountListService;

  @Autowired private SystemIdStoreService systemIdStoreService;

  @GetMapping("/accounts")
  public ResponseEntity<TransactionAccountListDto> getTransactionListForAnAccount(
      @RequestParam Map<String, String> requestParams, Authentication authentication)
      throws VoltCustomException, VoltIdException, T24IdException {
	  System.out.println("Name::" + authentication+ "      " +authentication.getName());
    VoltAuthIdPairDto voltAuthIdPairDto = systemIdStoreService.getVoltId(authentication.getName());
    VoltT24IdPairDto t24CustomerIdForVoltId =
        systemIdStoreService.getT24CustomerIdForVoltId(voltAuthIdPairDto.getVoltId());
    TransactionAccountListDto transactionAccountListDto =
        accountListService.getAccountsList(requestParams, t24CustomerIdForVoltId.getT24Id());
    log.info("Successfully fetched Accounts list for the given account");
    return ResponseEntity.status(HttpStatus.OK).body(transactionAccountListDto);
  }
}
