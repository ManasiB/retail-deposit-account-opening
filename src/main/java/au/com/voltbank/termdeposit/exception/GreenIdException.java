package au.com.voltbank.termdeposit.exception;

public class GreenIdException extends Exception {

  public GreenIdException(String message) {
    super(message);
  }

  public GreenIdException(String message, Throwable cause) {
    super(message, cause);
  }
}
