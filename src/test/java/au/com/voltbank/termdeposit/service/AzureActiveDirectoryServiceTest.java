package au.com.voltbank.termdeposit.service;

import au.com.voltbank.RetailDepositAccountOpeningApplication;
import au.com.voltbank.termdeposit.dto.AzureTokenResponse;
import au.com.voltbank.termdeposit.exception.AzureActiveDirectoryAccessTokenException;
import com.tyro.oss.randomdata.RandomString;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import static au.com.voltbank.termdeposit.service.AzureActiveDirectoryService.AZURE_TOKEN_ENDPOINT;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailDepositAccountOpeningApplication.class)
public class AzureActiveDirectoryServiceTest {

  private static final String RESOURCE = "00000002-0000-0000-c000-000000000000";

  @Autowired private AzureActiveDirectoryService azureActiveDirectoryService;

  @MockBean(name = "secureRestTemplate")
  private RestTemplate secureRestTemplate;

  @Before
  public void setUp() {
    reset(secureRestTemplate);
  }

  @Test
  public void shouldCallAzureADToGetAccessToken() {
    // setup
    AzureTokenResponse expectedAzureTokenResponse = new AzureTokenResponse();
    expectedAzureTokenResponse.setAccessToken(RandomString.randomUUID());

    ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);

    when(secureRestTemplate.postForEntity(
            eq(AZURE_TOKEN_ENDPOINT), httpEntityCaptor.capture(), eq(AzureTokenResponse.class)))
        .thenReturn(new ResponseEntity<>(expectedAzureTokenResponse, HttpStatus.OK));

    // test
    String actualAccessToken = azureActiveDirectoryService.getAccessToken(RESOURCE);

    // verify
    assertThat(actualAccessToken, is(expectedAzureTokenResponse.getAccessToken()));

    HttpEntity httpEntity = httpEntityCaptor.getValue();
    HttpHeaders headers = httpEntity.getHeaders();
    assertThat(headers.getContentType(), is(APPLICATION_FORM_URLENCODED));

    MultiValueMap<String, String> requestBody =
        (MultiValueMap<String, String>) httpEntity.getBody();
    assertThat(requestBody.getFirst("grant_type"), is("client_credentials"));
    assertThat(requestBody.getFirst("client_id"), not(nullValue()));
    assertThat(requestBody.getFirst("client_secret"), not(nullValue()));
    assertThat(requestBody.getFirst("resource"), is(RESOURCE));
  }

  @Test
  public void shouldThrowExceptionWhenCannotRetrieveAccessToken() {
    // setup
    when(secureRestTemplate.postForEntity(
            eq(AZURE_TOKEN_ENDPOINT), any(HttpEntity.class), eq(AzureTokenResponse.class)))
        .thenThrow(new RuntimeException("BOOM!"));

    AzureActiveDirectoryAccessTokenException expectedException = null;

    // test
    try {
      azureActiveDirectoryService.getAccessToken(RESOURCE);
    } catch (Exception e) {
      expectedException = (AzureActiveDirectoryAccessTokenException) e;
    }

    // verify
    assertThat(
        expectedException.getMessage(),
        containsString("Could not get Azure Active Directory access token for client_id:"));
  }
}
