package au.com.voltbank.termdeposit.service;

import static org.apache.commons.lang3.StringUtils.join;

import au.com.voltbank.salesforce.arrangement.model.ArrangementPostDto;
import au.com.voltbank.salesforce.arrangement.model.SavingsArrangementDataDto;
import au.com.voltbank.salesforce.arrangement.model.SavingsArrangementDataDto.FinServOwnershipCEnum;
import au.com.voltbank.salesforce.arrangement.model.SuccessResponseDto;
import au.com.voltbank.sis.model.VoltSalesforceIdPairDto;
import au.com.voltbank.t24.account.AccountDetailBodyDto;
import au.com.voltbank.t24.account.TDAccountDetailBodyDto;
import au.com.voltbank.termdeposit.dto.SalesforceUpdateDto;
import au.com.voltbank.termdeposit.entity.SavingsAccountApplication;
import au.com.voltbank.termdeposit.entity.TermDepositAccountApplication;
import au.com.voltbank.termdeposit.exception.SalesforceArragementCreationException;
import au.com.voltbank.termdeposit.exception.SalesforceFinancialAccountException;
import au.com.voltbank.termdeposit.util.RetailDepositAccountOpeningUtil;
import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SalesforceArrangementService {

	private static final String RECORD_TYPE_ID = "0126F000001o1N7QAI";
	final String SF_ARRANGEMENT_ENDPOINT = "/arrangement";
	final String SF_ARRANGEMENT_CREATION_ERROR = "Could not create Salesforce arrangement for VoltId: %s";
	final String SF_ID_NOTFOUND = "Could not find SalesforceId for VoltId: %s";
	final String SF_FINANCIAL_ACCOUNT_ENDPOINT = "/services/data/v45.0/sobjects/FinServ_FinancialAccount_c";
	final String SF_FINANCIAL_ACCOUNT_ERROR = "Could not update Salesforce for account: %s";

	@Value("${SALESFORCE_HOST}")
	private String salesforceHost;

	@Value("${salesforce.customer.api.resource.id}")
	private String resourceId;

	@Autowired
	private RestTemplate secureRestTemplate;

	@Autowired
	private SystemIdStoreService systemIdStoreService;

	@Autowired
	private TermDepositAccountApplicationService termDepositAccountApplicationService;

	@Autowired
	private SavingsAccountService savingsAccountService;

	@Autowired
	private AzureActiveDirectoryService azureActiveDirectoryService;

	public SuccessResponseDto create(TermDepositAccountApplication termDepositAccountApplication)
			throws SalesforceArragementCreationException {

		try {
			VoltSalesforceIdPairDto salesforceIdDto = systemIdStoreService
					.getSalesforceId(termDepositAccountApplication.getUserId())
					.orElseThrow(() -> new SalesforceArragementCreationException(
							String.format(SF_ID_NOTFOUND, termDepositAccountApplication.getUserId())));

			TDAccountDetailBodyDto termDepositAccountDetails = termDepositAccountApplicationService
					.getTermDepositAccountDetails(termDepositAccountApplication);

			ArrangementPostDto arrangementPostDto = new ArrangementPostDto()
					.customerID(salesforceIdDto.getSalesforceId()).accountNumber(termDepositAccountDetails.getAccount())
					.bsbNumber(termDepositAccountDetails.getSortcode())
					.principalBalanceInWholeDollars(termDepositAccountDetails.getPrincipalBalance())
					.maturityDate(termDepositAccountDetails.getMaturityDate())
					.openDate(termDepositAccountDetails.getStartDate())
					.interestRate(new BigDecimal(termDepositAccountDetails.getInterestRate()))
					.linkedAccountName(termDepositAccountApplication.getLinkedAccountName())
					.linkedAccountBSB(termDepositAccountApplication.getLinkedAccountBSB())
					.linkedAccountNumber(termDepositAccountApplication.getLinkedAccountNumber());

			HttpEntity<ArrangementPostDto> request = new HttpEntity<>(arrangementPostDto, getHttpHeaders());

			return secureRestTemplate
					.postForEntity(join(salesforceHost, SF_ARRANGEMENT_ENDPOINT), request, SuccessResponseDto.class)
					.getBody();
		} catch (Exception e) {
			throw new SalesforceArragementCreationException(
					String.format(SF_ARRANGEMENT_CREATION_ERROR, termDepositAccountApplication.getUserId()), e);
		}
	}

	public SuccessResponseDto createSavingsAccount(SavingsAccountApplication savingsAccountApplication)
			throws SalesforceArragementCreationException {

		try {
			VoltSalesforceIdPairDto salesforceIdDto = systemIdStoreService
					.getSalesforceId(savingsAccountApplication.getVoltId())
					.orElseThrow(() -> new SalesforceArragementCreationException(
							String.format(SF_ID_NOTFOUND, savingsAccountApplication.getVoltId())));

			AccountDetailBodyDto accountDetails = savingsAccountService
					.getSavingsAccountDetails(savingsAccountApplication);

			SavingsArrangementDataDto savingsArrangementPostDto = new SavingsArrangementDataDto()
					.recordTypeId(RECORD_TYPE_ID).finServOwnershipC(FinServOwnershipCEnum.INDIVIDUAL)
					.finServPrimaryOwnerC(salesforceIdDto.getSalesforceId())
					.accountStatusC(accountDetails.getArrangementStatus())
					.finServOpenDateC(RetailDepositAccountOpeningUtil
							.convertContractDateToFinServOpenDate(accountDetails.getContractDate()))
					.productNameC(accountDetails.getProductId())
					.finServInterestRateC(Double.valueOf(accountDetails.getCreditInterestRate()))
					.temenosIdC(accountDetails.getArrangementId()).bankAccountNumberC(accountDetails.getAcctNumber())
					.accountTypeC(accountDetails.getAccountType()).name(accountDetails.getAccountTitle())
					.finServDescriptionC(accountDetails.getDescription())
					.finServCurrentPostedBalanceC(String.valueOf(accountDetails.getCurrentBalance()))
					.availableBalanceC(String.valueOf(accountDetails.getAvailableBalance()))
					.finServBalanceC(String.valueOf(accountDetails.getBalance()))
					.finServNicknameC(accountDetails.getNickname()).bsBC(accountDetails.getSortCode());

			HttpEntity<SavingsArrangementDataDto> request = new HttpEntity<>(savingsArrangementPostDto,
					getHttpHeaders());

			return secureRestTemplate
					.postForEntity(join(salesforceHost, SF_ARRANGEMENT_ENDPOINT), request, SuccessResponseDto.class)
					.getBody();
		} catch (Exception e) {
			throw new SalesforceArragementCreationException(
					String.format(SF_ARRANGEMENT_CREATION_ERROR, savingsAccountApplication.getVoltId()), e);
		}
	}

	public void updatAccount(TermDepositAccountApplication termDepositAccountApplication)
		      throws SalesforceFinancialAccountException {

	    try {
	      VoltSalesforceIdPairDto salesforceIdDto = systemIdStoreService
	          .getSalesforceId(termDepositAccountApplication.getUserId()).orElseThrow(
	              () -> new SalesforceArragementCreationException(String
	                  .format(SF_ID_NOTFOUND,
	                      termDepositAccountApplication.getUserId())));


	      ArrangementPostDto arrangementPostDto = new ArrangementPostDto()
	          .customerID(salesforceIdDto.getSalesforceId())
	          .fundingStatus(termDepositAccountApplication.isFundingStatus());

	      HttpEntity<ArrangementPostDto> request = new HttpEntity<>(arrangementPostDto,
	          getHttpHeaders());

	      String url=salesforceHost+SF_FINANCIAL_ACCOUNT_ENDPOINT; secureRestTemplate.put(url,
	    			 request);
	    			  
	    return secureRestTemplate.postForEntity(join(salesforceHost,
	    			  SF_FINANCIAL_ACCOUNT_ENDPOINT), request).getBody();
	    } catch (Exception e) {
	      throw new SalesforceArragementCreationException(String
	          .format(SF_ARRANGEMENT_CREATION_ERROR,
	              termDepositAccountApplication.getUserId()), e);
	    }
	}

		/*
		 * try { System.out.println("updatingsalesforce");
		 * salesforceDto.setAccountNumber("100000058");
		 * salesforceDto.setAccountStatus("Active"); HttpEntity<SalesforceUpdateDto>
		 * request = new HttpEntity<>(salesforceDto, getHttpHeaders()); final String
		 * url=salesforceHost+SF_FINANCIAL_ACCOUNT_ENDPOINT; secureRestTemplate.put(url,
		 * request);
		 * 
		 * return secureRestTemplate.postForEntity(join(salesforceHost,
		 * SF_FINANCIAL_ACCOUNT_ENDPOINT), request, SuccessResponseDto.class).getBody();
		 * 
		 * 
		 * } catch (Exception e) { throw new
		 * SalesforceFinancialAccountException(String.format(SF_FINANCIAL_ACCOUNT_ERROR)
		 * , e); }
		 */
	

	private HttpHeaders getHttpHeaders() {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("Authorization", "Bearer " + azureActiveDirectoryService.getAccessToken(resourceId));
		return httpHeaders;
	}

}
