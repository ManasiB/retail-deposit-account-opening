package au.com.voltbank.termdeposit.service;

import au.com.voltbank.account.model.TransactionAccountListDto;
import au.com.voltbank.termdeposit.exception.VoltCustomException;

import java.util.Map;

/** Created by pavankumarjoshi on 6/6/19. */
public interface AccountList {

  TransactionAccountListDto getAccountsList(Map<String, String> requestParams, String t24Id)
      throws VoltCustomException;
}
