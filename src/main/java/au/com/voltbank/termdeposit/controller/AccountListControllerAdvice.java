package au.com.voltbank.termdeposit.controller;

import au.com.voltbank.termdeposit.dto.ErrorResponse;
import au.com.voltbank.termdeposit.exception.T24IdException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.exception.VoltIdException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import static au.com.voltbank.Constants.ERROR_SYSTEM;
import static au.com.voltbank.Constants.ERROR_VOLT_ID_NOT_FOUND;

@Slf4j
@ControllerAdvice(assignableTypes = AccountListController.class)
public class AccountListControllerAdvice {

  @ExceptionHandler
  public ResponseEntity<ErrorResponse> handle(VoltIdException e) {
    log.error(e.getMessage(), e);
    return ResponseEntity.badRequest().body(new ErrorResponse(ERROR_VOLT_ID_NOT_FOUND));
  }

  @ExceptionHandler
  public ResponseEntity<ErrorResponse> handle(VoltCustomException e) {
    log.error(e.getMessage(), e);
    return ResponseEntity.badRequest().body(new ErrorResponse(ERROR_VOLT_ID_NOT_FOUND));
  }

  @ExceptionHandler
  public ResponseEntity<ErrorResponse> handle(T24IdException e) {
    log.error(e.getMessage(), e);
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(ERROR_SYSTEM));
  }

  @ExceptionHandler
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ErrorResponse handle(Exception exception) {
    log.error(ERROR_SYSTEM, exception);
    return new ErrorResponse(ERROR_SYSTEM);
  }
}
