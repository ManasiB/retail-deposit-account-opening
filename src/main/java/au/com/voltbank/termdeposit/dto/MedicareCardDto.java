package au.com.voltbank.termdeposit.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder(builderClassName = "MedicareDtoBuilder")
@JsonDeserialize(builder = MedicareCardDto.MedicareDtoBuilder.class)
@NoArgsConstructor
@AllArgsConstructor
public class MedicareCardDto {

  private String cardNumber;
  private LocalDate validTo;
  private String fullNameAsOnCard;
  private String individualReferenceNumber;
  private String colour;

  @JsonPOJOBuilder(withPrefix = "")
  public static class MedicareDtoBuilder {
    // Lombok will add constructor, setters, build method
  }
}
