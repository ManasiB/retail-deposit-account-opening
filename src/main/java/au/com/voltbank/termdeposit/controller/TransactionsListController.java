package au.com.voltbank.termdeposit.controller;

import au.com.voltbank.account.model.TransactionListDto;
import au.com.voltbank.termdeposit.exception.TransactionAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.service.TransactionListService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Map;

@Slf4j
@RestController
public class TransactionsListController {

	public static final String PATH_ACCOUNTS_ACCOUNT_NUMBER_TRANSACTIONS =
			"/accounts/{accountNumber}/transactions";
	@Autowired
	private TransactionListService transactionsListService;

	@GetMapping(PATH_ACCOUNTS_ACCOUNT_NUMBER_TRANSACTIONS)
	public ResponseEntity<TransactionListDto> getTransactionListForAnAccount(
			@PathVariable("accountNumber") String accountNumber,
			@RequestParam Map<String, String> requestParams, Principal principal)
			throws VoltCustomException, TransactionAccountNotFoundException {

		TransactionListDto transactions = transactionsListService.getTransactions(principal.getName(),
				accountNumber, requestParams);
		log.info("Successfully fetched transactions list for the given account");
		return ResponseEntity.status(HttpStatus.OK).body(transactions);
	}
}
