#!/usr/bin/env bash

### Constants ###
EXEC_ON_IMAGE='g1vimgacr.azurecr.io/voltbank/build:fat-stable-latest'

### Environment variables
export SPRING_PROFILES_ACTIVE=build

### Functions ###
f () {
    error_code=$?
    echo "Error ${error_code}"
    echo "The command executing at the time of the error was"
    echo "$BASH_COMMAND"
    echo "on line ${BASH_LINENO[0]}"
    exit ${error_code}
}
trap f ERR

fail_if_not_set () {
  if [ -z "$2" ]; then echo "🛑 Error: Environment variable not set! - $1"; exit 1; fi
}

### Script ###

echo "🤓 Environment variables used by this script:"
echo "######################################################################"
echo "DOCKER_IMAGE_NAME  : '${DOCKER_IMAGE_NAME:-}'"
echo "DOCKER_IMAGE_TAG   : '${DOCKER_IMAGE_TAG:-}'"

echo
echo "######################################################################"
echo "#  🧐 Checking configuration..."
echo "######################################################################"
fail_if_not_set 'DOCKER_IMAGE_NAME' "${DOCKER_IMAGE_NAME}"
fail_if_not_set 'DOCKER_IMAGE_TAG' "${DOCKER_IMAGE_TAG}"
echo "✅ All required environment variables set."
echo

echo
echo "######################################################################"
echo "#  Pulling the build image..."
echo "#  ${EXEC_ON_IMAGE}"
echo "######################################################################"
az login --service-principal -u "${DOCKER_USERNAME}" -p "${DOCKER_PASSWORD}" --tenant voltbank.com.au
az acr login --name "${DOCKER_REGISTRY}"
#export DOCKER_CONTENT_TRUST=1
docker pull "${EXEC_ON_IMAGE}" \
  || { echo "🛑 Error: Failed to pull docker image for build!"; exit 1; }

#export DOCKER_CONTENT_TRUST=

echo
echo "######################################################################"
echo "#  Executing the build in Docker container..."
echo "#  ${EXEC_ON_IMAGE}"
echo "######################################################################"
set | grep '^[A-Z].*' \
  | grep -v '^ANDROID' \
  | grep -v '^ANT' \
  | grep -v '^BASH' \
  | grep -v '^CHROME_BIN' \
  | grep -v '^COMMON_TESTRESULTSDIRECTORY' \
  | grep -v '^CONDA' \
  | grep -v '^DIRSTACK' \
  | grep -v '^DOTNET' \
  | grep -v '^GOROOT' \
  | grep -v '^EUID' \
  | grep -v '^GRADLE' \
  | grep -v '^HOME' \
  | grep -v '^HOST' \
  | grep -v '^IFS' \
  | grep -v '^INPUT_ARGUMENTS' \
  | grep -v '^JAVA' \
  | grep -v '^LEIN' \
  | grep -v '^M2' \
  | grep -v '^MACHTYPE' \
  | grep -v '^MSDEPLOY' \
  | grep -v '^OPTERR' \
  | grep -v '^OPTIND' \
  | grep -v '^PATH' \
  | grep -v '^PIPE' \
  | grep -v '^PPID' \
  | grep -v '^SHELL' \
  | grep -v '^SYSTEM' \
  | grep -v '^UID' \
  | grep -v '^USER' \
  | grep -v '^VCPKG' \
  | grep -v '^VSTS' \
  > env_vars.txt
docker run -v $(pwd):/work --net=host --env-file=env_vars.txt "${EXEC_ON_IMAGE}" \
  || { echo "🛑 Error: Failed to execute build on a docker container!"; exit 1; }

if [ -f "Dockerfile" ]; then

  echo
  echo "######################################################################"
  echo "#  Building Docker image..."
  echo "#  ${DOCKER_IMAGE_NAME}"
  echo "######################################################################"
  docker build -t "${DOCKER_IMAGE_NAME}" . \
    || { echo "🛑 Error: Failed to build docker image!"; exit 1; }

  echo
  echo "######################################################################"
  echo "#  Tagging Docker image..."
  echo "#  ${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG}"
  echo "######################################################################"
  docker tag "${DOCKER_IMAGE_NAME}" "${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG}" \
    || { echo "🛑 Error: Failed to tag docker image!"; exit 1; }

  echo
  echo "######################################################################"
  echo "#  Pushing Docker image..."
  echo "#  ${DOCKER_IMAGE_NAME}"
  echo "######################################################################"
  docker push "${DOCKER_IMAGE_NAME}" \
    || { echo "🛑 Error: Failed to push docker image!"; exit 1; }

  echo
  echo "######################################################################"
  echo "#  Pushing tagged Docker image..."
  echo "#  ${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG}"
  echo "######################################################################"
  docker push "${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG}" \
    || { echo "🛑 Error: Failed to push tagged docker image!"; exit 1; }

fi


