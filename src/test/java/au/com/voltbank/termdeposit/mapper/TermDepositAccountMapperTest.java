package au.com.voltbank.termdeposit.mapper;

import au.com.voltbank.t24.account.T24CreateDepositAccountDto;
import au.com.voltbank.termdeposit.entity.TermDepositAccountApplication;
import au.com.voltbank.termdeposit.mapper.t24.T24QuirkMapper;
import au.com.voltbank.termdeposit.util.CannedData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class TermDepositAccountMapperTest {

  @Test
  public void should_map_entity_to_dto() {
    TermDepositAccountApplication entity = CannedData.getDepositAccountApplication();
    T24CreateDepositAccountDto dto = TermDepositAccountMapper.toDto(entity);
    assertEquals(
        dto.getProductId(),
        TermDepositAccountMapper.productIdLookUpTable.get(entity.getIndexCode()));
    assertEquals(dto.getEffectiveDate(), T24QuirkMapper.getT24Date(entity.getStartDate()));
    assertEquals(dto.getActivityId(), "DEPOSITS-NEW-ARRANGEMENT");
    assertEquals(dto.getParties().get(0).getPartyId(), entity.getT24CustomerId());
    assertEquals(dto.getCommitment().getTerm(), entity.getTermInMonths() + 'M');
    assertEquals(dto.getCommitment().getAmount(), entity.getAmountInWholeDollars());
    assertEquals(dto.getCurrencyId(), "AUD");
  }
}
