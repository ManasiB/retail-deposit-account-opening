package au.com.voltbank.termdeposit.service;

import au.com.voltbank.Constants;
import au.com.voltbank.RetailDepositAccountOpeningApplication;
import au.com.voltbank.account.model.TransactionAccountApplicationDataDto;
import au.com.voltbank.customer.model.CustomerBankingCreateResponseDto;
import au.com.voltbank.termdeposit.corebanking.service.T24AccountService;
import au.com.voltbank.t24.T24ResponseBody;
import au.com.voltbank.t24.account.AccountDetailBodyDto;
import au.com.voltbank.termdeposit.dao.TransactionAccountApplicationRepository;
import au.com.voltbank.termdeposit.dto.Customer;
import au.com.voltbank.termdeposit.entity.TransactionAccountApplication;
import au.com.voltbank.termdeposit.exception.TransactionAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.util.CannedData;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

import static com.tyro.oss.randomdata.RandomString.randomNumericString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailDepositAccountOpeningApplication.class)
public class TransactionAccountServiceTest {

  @Autowired private TransactionAccountService transactionAccountService;

  @Autowired
  private TransactionAccountApplicationRepository transactionAccountApplicationRepository;

  @MockBean private CustomerService customerService;

  @MockBean private T24AccountService t24AccountService;

  private TransactionAccountApplicationDataDto transactionAccountApplicationDataDto;

  private Customer customer;

  private CustomerBankingCreateResponseDto customerBankingDetailsDto;

  @Before
  public void setup() {
    transactionAccountApplicationDataDto = CannedData.getTransactionAccountApplicationDataDto();
    customer = CannedData.getCannedCustomerEntity();
    customerBankingDetailsDto = CannedData.getCustomerBankingDetailsDto();
  }

  @After
  public void reset() {
    Mockito.reset(customerService, t24AccountService);
  }

  @Test
  public void shouldCreateTransactionAccount() throws VoltCustomException, IOException {
    // setup
    String voltId = randomNumericString(8);

    when(customerService.createt24CustomerIfnotExist(voltId)).thenReturn(customerBankingDetailsDto);

    T24ResponseBody expectedResponse =
        new T24ResponseBody("mockHeader", CannedData.getCreateAccountResponseBody(), null);

    when(t24AccountService.createTransactionAccount(any(TransactionAccountApplication.class)))
        .thenReturn(new ResponseEntity<>(expectedResponse, HttpStatus.CREATED));

    // test
    TransactionAccountApplication result =
        transactionAccountService.create(transactionAccountApplicationDataDto, voltId);

    // verify
    CustomerBankingCreateResponseDto customerBankingDetailsDto =
        customerService.createt24CustomerIfnotExist(voltId);
    assertThat(customer, notNullValue());

    TransactionAccountApplication accountApplication =
        transactionAccountApplicationRepository.findOneByVoltId(voltId).get();

    assertThat(accountApplication.getT24CustomerId(), is(result.getT24CustomerId()));
    assertThat(accountApplication.getArrangementId(), is(result.getArrangementId()));
  }

  @Test
  public void shouldFetchAccountDetailsCreated()
      throws VoltCustomException, IOException, TransactionAccountNotFoundException {
    // setup
    List<LinkedHashMap<String, String>> accountDetails =
        CannedData.getAccountDetailsBodyDtoLinkedHashMap();
    AccountDetailBodyDto expectedResult = CannedData.getAccountDetailsBodyDto().get(0);

    String t24CustomerId = customer.getT24CustomerId();

    when(t24AccountService.getAccounts(t24CustomerId))
        .thenReturn(
            new ResponseEntity<>(
                new T24ResponseBody("mockHeader", accountDetails, null), HttpStatus.OK));

    TransactionAccountApplication createResult =
        new TransactionAccountApplication()
            .toBuilder()
            .arrangementId(accountDetails.get(0).get("arrangementId"))
            .t24CustomerId(t24CustomerId)
            .build();

    // test
    AccountDetailBodyDto getResultDto = transactionAccountService.getAccountDetails(createResult);

    // verify
    assertThat(getResultDto, is(expectedResult));
  }

  @Test(expected = VoltCustomException.class)
  public void shouldThrowVoltCustomerNotFoundActiveCustomer() throws VoltCustomException {
    // setup
    String voltId = randomNumericString(8);

    when(customerService.createt24CustomerIfnotExist(voltId))
        .thenThrow(
            new VoltCustomException(
                Constants.ERROR_CUSTOMER_NOT_FOUND,
                "Can not create account for an inactive customer - voltId:" + voltId,
                HttpStatus.NOT_FOUND.value()));

    // test
    transactionAccountService.create(
        new ModelMapper()
            .map(transactionAccountApplicationDataDto, TransactionAccountApplicationDataDto.class),
        voltId);
  }

  @Test(expected = TransactionAccountNotFoundException.class)
  public void shouldThrowTransactionAccountNotFoundExceptionIfSavingsAccountNotFound()
      throws VoltCustomException, TransactionAccountNotFoundException {
    // setup
    String t24CustomerId = randomNumericString(8);

    when(t24AccountService.getAccounts(t24CustomerId))
        .thenReturn(
            new ResponseEntity<>(
                new T24ResponseBody("mockHeader", Collections.emptyList(), null), HttpStatus.OK));

    TransactionAccountApplication transactionAccountApplication =
        new TransactionAccountApplication();
    transactionAccountApplication.setT24CustomerId(t24CustomerId);

    TransactionAccountNotFoundException expected = null;
    // test
    transactionAccountService.getAccountDetails(transactionAccountApplication);
  }
}
