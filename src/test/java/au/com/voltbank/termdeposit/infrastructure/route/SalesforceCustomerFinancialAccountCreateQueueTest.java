package au.com.voltbank.termdeposit.infrastructure.route;


import au.com.voltbank.RetailDepositAccountOpeningApplication;
import au.com.voltbank.termdeposit.message.processor.SalesforceFinancialAccountCreateProcessor;
import com.microsoft.azure.storage.queue.CloudQueueMessage;
import org.apache.camel.Exchange;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.model.BeanDefinition;
import org.apache.camel.model.RouteDefinition;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.UseAdviceWith;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.rule.OutputCapture;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.web.client.RestClientException;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

@RunWith(CamelSpringBootRunner.class)
@UseAdviceWith
@SpringBootTest(
    classes = RetailDepositAccountOpeningApplication.class,
    properties = {"camel.springboot.java-routes-include-pattern=**/SalesforceCustomerFinancialAccountCreateQueue*"})

public class SalesforceCustomerFinancialAccountCreateQueueTest extends BaseAzureQueueTest{
  @Mock
  private SalesforceFinancialAccountCreateProcessor salesforceFinancialAccountCreateProcessor;

  @Before
  public void setup(){
    MockEndpoint.resetMocks(context);
  }
  @Rule public final OutputCapture outputCapture = new OutputCapture();

  @Test
  @DirtiesContext
  public void shouldIgnoreEmptyMsg() throws Exception {
    RouteDefinition routeDefinition =
        context.getRouteDefinition(
            SalesforceCustomerFinancialAccountCreateQueue.RID_SALESFORCE_FINANCIAL_ACCOUNT_CREATION_MSG_RETRIEVED);

    (routeDefinition).adviceWith(
        context,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith(DIRECT_START_URI);
          }
        });

    mockResult.expectedMessageCount(EXPECTED_COUNT_ZERO);

    context.start();
    start.sendBody(null);

    MockEndpoint.assertIsSatisfied(context, TIMEOUT_TWO_SECONDS, TimeUnit.SECONDS);
  }

  @Test
  @DirtiesContext
  public void shouldSendNotFoundVoltIdToDeadLetterChannel() throws Exception {
    RouteDefinition routeDefinition =
        context.getRouteDefinition(
            SalesforceCustomerFinancialAccountCreateQueue.RID_SALESFORCE_FINANCIAL_ACCOUNT_CREATION_MSG_RETRIEVED);

    routeDefinition.adviceWith(
        context,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() {
            replaceFromWith(DIRECT_START_URI);
            weaveById(SalesforceCustomerFinancialAccountCreateQueue.RID_DEAD_LETTER_CHANNEL).replace().to(mockResult);
            weaveById(SalesforceCustomerFinancialAccountCreateQueue.RID_DELETEMSG).replace().to(MOCK_URI_SKIPPED);
          }
        });

    mockResult.expectedMessageCount(EXPECTED_COUNT_ONE);
    azureMsgBody(mockResult, FIRST_MSG).isEqualTo(VOLT_ID_NOT_EXIST);

    MockEndpoint deleteMsgSkipped = context.getEndpoint(MOCK_URI_SKIPPED, MockEndpoint.class);
    deleteMsgSkipped.expectedMessageCount(0);

    context.start();
    start.sendBody(new CloudQueueMessage(VOLT_ID_NOT_EXIST));

    MockEndpoint.assertIsSatisfied(context, TIMEOUT_TWO_SECONDS, TimeUnit.SECONDS);
  }


  @Test
  @DirtiesContext
  public void shouldDeleteProcessedMsg() throws Exception {
    RouteDefinition routeDefinition =
        context.getRouteDefinition(
            SalesforceCustomerFinancialAccountCreateQueue.RID_SALESFORCE_FINANCIAL_ACCOUNT_CREATION_MSG_RETRIEVED);

    routeDefinition.adviceWith(
        context,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith(DIRECT_START_URI);
            // ignore all processors
            weaveByType(BeanDefinition.class).remove();
            weaveById(SalesforceCustomerFinancialAccountCreateQueue.RID_DELETEMSG).replace().to(mockResult);
          }
        });

    mockResult.expectedMessageCount(EXPECTED_COUNT_ONE);
    azureMsgBody(mockResult, FIRST_MSG).isEqualTo(VOLT_ID_PROCESSED);

    context.start();
    start.sendBody(new CloudQueueMessage(VOLT_ID_PROCESSED));

    MockEndpoint.assertIsSatisfied(context, TIMEOUT_TWO_SECONDS, TimeUnit.SECONDS);
  }

  @Test
  @DirtiesContext
  public void shouldRetrySalesforceUptoFiveTimesAndFail() throws Exception {
    RouteDefinition routeDefinition =
        context.getRouteDefinition(
            SalesforceCustomerFinancialAccountCreateQueue.RID_SALESFORCE_FINANCIAL_ACCOUNT_CREATION_MSG_RETRIEVED);

    routeDefinition.adviceWith(
        context,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith(DIRECT_START_URI);
            // ignore findCustomer

            weaveById(SalesforceCustomerFinancialAccountCreateQueue.BEAN_CREATE_FINANCIAL_ACCOUNT_SF_CUSTOMER)
                .replace().bean(salesforceFinancialAccountCreateProcessor);
            // failed delivery goes to DLC
            weaveById(SalesforceCustomerFinancialAccountCreateQueue.RID_DEAD_LETTER_CHANNEL).replace().to(mockResult);
            // normal endpoint receives nothing
            weaveById(SalesforceCustomerFinancialAccountCreateQueue.RID_DELETEMSG).replace().to(MOCK_URI_RETRY_FAILED);
          }
        });

    // use mock to simulate exception
    doThrow(new RestClientException("get ready to fail"))
        .when(salesforceFinancialAccountCreateProcessor).createFinancialAccountForSalesforceCustomer(any(),any());

    mockResult.expectedMessageCount(EXPECTED_COUNT_ONE);
    azureMsgBody(mockResult, FIRST_MSG).isEqualTo(MSG_FAILED_AFTER_REST_RETRY);

    System.out.println(azureMsgBody(mockResult, FIRST_MSG).isEqualTo(MSG_FAILED_AFTER_REST_RETRY));
    assertThat(outputCapture.toString(),containsString(MSG_FAILED_AFTER_REST_RETRY));

    MockEndpoint originalEndPoint = context.getEndpoint(MOCK_URI_RETRY_FAILED, MockEndpoint.class);
    originalEndPoint.expectedMessageCount(EXPECTED_COUNT_ZERO);

    context.start();
    start.sendBody(new CloudQueueMessage(MSG_FAILED_AFTER_REST_RETRY));


    MockEndpoint.assertIsSatisfied(context, TIMEOUT_TWENTY_SECONDS, TimeUnit.SECONDS);
  }

  @Test
  @DirtiesContext
  public void shouldRetrySalesforceRestClientExceptionSucceeded() throws Exception {
    RouteDefinition routeDefinition =
        context.getRouteDefinition(
            SalesforceCustomerFinancialAccountCreateQueue.RID_SALESFORCE_FINANCIAL_ACCOUNT_CREATION_MSG_RETRIEVED);

    final int retries = 3;
    AtomicInteger counter = new AtomicInteger(retries);

    routeDefinition.adviceWith(
        context,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith(DIRECT_START_URI);

            weaveById(
                SalesforceCustomerFinancialAccountCreateQueue.BEAN_CREATE_FINANCIAL_ACCOUNT_SF_CUSTOMER)
                .replace()
                .process(
                    ex -> {
                      counter.getAndDecrement();
                      if (counter.get() >= 0) {
                        throw new RestClientException("keep trying");
                      }
                    });

            weaveById(SalesforceCustomerFinancialAccountCreateQueue.RID_DELETEMSG).replace()
                .to(mockResult);
          }
        });

    mockResult.expectedMessageCount(EXPECTED_COUNT_ONE);
    mockResult.allMessages().header(Exchange.REDELIVERY_COUNTER).isEqualTo(retries);
    mockResult.allMessages().header(Exchange.REDELIVERED).isEqualTo(true);
    azureMsgBody(mockResult, FIRST_MSG).isEqualTo(RETRY_SUCCESSFUL);

    context.start();
    start.sendBody(new CloudQueueMessage(RETRY_SUCCESSFUL));

    MockEndpoint.assertIsSatisfied(context, TIMEOUT_TWENTY_SECONDS, TimeUnit.SECONDS);
  }
}
