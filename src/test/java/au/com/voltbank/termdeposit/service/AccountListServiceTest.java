package au.com.voltbank.termdeposit.service;

import au.com.voltbank.RetailDepositAccountOpeningApplication;
import au.com.voltbank.account.model.ProductIdDto;
import au.com.voltbank.account.model.TransactionAccountListDto;
import au.com.voltbank.sis.model.VoltAuthIdPairDto;
import au.com.voltbank.t24.T24ResponseBody;
import au.com.voltbank.termdeposit.corebanking.service.T24AccountService;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.util.CannedData;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.MultiValueMap;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailDepositAccountOpeningApplication.class)
public class AccountListServiceTest {

  @Autowired private AccountListService accountListService;

  @MockBean private T24AccountService t24AccountService;

  @MockBean private SystemIdStoreService systemIdStoreService;

  @Before
  public void setup() {}

  @After
  public void reset() {}

  @Test
  public void shouldGetListOfTransactionsForAnAccount() {
    try {
      T24ResponseBody mockTransactionAccountListDtoWIthValidData =
          CannedData.getMockTransactionAccountListDtoWithValidData();

      VoltAuthIdPairDto voltAuthIdPairDto = CannedData.getVoltAuthIdPairDto();
      when(systemIdStoreService.getVoltId(voltAuthIdPairDto.getAuthId()))
          .thenReturn(voltAuthIdPairDto);
      when(systemIdStoreService.getT24CustomerIdForVoltId(voltAuthIdPairDto.getAuthId()))
          .thenReturn(CannedData.getVoltT24IdPairDto());

      when(t24AccountService.getAccounts(anyString(), any()))
          .thenReturn(
              new ResponseEntity<>(mockTransactionAccountListDtoWIthValidData, HttpStatus.OK));

      Map<String, String> testMap = new HashMap<>();
      testMap.put("hello", "hello");
      testMap.put("pageSize", "23");
      testMap.put("pageStart", "15");

      TransactionAccountListDto transactionAccountListDto =
          accountListService.getAccountsList(testMap, "12345");

      Assert.assertTrue(transactionAccountListDto.getData().size() == 3);
      Assert.assertTrue(
          transactionAccountListDto
              .getData()
              .get(0)
              .getAccountName()
              .equals("Saver Account Individual"));
      Assert.assertTrue(
          transactionAccountListDto.getData().get(0).getAccountNumber().equals("100000085"));
      Assert.assertTrue(
          transactionAccountListDto
              .getData()
              .get(0)
              .getAccountType()
              .equals("Volt Saver Account-Individual"));

      Assert.assertTrue(
          transactionAccountListDto.getData().get(0).getAvailableBalance().equals("2107.07"));
      Assert.assertTrue(transactionAccountListDto.getData().get(0).getBsb().equals("517000"));
      Assert.assertTrue(transactionAccountListDto.getData().get(0).getCurrency().equals("AUD"));
      Assert.assertTrue(
          transactionAccountListDto.getData().get(0).getCurrentBalance().equals("2107.07"));
      Assert.assertTrue(
          transactionAccountListDto.getData().get(0).getInterestPaidPreviousYear().equals("0.65"));
      Assert.assertTrue(
          transactionAccountListDto.getData().get(0).getInterestPaidYearToDate().equals("6.03"));
      Assert.assertTrue(
          transactionAccountListDto.getData().get(0).getInterestRate().equals("1.15"));
      Assert.assertTrue(
          transactionAccountListDto.getData().get(0).getAccountCreatedDate().equals("2019-02-08"));
      Assert.assertNull(transactionAccountListDto.getData().get(0).getTotalInterest());
      Assert.assertTrue(
          transactionAccountListDto.getData().get(0).getProductId().equals(ProductIdDto._100));

      Assert.assertTrue(
          transactionAccountListDto
              .getData()
              .get(1)
              .getAccountName()
              .equals("Transaction Account Individual"));
      Assert.assertTrue(
          transactionAccountListDto.getData().get(1).getAccountNumber().equals("100000093"));
      Assert.assertTrue(
          transactionAccountListDto
              .getData()
              .get(1)
              .getAccountType()
              .equals("Volt Transaction Account-Individual"));

      Assert.assertTrue(
          transactionAccountListDto.getData().get(1).getAvailableBalance().equals("66313753.72"));
      Assert.assertTrue(transactionAccountListDto.getData().get(1).getBsb().equals("517000"));
      Assert.assertTrue(transactionAccountListDto.getData().get(1).getCurrency().equals("AUD"));
      Assert.assertTrue(
          transactionAccountListDto.getData().get(1).getCurrentBalance().equals("66313753.72"));
      Assert.assertTrue(
          transactionAccountListDto.getData().get(1).getInterestPaidPreviousYear().equals("0"));
      Assert.assertTrue(
          transactionAccountListDto.getData().get(1).getInterestPaidYearToDate().equals("0"));
      Assert.assertTrue(
          transactionAccountListDto.getData().get(1).getInterestRate().equals("0.00"));
      Assert.assertTrue(
          transactionAccountListDto.getData().get(1).getAccountCreatedDate().equals("2019-02-08"));
      Assert.assertNull(transactionAccountListDto.getData().get(1).getTotalInterest());
      Assert.assertTrue(
          transactionAccountListDto.getData().get(1).getProductId().equals(ProductIdDto._200));

      Assert.assertTrue(transactionAccountListDto.getPagingInfo().getPageSize().equals("50"));
      Assert.assertTrue(transactionAccountListDto.getPagingInfo().getPageStart().equals("0"));
      Assert.assertTrue(transactionAccountListDto.getPagingInfo().getTotalSize().equals("8"));
      Assert.assertTrue(
          transactionAccountListDto
              .getPagingInfo()
              .getPageToken()
              .equals("4b7da3d3-edd3-4bed-937b-cde2ee6e4187"));

      ArgumentCaptor<MultiValueMap<String, String>> captor =
          ArgumentCaptor.forClass(MultiValueMap.class);
      verify(t24AccountService).getAccounts(contains("1234"), captor.capture());
      MultiValueMap<String, String> value = captor.getValue();
      Assert.assertTrue(value.size() == 3);

      Assert.assertTrue(value.get("page_start").get(0).equals("15"));
      Assert.assertTrue(value.get("page_size").get(0).equals("23"));
      Assert.assertTrue(value.get("hello").get(0).equals("hello"));
    } catch (Exception ex) {
      Assert.fail("Flow Should not reach here : " + ex.getMessage());
    }
  }

  @Test
  public void shouldGetListOfAccountsWithNullAndInvalidProductId() {
    try {
      T24ResponseBody mockTransactionAccountListDtoWIthValidData =
          CannedData.getMockAccountListWithNullAndInValidProductId();

      VoltAuthIdPairDto voltAuthIdPairDto = CannedData.getVoltAuthIdPairDto();
      when(systemIdStoreService.getVoltId(voltAuthIdPairDto.getAuthId()))
          .thenReturn(voltAuthIdPairDto);
      when(systemIdStoreService.getT24CustomerIdForVoltId(voltAuthIdPairDto.getAuthId()))
          .thenReturn(CannedData.getVoltT24IdPairDto());

      when(t24AccountService.getAccounts(anyString(), any()))
          .thenReturn(
              new ResponseEntity<>(mockTransactionAccountListDtoWIthValidData, HttpStatus.OK));

      Map<String, String> testMap = new HashMap<>();
      testMap.put("hello", "hello");
      testMap.put("pageSize", "23");
      testMap.put("pageStart", "15");

      TransactionAccountListDto transactionAccountListDto =
          accountListService.getAccountsList(testMap, "12345");

      Assert.assertTrue(transactionAccountListDto.getData().size() == 2);
      Assert.assertNull(transactionAccountListDto.getData().get(0).getAccountName());
      Assert.assertNull(transactionAccountListDto.getData().get(0).getAccountNumber());
      Assert.assertNull(transactionAccountListDto.getData().get(0).getAccountType());

      Assert.assertTrue(
          transactionAccountListDto.getData().get(0).getAvailableBalance().equals("0"));
      Assert.assertNull(transactionAccountListDto.getData().get(0).getBsb());
      Assert.assertNull(transactionAccountListDto.getData().get(0).getCurrency());
      Assert.assertTrue(transactionAccountListDto.getData().get(0).getCurrentBalance().equals("0"));
      Assert.assertNull(transactionAccountListDto.getData().get(0).getInterestPaidPreviousYear());
      Assert.assertNull(transactionAccountListDto.getData().get(0).getInterestPaidYearToDate());
      Assert.assertNull(transactionAccountListDto.getData().get(0).getInterestRate());
      Assert.assertTrue(
          transactionAccountListDto.getData().get(0).getAccountCreatedDate().equals("2019-02-08"));
      Assert.assertNull(transactionAccountListDto.getData().get(0).getTotalInterest());
      Assert.assertTrue(
          transactionAccountListDto.getData().get(0).getProductId().equals(ProductIdDto._000));

      Assert.assertTrue(
          transactionAccountListDto
              .getData()
              .get(1)
              .getAccountName()
              .equals("Transaction Account Individual"));
      Assert.assertTrue(
          transactionAccountListDto.getData().get(1).getAccountNumber().equals("100000093"));
      Assert.assertTrue(
          transactionAccountListDto
              .getData()
              .get(1)
              .getAccountType()
              .equals("Volt Transaction Account-Individual"));

      Assert.assertTrue(
          transactionAccountListDto.getData().get(1).getAvailableBalance().equals("66313753.72"));
      Assert.assertTrue(transactionAccountListDto.getData().get(1).getBsb().equals("517000"));
      Assert.assertTrue(transactionAccountListDto.getData().get(1).getCurrency().equals("AUD"));
      Assert.assertTrue(
          transactionAccountListDto.getData().get(1).getCurrentBalance().equals("66313753.72"));
      Assert.assertTrue(
          transactionAccountListDto.getData().get(1).getInterestPaidPreviousYear().equals("0"));
      Assert.assertTrue(
          transactionAccountListDto.getData().get(1).getInterestPaidYearToDate().equals("0"));
      Assert.assertTrue(
          transactionAccountListDto.getData().get(1).getInterestRate().equals("0.00"));
      Assert.assertTrue(
          transactionAccountListDto.getData().get(1).getAccountCreatedDate().equals("2019-02-08"));
      Assert.assertNull(transactionAccountListDto.getData().get(1).getTotalInterest());
      Assert.assertTrue(
          transactionAccountListDto.getData().get(1).getProductId().equals(ProductIdDto._000));

      Assert.assertTrue(transactionAccountListDto.getPagingInfo().getPageSize().equals("50"));
      Assert.assertTrue(transactionAccountListDto.getPagingInfo().getPageStart().equals("0"));
      Assert.assertTrue(transactionAccountListDto.getPagingInfo().getTotalSize().equals("8"));
      Assert.assertTrue(
          transactionAccountListDto
              .getPagingInfo()
              .getPageToken()
              .equals("4b7da3d3-edd3-4bed-937b-cde2ee6e4187"));

      ArgumentCaptor<MultiValueMap<String, String>> captor =
          ArgumentCaptor.forClass(MultiValueMap.class);
      verify(t24AccountService).getAccounts(contains("1234"), captor.capture());
      MultiValueMap<String, String> value = captor.getValue();
      Assert.assertTrue(value.size() == 3);

      Assert.assertTrue(value.get("page_start").get(0).equals("15"));
      Assert.assertTrue(value.get("page_size").get(0).equals("23"));
      Assert.assertTrue(value.get("hello").get(0).equals("hello"));
    } catch (Exception ex) {
      Assert.fail("Flow Should not reach here : " + ex.getMessage());
    }
  }

  @Test
  public void shouldThrowExceptionWhenValueDateIsNullForAccountsList() {
    try {
      VoltAuthIdPairDto voltAuthIdPairDto = new VoltAuthIdPairDto();
      voltAuthIdPairDto.setVoltId("12345");
      T24ResponseBody t24ResponseBody = CannedData.getMockAccountListwithNullValueDateBody();
      when(t24AccountService.getAccounts(anyString(), any()))
          .thenReturn(new ResponseEntity<>(t24ResponseBody, HttpStatus.OK));
      accountListService.getAccountsList(new HashMap<>(), "12345");
      Assert.fail(
          "Flow should not reach here, should throw a VoltCustomException saying value date is null as it is mandatory");
    } catch (VoltCustomException ex) {
      Assert.assertNotNull(ex);
      Assert.assertTrue(ex.getMessage().equals("The accounts value date is null"));
      Assert.assertTrue(ex.getErrCode().equals("0200100"));
    } catch (Exception ex) {
      Assert.fail("Exception should not reach here");
    }
  }

  @Test
  public void shouldThrowExceptionAccountsList() {
    try {
      VoltAuthIdPairDto voltAuthIdPairDto = new VoltAuthIdPairDto();
      voltAuthIdPairDto.setVoltId("12345");
      T24ResponseBody t24ResponseBody = CannedData.getMockAccountListwithEmptyBody();
      when(t24AccountService.getAccounts(anyString(), any()))
          .thenReturn(new ResponseEntity<>(t24ResponseBody, HttpStatus.OK));
      TransactionAccountListDto transactionAccountListDto =
          accountListService.getAccountsList(new HashMap<>(), "12345");
      Assert.assertTrue(transactionAccountListDto.getData().size() == 0);
      Assert.assertTrue(transactionAccountListDto.getPagingInfo().getPageSize().equals("50"));
      Assert.assertTrue(transactionAccountListDto.getPagingInfo().getPageStart().equals("0"));
      Assert.assertTrue(transactionAccountListDto.getPagingInfo().getTotalSize().equals("8"));
      Assert.assertTrue(
          transactionAccountListDto
              .getPagingInfo()
              .getPageToken()
              .equals("4b7da3d3-edd3-4bed-937b-cde2ee6e4187"));

    } catch (Exception ex) {
      Assert.fail("Exception should not reach here");
    }
  }

  @Test
  public void shouldThrowNullPointerExceptionAccountsList() {
    try {
      VoltAuthIdPairDto voltAuthIdPairDto = new VoltAuthIdPairDto();
      voltAuthIdPairDto.setVoltId("12345");
      T24ResponseBody t24ResponseBody = CannedData.getMockAccountListwithEmptyBody();
      when(t24AccountService.getAccounts(anyString(), any()))
          .thenReturn(new ResponseEntity<>(t24ResponseBody, HttpStatus.OK));
      TransactionAccountListDto transactionAccountListDto =
          accountListService.getAccountsList(null, null);
      Assert.fail("Exception should not reach here");
    } catch (NullPointerException ex) {
      Assert.assertNotNull(ex);
    } catch (Exception ex) {
      Assert.fail("Exception should not reach here");
    }
  }
}
