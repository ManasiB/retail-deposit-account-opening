package au.com.voltbank.termdeposit.service;

import au.com.voltbank.Constants;
import au.com.voltbank.RetailDepositAccountOpeningApplication;
import au.com.voltbank.account.model.SavingsAccountApplicationDataDto;
import au.com.voltbank.customer.model.CustomerBankingCreateResponseDto;
import au.com.voltbank.sis.model.VoltT24IdPairDto;
import au.com.voltbank.termdeposit.corebanking.service.T24AccountService;
import au.com.voltbank.t24.T24ResponseBody;
import au.com.voltbank.t24.account.AccountDetailBodyDto;
import au.com.voltbank.termdeposit.dao.SavingsAccountApplicationRepository;
import au.com.voltbank.termdeposit.dto.Customer;
import au.com.voltbank.termdeposit.entity.SavingsAccountApplication;
import au.com.voltbank.termdeposit.exception.SavingsAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.T24IdException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.infrastructure.route.BaseAzureQueueTest;
import au.com.voltbank.termdeposit.infrastructure.route.SalesforceCustomerFinancialAccountCreateQueue;
import au.com.voltbank.termdeposit.util.CannedData;
import com.microsoft.azure.storage.queue.CloudQueueMessage;
import java.util.concurrent.TimeUnit;
import org.apache.camel.EndpointInject;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.model.BeanDefinition;
import org.apache.camel.model.RouteDefinition;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.UseAdviceWith;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

import static com.tyro.oss.randomdata.RandomString.randomNumericString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailDepositAccountOpeningApplication.class)
public class SavingsAccountServiceTest {

  @Autowired private SavingsAccountService savingsAccountService;

  @Autowired private SavingsAccountApplicationRepository savingsAccountApplicationRepository;

  @MockBean private CustomerService customerService;

  @MockBean private T24AccountService t24AccountService;

  @MockBean private SystemIdStoreService systemIdStoreService;

  private SavingsAccountApplicationDataDto savingsApplicationDto;

  private Customer customer;

  private CustomerBankingCreateResponseDto customerBankingDetailsDto;

  @Before
  public void setup() {
    savingsApplicationDto = CannedData.getSavingsAccountApplicationDataDto();
    customer = CannedData.getCannedCustomerEntity();
    customerBankingDetailsDto = CannedData.getCustomerBankingDetailsDto();
  }

  @After
  public void reset() {
    Mockito.reset(customerService, t24AccountService, systemIdStoreService);
  }

  @Test
  public void shouldCreateAccount() throws VoltCustomException, IOException {
    // setup
    String voltId = randomNumericString(8);

    when(customerService.createt24CustomerIfnotExist(voltId)).thenReturn(customerBankingDetailsDto);

    T24ResponseBody expectedResponse =
        new T24ResponseBody("mockHeader", CannedData.getCreateAccountResponseBody(), null);

    when(t24AccountService.createSavingsAccount(any()))
        .thenReturn(new ResponseEntity<>(expectedResponse, HttpStatus.CREATED));

    // test
    SavingsAccountApplication result = savingsAccountService.create(savingsApplicationDto, voltId);

    SavingsAccountApplication accountApplication =
        savingsAccountApplicationRepository.findOneByVoltId(voltId).get();

    assertThat(accountApplication.getArrangementId(), is(result.getArrangementId()));
  }

  @Test
  public void shouldFetchAccountDetailsCreated()
      throws VoltCustomException, IOException, SavingsAccountNotFoundException, T24IdException {
    // setup
    List<LinkedHashMap<String, String>> accountDetails =
        CannedData.getAccountDetailsBodyDtoLinkedHashMap();
    AccountDetailBodyDto expectedResult = CannedData.getAccountDetailsBodyDto().get(0);

    String t24CustomerId = customer.getT24CustomerId();
    VoltT24IdPairDto dto = new VoltT24IdPairDto();
    dto.setT24Id(t24CustomerId);
    dto.setVoltId(customer.getVoltId());

    when(t24AccountService.getAccounts(t24CustomerId))
        .thenReturn(
            new ResponseEntity<>(
                new T24ResponseBody("mockHeader", accountDetails, null), HttpStatus.OK));

    SavingsAccountApplication createResult =
        new SavingsAccountApplication()
            .toBuilder()
            .arrangementId(accountDetails.get(0).get("arrangementId"))
            .build();

    when(systemIdStoreService.getT24CustomerIdForVoltId(createResult.getVoltId())).thenReturn(dto);
    // test
    AccountDetailBodyDto getResultDto =
        savingsAccountService.getSavingsAccountDetails(createResult);

    // verify
    assertThat(getResultDto, is(expectedResult));
  }

  @Test(expected = VoltCustomException.class)
  public void shouldThrowVoltCustomerNotFoundExceptionIfCustomerNotFound()
      throws VoltCustomException {
    // setup
    String voltId = randomNumericString(8);

    when(customerService.createt24CustomerIfnotExist(voltId))
        .thenThrow(
            new VoltCustomException(
                Constants.ERROR_SYSTEM,
                "Can not create account for an inactive customer - voltId:" + voltId,
                HttpStatus.INTERNAL_SERVER_ERROR.value()));

    // test
    savingsAccountService.create(
        new ModelMapper().map(savingsApplicationDto, SavingsAccountApplicationDataDto.class),
        voltId);
  }

  @Test(expected = SavingsAccountNotFoundException.class)
  public void shouldThrowSavinsAccountNotFoundExceptionIfSavingsAccountNotFound()
      throws VoltCustomException, SavingsAccountNotFoundException, T24IdException {
    // setup
    String t24CustomerId = CannedData.getMockT24CustomerId();

    VoltT24IdPairDto voltT24IdPairDto = new VoltT24IdPairDto();
    voltT24IdPairDto.setT24Id(t24CustomerId);
    voltT24IdPairDto.setVoltId(CannedData.getMockVoltId());
    when(systemIdStoreService.getT24CustomerIdForVoltId(any())).thenReturn(voltT24IdPairDto);

    when(t24AccountService.getAccounts(t24CustomerId))
        .thenReturn(
            new ResponseEntity<>(
                new T24ResponseBody("mockHeader", Collections.emptyList(), null), HttpStatus.OK));

    SavingsAccountApplication savingsAccountApplication = new SavingsAccountApplication();
    savingsAccountApplication.setVoltId(voltT24IdPairDto.getVoltId());

    // test
    savingsAccountService.getSavingsAccountDetails(savingsAccountApplication);
  }
}
