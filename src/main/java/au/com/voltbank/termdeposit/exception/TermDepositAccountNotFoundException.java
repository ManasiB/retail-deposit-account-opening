package au.com.voltbank.termdeposit.exception;

public class TermDepositAccountNotFoundException extends Exception {

  public TermDepositAccountNotFoundException(String message) {
    super(message);
  }
}
