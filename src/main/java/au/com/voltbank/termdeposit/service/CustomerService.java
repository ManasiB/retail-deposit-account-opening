package au.com.voltbank.termdeposit.service;

import au.com.voltbank.Constants;
import au.com.voltbank.customer.model.CustomerBankingCreateResponseDto;
import au.com.voltbank.termdeposit.exception.CustomerNotFoundException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class CustomerService {

  @Value("${CUSTOMER_MANAGEMENT_HOST}")
  private String customer_management_host;

  static final String CUSTOMER_MANAGEMENT_PATH =
      "/v1/customer_management/customer/banking_customer/";

  private static final String AUTHORIZATION = "Authorization";
  private static final String BEARER = "Bearer ";

  @Autowired private RestTemplate secureRestTemplate;

  @Autowired private AzureActiveDirectoryService azureActiveDirectoryService;

  @Value("${customer.management.resource.id}")
  private String resourceId;

  public CustomerBankingCreateResponseDto createt24CustomerIfnotExist(String voltId)
      throws VoltCustomException {
    try {
      HttpHeaders httpHeaders = new HttpHeaders();
      String accessToken = azureActiveDirectoryService.getAccessToken(resourceId);
      httpHeaders.add(AUTHORIZATION, StringUtils.join(BEARER, accessToken));
      HttpEntity<CustomerBankingCreateResponseDto> requestEntity =
          new HttpEntity<>(null, httpHeaders);
      ResponseEntity responseEntity = secureRestTemplate.postForEntity(
          StringUtils.join(customer_management_host, CUSTOMER_MANAGEMENT_PATH, voltId),
          requestEntity, CustomerBankingCreateResponseDto.class);
      if (HttpStatus.NOT_FOUND.equals(responseEntity.getStatusCode())) {
        throw new CustomerNotFoundException(voltId);
      }
      return (CustomerBankingCreateResponseDto) responseEntity.getBody();
    } catch (Exception e) {
      log.error(
          String.format("Failed to call customer management service with voltId : %s", voltId), e);
      throw new VoltCustomException(
          Constants.ERROR_SYSTEM, "", HttpStatus.INTERNAL_SERVER_ERROR.value());
    }
  }
}
