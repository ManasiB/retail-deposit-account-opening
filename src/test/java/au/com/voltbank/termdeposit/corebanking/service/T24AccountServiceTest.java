package au.com.voltbank.termdeposit.corebanking.service;

import au.com.voltbank.Constants;
import au.com.voltbank.RetailDepositAccountOpeningApplication;
import au.com.voltbank.t24.T24RequestBody;
import au.com.voltbank.t24.T24ResponseBody;
import au.com.voltbank.t24.account.AccountDetailBodyDto;
import au.com.voltbank.t24.account.CommitmentDto;
import au.com.voltbank.t24.account.T24CreateDepositAccountDto;
import au.com.voltbank.t24.account.T24CreateSavingsAccountDto;
import au.com.voltbank.t24.account.T24CreateTransactionAccountDto;
import au.com.voltbank.t24.account.T24PartyDto;
import au.com.voltbank.termdeposit.corebanking.T24Client;
import au.com.voltbank.termdeposit.entity.SavingsAccountApplication;
import au.com.voltbank.termdeposit.entity.TermDepositAccountApplication;
import au.com.voltbank.termdeposit.entity.TransactionAccountApplication;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.util.CannedData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailDepositAccountOpeningApplication.class)
public class T24AccountServiceTest {

  private final String t24CustomerId = "10000060";
  @Autowired private T24AccountService t24AccountService;
  @MockBean(name = "secureRestTemplate")
  private RestTemplate secureRestTemplate;
  @MockBean private T24Client mockT24Client;

  @Before
  public void setUp() {
    reset(secureRestTemplate, mockT24Client);
  }

  @Test
  public void shouldSetDefaultsOnT24DtoBeforeCreatingSavingsAccount() throws VoltCustomException {
    // setup
    SavingsAccountApplication accountApplication = CannedData.getSavingsAccountApplication();

    when(secureRestTemplate.postForEntity(anyString(), any(), any()))
        .thenReturn(new ResponseEntity<>(mockResponseBody(), null, HttpStatus.CREATED));

    // test
    t24AccountService.createSavingsAccount(t24CustomerId);

    ArgumentCaptor<HttpEntity<T24RequestBody>> captor = ArgumentCaptor.forClass(HttpEntity.class);
    verify(mockT24Client)
        .post(
            contains("voltarrangementservice/product/createAccountArrangement"), captor.capture());

    T24CreateSavingsAccountDto t24CreateSavingsAccountDto =
        (T24CreateSavingsAccountDto) captor.getValue().getBody().getBody();

    assertThat(t24CreateSavingsAccountDto.getActivityId(), is("ACCOUNTS-NEW-ARRANGEMENT"));
    assertThat(
        t24CreateSavingsAccountDto.getEffectiveDate(),
        is(LocalDate.now().format(DateTimeFormatter.ofPattern(Constants.T24_DATE_FORMAT))));
    assertThat(t24CreateSavingsAccountDto.getProductId(), is("VB.SAVER.AC.IND"));
    assertThat(t24CreateSavingsAccountDto.getCurrencyId(), is("AUD"));

    T24PartyDto party = t24CreateSavingsAccountDto.getParties().get(0);
    assertThat(party.getPartyRole(), is("OWNER"));
  }

  @Test
  public void shouldSetDefaultsOnT24DtoBeforeCreatingTransactionAccount()
      throws VoltCustomException {
    // setup
    TransactionAccountApplication accountApplication =
        CannedData.getTransactionAccountApplication();

    when(secureRestTemplate.postForEntity(anyString(), any(), any()))
        .thenReturn(new ResponseEntity<>(mockResponseBody(), null, HttpStatus.CREATED));

    // test
    t24AccountService.createTransactionAccount(accountApplication);

    ArgumentCaptor<HttpEntity<T24RequestBody>> captor = ArgumentCaptor.forClass(HttpEntity.class);
    verify(mockT24Client)
        .post(
            contains("voltarrangementservice/product/createAccountArrangement"), captor.capture());

    T24CreateTransactionAccountDto t24CreateTransactionAccountDto =
        (T24CreateTransactionAccountDto) captor.getValue().getBody().getBody();

    assertThat(t24CreateTransactionAccountDto.getActivityId(), is("ACCOUNTS-NEW-ARRANGEMENT"));
    assertThat(
        t24CreateTransactionAccountDto.getEffectiveDate(),
        is(LocalDate.now().format(DateTimeFormatter.ofPattern(Constants.T24_DATE_FORMAT))));
    assertThat(t24CreateTransactionAccountDto.getProductId(), is("VB.TRAN.AC.IND"));
    assertThat(t24CreateTransactionAccountDto.getCurrencyId(), is("AUD"));

    T24PartyDto party = t24CreateTransactionAccountDto.getParties().get(0);
    assertThat(party.getPartyId(), is(accountApplication.getT24CustomerId()));
    assertThat(party.getPartyRole(), is("OWNER"));
  }

  @Test
  public void shouldSetDefaultsOnT24DtoBeforeCreatingTermDepositAccount()
      throws VoltCustomException {
    // setup
    TermDepositAccountApplication accountApplication = CannedData.getDepositAccountApplication();

    when(secureRestTemplate.postForEntity(anyString(), any(), any()))
        .thenReturn(new ResponseEntity<>(mockResponseBody(), null, HttpStatus.CREATED));

    // test
    t24AccountService.createTermDepositAccount(accountApplication);

    ArgumentCaptor<HttpEntity<T24RequestBody>> captor = ArgumentCaptor.forClass(HttpEntity.class);
    verify(mockT24Client)
        .post(
            contains("voltarrangementservice/product/createDepositArrangement"), captor.capture());

    T24CreateDepositAccountDto t24CreateDepositAccountDto =
        (T24CreateDepositAccountDto) captor.getValue().getBody().getBody();

    assertThat(t24CreateDepositAccountDto.getActivityId(), is("DEPOSITS-NEW-ARRANGEMENT"));
    assertThat(
        t24CreateDepositAccountDto.getEffectiveDate(),
        is(LocalDate.now().format(DateTimeFormatter.ofPattern(Constants.T24_DATE_FORMAT))));
    assertThat(t24CreateDepositAccountDto.getProductId(), is("VB.TD.RETAIL.MATURITY"));
    assertThat(t24CreateDepositAccountDto.getCurrencyId(), is("AUD"));

    CommitmentDto commitment = t24CreateDepositAccountDto.getCommitment();
    assertThat(
        commitment.getTerm(), is(String.format("%sM", accountApplication.getTermInMonths())));
    assertThat(commitment.getAmount(), is(accountApplication.getAmountInWholeDollars()));

    T24PartyDto party = t24CreateDepositAccountDto.getParties().get(0);
    assertThat(party.getPartyId(), is(accountApplication.getT24CustomerId()));
    assertThat(party.getPartyRole(), is("OWNER"));
  }

  @Test
  public void shouldGetListOfAccounts() throws VoltCustomException, IOException {

    List<AccountDetailBodyDto> bodyDto = CannedData.getAccountDetailsBodyDto();

    ResponseEntity<T24ResponseBody> expectedResponse =
        ResponseEntity.ok(new T24ResponseBody(null, bodyDto, null));
    when(mockT24Client.getHttpRequestHeader()).thenReturn(CannedData.getAzureTokenRequestHeader());
    when(mockT24Client.get(anyString(), eq(t24CustomerId), any(HttpHeaders.class)))
        .thenReturn(expectedResponse);

    // test
    ResponseEntity<T24ResponseBody> accountsList = t24AccountService.getAccounts(t24CustomerId);

    verify(mockT24Client)
        .get(
            contains("baasarrangementservice/product/getAccountArrangementDetails"),
            eq(t24CustomerId),
            any(HttpHeaders.class));

    assertThat(accountsList, is(expectedResponse));
  }

  private T24ResponseBody mockResponseBody() {
    return new T24ResponseBody("mockHeader", "mockBody", "mockErr");
  }
}
