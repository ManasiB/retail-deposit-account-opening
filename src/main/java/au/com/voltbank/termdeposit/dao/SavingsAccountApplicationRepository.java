package au.com.voltbank.termdeposit.dao;

import au.com.voltbank.termdeposit.entity.SavingsAccountApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/** Spring Data JPA repository for the User entity. */
@Repository
public interface SavingsAccountApplicationRepository
    extends JpaRepository<SavingsAccountApplication, Long> {

  Optional<SavingsAccountApplication> findOneByVoltId(String voltId);
}
