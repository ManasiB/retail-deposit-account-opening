package au.com.voltbank.termdeposit.security;

import au.com.voltbank.security.TokenParser;
import au.com.voltbank.termdeposit.util.CannedData;
import com.auth0.spring.security.api.authentication.PreAuthenticatedAuthenticationJsonWebToken;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AzureAuthenticationProviderTest {

  @InjectMocks AzureAuthenticationProvider provider;

  @Mock TokenParser tokenParser;

  @Test
  public void should_not_authenticate() {
    when(tokenParser.validateToken(anyString())).thenReturn(false);
    assertNull(
        provider.authenticate(
            PreAuthenticatedAuthenticationJsonWebToken.usingToken(CannedData.EXPIRED_AZURE_TOKEN)));
  }
}
