package au.com.voltbank.termdeposit.mapper;

import au.com.voltbank.account.model.SavingsAccountApplicationDataDto;
import au.com.voltbank.account.model.SavingsAccountApplicationResultDto;
import au.com.voltbank.account.model.TransactionAccountApplicationResultDto;
import au.com.voltbank.t24.account.AccountDetailBodyDto;
import au.com.voltbank.termdeposit.entity.SavingsAccountApplication;
import org.apache.commons.lang3.math.NumberUtils;

public class AccountMapper {

  private AccountMapper() {}

  public static SavingsAccountApplicationResultDto mapToSavingsAccountResultDto(
      AccountDetailBodyDto accountDetail) {
    SavingsAccountApplicationResultDto resultDto = new SavingsAccountApplicationResultDto();
    resultDto.setAccountBSB(accountDetail.getSortCode());
    resultDto.setAccountNumber(accountDetail.getAcctNumber());
    resultDto.setAccountName(accountDetail.getNickname());
    resultDto.setPrincipalBalanceInWholeDollars(
        NumberUtils.toInt(accountDetail.getPrincipalBalance(), 0));
    resultDto.setInterestRate(accountDetail.getDebitInterestRate());
    return resultDto;
  }

  public static TransactionAccountApplicationResultDto mapToTransactionAccountResultDto(
      AccountDetailBodyDto accountDetail) {
    TransactionAccountApplicationResultDto resultDto = new TransactionAccountApplicationResultDto();
    resultDto.setAccountBSB(accountDetail.getSortCode());
    resultDto.setAccountNumber(accountDetail.getAcctNumber());
    resultDto.setAccountName(accountDetail.getNickname());
    return resultDto;
  }

  public static SavingsAccountApplication mapToSavingsAccountApplicationEntity(
      SavingsAccountApplicationDataDto dto) {
    SavingsAccountApplication entity = new SavingsAccountApplication();
    entity.setAccountName(dto.getAccountName());
    entity.setNominatedAccountBSB(dto.getNominatedAccountBSB());
    entity.setNominatedAccountId(dto.getNominatedAccountId());
    entity.setNominatedAccountName(dto.getNominatedAccountName());
    entity.setNominatedAccountNumber(dto.getNominatedAccountNumber());
    return entity;
  }
}
