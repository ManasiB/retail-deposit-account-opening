package au.com.voltbank.termdeposit.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder(builderClassName = "PassportDtoBuilder")
@JsonDeserialize(builder = PassportDto.PassportDtoBuilder.class)
@NoArgsConstructor
@AllArgsConstructor
public class PassportDto {

  private String number;
  private LocalDate expiry;
  private String country;

  @JsonPOJOBuilder(withPrefix = "")
  public static class PassportDtoBuilder {
    // Lombok will add constructor, setters, build method
  }
}
