package au.com.voltbank.termdeposit.config;

public enum VoltTermDepositIndexCode {
  MONTHLY("01"),
  QUARTERLY("02"),
  SIXMONTHLY("03"),
  YEARLY("04");
  private final String value;

  VoltTermDepositIndexCode(String value) {
    this.value = value;
  }

  public String getValue() {
    return this.value;
  }
}
