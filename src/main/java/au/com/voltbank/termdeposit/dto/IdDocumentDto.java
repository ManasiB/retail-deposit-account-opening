package au.com.voltbank.termdeposit.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder(builderClassName = "IdDocumentDtoBuilder")
@JsonDeserialize(builder = IdDocumentDto.IdDocumentDtoBuilder.class)
@NoArgsConstructor
@AllArgsConstructor
public class IdDocumentDto {

  private String legalId;
  private String legalDocumentName;
  private String nameOnId;
  private String issueAuthority;
  private LocalDate issueDate;
  private LocalDate expiryDate;

  @JsonPOJOBuilder(withPrefix = "")
  public static class IdDocumentDtoBuilder {
    // Lombok will add constructor, setters, build method
  }
}
