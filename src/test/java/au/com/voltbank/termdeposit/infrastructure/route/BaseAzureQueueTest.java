package au.com.voltbank.termdeposit.infrastructure.route;

import com.microsoft.azure.storage.queue.CloudQueueMessage;
import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.ValueBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseAzureQueueTest {

  static final String MOCK_URI_SKIPPED = "mock:uri:skipped";
  static final String MOCK_URI_RETRY_FAILED = "mock:uri:failed";
  static final String VOLT_ID_NOT_EXIST = "VOLT_ID_NOT_EXISTS";
  static final String VOLT_ID_PROCESSED = "VOLT_ID_PROCESSED";
  static final String DIRECT_START_URI = "direct:start";
  static final String AZURE_MSG_CONTENT = "messageContentAsString";
  static final String MOCK_RESULT = "mock:result";

  static final int EXPECTED_COUNT_ZERO = 0;
  static final int EXPECTED_COUNT_ONE = 1;

  static final int FIRST_MSG = 0;

 static final int TIMEOUT_TWO_SECONDS = 2;
  static final int TIMEOUT_TWENTY_SECONDS = 20;

  static final String RETRY_SUCCESSFUL = "RETRY_SUCCESSFUL";
  static final String MSG_FAILED_AFTER_REST_RETRY = "FAILED_AFTER_RETRY";

  @Autowired
  CamelContext context;

  @EndpointInject(uri = MOCK_RESULT)
  MockEndpoint mockResult;

  @Produce(uri = DIRECT_START_URI)
  ProducerTemplate start;

  @Before
  public void setup() {
    MockEndpoint.resetMocks(context);
  }

  protected ValueBuilder azureMsgBody(MockEndpoint mockResult, int index) {
    return mockResult.message(index).body()
        .convertTo(CloudQueueMessage.class)
        .method(AZURE_MSG_CONTENT);
  }
}
