package au.com.voltbank.termdeposit.mapper;

import au.com.voltbank.Constants;
import au.com.voltbank.account.model.TdAccountApplicationResultDto;
import au.com.voltbank.t24.account.CommitmentDto;
import au.com.voltbank.t24.account.T24CreateDepositAccountDto;
import au.com.voltbank.t24.account.T24PartyDto;
import au.com.voltbank.t24.account.TDAccountDetailBodyDto;
import au.com.voltbank.t24.beneficiary.BeneficiaryBodyDto;
import au.com.voltbank.termdeposit.config.VoltTermDepositIndexCode;
import au.com.voltbank.termdeposit.entity.TermDepositAccountApplication;
import au.com.voltbank.termdeposit.mapper.t24.T24QuirkMapper;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableMap;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static java.time.format.DateTimeFormatter.ofPattern;

public class TermDepositAccountMapper {

  @VisibleForTesting
  static final ImmutableMap<String, String> productIdLookUpTable =
      ImmutableMap.of(
          VoltTermDepositIndexCode.MONTHLY.getValue(), "VB.TD.RETAIL.MATURITY",
          VoltTermDepositIndexCode.QUARTERLY.getValue(), "VB.TD.RETAIL.MATURITY",
          VoltTermDepositIndexCode.SIXMONTHLY.getValue(), "VB.TD.RETAIL.MATURITY",
          VoltTermDepositIndexCode.YEARLY.getValue(), "VB.TD.RETAIL.MATURITY");
  private static final int T24_CANCELLATION_PERIOD = 6;

  private TermDepositAccountMapper() {}

  public static T24CreateDepositAccountDto toDto(TermDepositAccountApplication entity) {

    T24PartyDto partyDto = T24PartyDto.builder().partyId(entity.getT24CustomerId()).build();

    List<T24PartyDto> partyDtoList = new ArrayList<>();
    partyDtoList.add(partyDto);

    CommitmentDto commitmentDto =
        CommitmentDto.builder()
            .amount(entity.getAmountInWholeDollars())
            .term(T24QuirkMapper.getT24Term(entity.getTermInMonths()))
            .build();

    return T24CreateDepositAccountDto.builder()
        .effectiveDate(T24QuirkMapper.getT24Date(entity.getStartDate()))
        .parties(partyDtoList)
        .commitment(commitmentDto)
        .productId(productIdLookUpTable.get(entity.getIndexCode()))
        .build();
  }

  public static TdAccountApplicationResultDto mapToResultDto(
      TDAccountDetailBodyDto tdAccountDetail) {
    TdAccountApplicationResultDto resultDto = new TdAccountApplicationResultDto();
    resultDto.setBsb(tdAccountDetail.getSortcode());
    resultDto.setAccountNumber(tdAccountDetail.getAccount());
    resultDto.setPrincipalBalanceInWholeDollars(
        Integer.valueOf(tdAccountDetail.getPrincipalBalance()));
    resultDto.setMaturityDate(
        getDateInFormat(
            LocalDate.parse(
                tdAccountDetail.getMaturityDate(), ofPattern(Constants.T24_DATE_FORMAT))));
    resultDto.setCancellationDate(
        getDateInFormat(
            LocalDate.parse(tdAccountDetail.getBookingDate(), ofPattern(Constants.T24_DATE_FORMAT))
                .plusDays(T24_CANCELLATION_PERIOD)));
    resultDto.setInterestRate(tdAccountDetail.getInterestRate());
    return resultDto;
  }

  private static String getDateInFormat(LocalDate t24Date) {
    return t24Date.format(ofPattern(Constants.DATE_FORMAT));
  }

  public static BeneficiaryBodyDto initBeneficiaryDto(TermDepositAccountApplication entity) {
    return BeneficiaryBodyDto.builder()
        .beneficiaryBSB(entity.getLinkedAccountBSB())
        .beneficiaryAccountNumber(entity.getLinkedAccountNumber())
        .beneficaryType("NOMINATED.ACCOUNT")
        .beneficiaryAcctName(entity.getLinkedAccountName())
        .owningCustomer(entity.getT24CustomerId())
        .transactionType("AC")
        .build();
  }
}
