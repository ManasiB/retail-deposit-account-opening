package au.com.voltbank.termdeposit.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

@Component
@Data
@Builder(builderClassName = "SalesforceUpdateDtoBuilder")
@JsonDeserialize(builder = SalesforceUpdateDto.SalesforceUpdateDtoBuilder.class)
@NoArgsConstructor
@AllArgsConstructor

@Getter
@Setter
public class SalesforceUpdateDto {

  private String accountNumber;

  private String accountStatus;

  private String fundingStatus;


  @JsonPOJOBuilder(withPrefix = "")
  public static class SalesforceUpdateDtoBuilder {
    // Lombok will add constructor, setters, build method
  }
  
  @Override public String toString() {
	    return String.format("%s (age: %d)", accountNumber, accountStatus,fundingStatus);
	  }
}
