package au.com.voltbank.termdeposit.service;

import au.com.voltbank.Constants;
import au.com.voltbank.RetailDepositAccountOpeningApplication;
import au.com.voltbank.account.model.TdAccountApplicationDataDto;
import au.com.voltbank.customer.model.CustomerBankingCreateResponseDto;
import au.com.voltbank.termdeposit.corebanking.service.T24AccountService;
import au.com.voltbank.t24.T24ResponseBody;
import au.com.voltbank.t24.account.TDAccountDetailBodyDto;
import au.com.voltbank.termdeposit.dto.Customer;
import au.com.voltbank.termdeposit.entity.TermDepositAccountApplication;
import au.com.voltbank.termdeposit.exception.TermDepositAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.util.CannedData;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

import static com.tyro.oss.randomdata.RandomString.randomNumericString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailDepositAccountOpeningApplication.class)
@Transactional
public class TermDepositAccountApplicationServiceTest {

  @Autowired private TermDepositAccountApplicationService tdaApplicationService;

  @MockBean private CustomerService customerService;

  @MockBean private T24AccountService coreBankingTDAccountService;

  private TdAccountApplicationDataDto tdaApplicationDto;

  private Customer customer;

  private CustomerBankingCreateResponseDto customerBankingDetailsDto;

  @Before
  public void setup() {
    tdaApplicationDto = CannedData.getTdAccountApplicationDataDto();
    customer = CannedData.getCannedCustomerEntity();
    customerBankingDetailsDto = CannedData.getCustomerBankingDetailsDto();
  }

  @After
  public void reset() {
    Mockito.reset(customerService, coreBankingTDAccountService);
  }

  @Test
  public void should_create_account_in_account_opening_db()
      throws VoltCustomException, IOException {
    // setup
    String voltId = randomNumericString(8);

    customerBankingDetailsDto.setT24CustomerId(customer.getT24CustomerId());
    when(customerService.createt24CustomerIfnotExist(voltId)).thenReturn(customerBankingDetailsDto);

    T24ResponseBody expectedResponse =
        new T24ResponseBody("mockHeader", CannedData.getCreateAccountResponseBody(), null);

    when(coreBankingTDAccountService.createTermDepositAccount(
            any(TermDepositAccountApplication.class)))
        .thenReturn(new ResponseEntity<>(expectedResponse, HttpStatus.CREATED));

    // test
    TermDepositAccountApplication result = tdaApplicationService.create(tdaApplicationDto, voltId);

    assertThat(customer.getT24CustomerId(), is(result.getT24CustomerId()));
    assertThat(CannedData.ARRANGEMENT_ID, is(result.getArrangementId()));
    assertThat(CannedData.TERM_IN_MONTHS_12, is(result.getTermInMonths()));
  }

  @Test
  public void should_fetch_account_detail_created()
      throws VoltCustomException, IOException, TermDepositAccountNotFoundException {
    // setup
    List<LinkedHashMap<String, String>> account =
        CannedData.getTDAccountResponseBodyLinkedHashMap();
    TDAccountDetailBodyDto expectedDto = CannedData.getTDAccountResponseBody();

    String t24CustomerId = customer.getT24CustomerId();

    when(coreBankingTDAccountService.getTermDepositAccounts(t24CustomerId))
        .thenReturn(
            new ResponseEntity<>(new T24ResponseBody("mockHeader", account, null), HttpStatus.OK));

    TermDepositAccountApplication createResult =
        new TermDepositAccountApplication()
            .toBuilder()
            .arrangementId(account.get(0).get("reference"))
            .t24CustomerId(t24CustomerId)
            .build();

    // test
    TDAccountDetailBodyDto getResultDto =
        tdaApplicationService.getTermDepositAccountDetails(createResult);

    // verify
    assertThat(getResultDto, is(expectedDto));
  }

  @Test(expected = VoltCustomException.class)
  public void shouldThrowVoltCustomerNotFoundExceptionIfCustomerNotFound()
      throws VoltCustomException {
    // setup
    String voltId = randomNumericString(8);

    when(customerService.createt24CustomerIfnotExist(voltId))
        .thenThrow(
            new VoltCustomException(
                Constants.ERROR_SYSTEM,
                "Can not create account for an inactive customer - voltId:" + voltId,
                HttpStatus.INTERNAL_SERVER_ERROR.value()));

    // test
    tdaApplicationService.create(
        new ModelMapper().map(tdaApplicationDto, TdAccountApplicationDataDto.class), voltId);
  }

  @Test(expected = TermDepositAccountNotFoundException.class)
  public void shouldThrowTermDepositAccountNotFoundExceptionIfTermDepositAccountNotFound()
      throws VoltCustomException, TermDepositAccountNotFoundException {
    // setup
    String t24CustomerId = randomNumericString(8);

    when(coreBankingTDAccountService.getTermDepositAccounts(t24CustomerId))
        .thenReturn(
            new ResponseEntity<>(
                new T24ResponseBody("mockHeader", Collections.emptyList(), null), HttpStatus.OK));

    TermDepositAccountApplication termDepositAccountApplication =
        new TermDepositAccountApplication();
    termDepositAccountApplication.setT24CustomerId(t24CustomerId);

    // test
    tdaApplicationService.getTermDepositAccountDetails(termDepositAccountApplication);
  }

  @Test(expected = VoltCustomException.class)
  public void shouldThrowVoltCustomExceptionIfCreatingAccountForNonKYCedCustomer()
      throws VoltCustomException {
    // setup
    String voltId = randomNumericString(8);
    when(customerService.createt24CustomerIfnotExist(voltId))
        .thenThrow(
            new VoltCustomException(
                Constants.ERROR_SYSTEM,
                "Can not create account for an inactive customer - voltId:" + voltId,
                HttpStatus.INTERNAL_SERVER_ERROR.value()));
    // test
    tdaApplicationService.create(
        new ModelMapper().map(tdaApplicationDto, TdAccountApplicationDataDto.class), voltId);
  }
}
