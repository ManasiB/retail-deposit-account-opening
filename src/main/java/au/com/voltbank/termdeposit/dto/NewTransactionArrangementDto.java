package au.com.voltbank.termdeposit.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder(builderClassName = "NewTransactionArrangementDtoBuilder")
@JsonDeserialize(builder = NewTransactionArrangementDto.NewTransactionArrangementDtoBuilder.class)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewTransactionArrangementDto {
  private String voltId;
  private String arrangementId;

  @JsonPOJOBuilder(withPrefix = "")
  public static class NewTransactionArrangementDtoBuilder {
    // Lombok will add constructor, setters, build method
  }
}
