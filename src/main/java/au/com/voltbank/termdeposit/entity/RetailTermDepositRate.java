package au.com.voltbank.termdeposit.entity;

import au.com.voltbank.converter.DateTimeAttributeConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Data
@Entity
@Table
public class RetailTermDepositRate {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private Double percentRate;

  private Integer termInMonths;

  private String indexCode;

  @Convert(converter = DateTimeAttributeConverter.class)
  private String effectiveDateTime;
}
