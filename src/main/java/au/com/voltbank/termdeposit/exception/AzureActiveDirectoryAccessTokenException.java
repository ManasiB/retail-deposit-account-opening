package au.com.voltbank.termdeposit.exception;

public class AzureActiveDirectoryAccessTokenException extends RuntimeException {

  public AzureActiveDirectoryAccessTokenException(String clientId, Throwable cause) {
    super(
        String.format(
            "Could not get Azure Active Directory access token for client_id: %s", clientId),
        cause);
  }
}
