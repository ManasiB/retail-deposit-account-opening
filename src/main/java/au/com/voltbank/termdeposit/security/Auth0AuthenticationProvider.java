package au.com.voltbank.termdeposit.security;

import com.auth0.jwk.JwkProviderBuilder;
import com.auth0.spring.security.api.JwtAuthenticationProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Auth0AuthenticationProvider {

  @Value("${volt.jwt.token.leeway:60}")
  private long leeway;

  public JwtAuthenticationProvider initProvider(String auth0UserIssuer, String auth0UserAudience) {
    return new JwtAuthenticationProvider(
            new JwkProviderBuilder(auth0UserIssuer).build(), auth0UserIssuer, auth0UserAudience)
        .withJwtVerifierLeeway(leeway);
  }
}
