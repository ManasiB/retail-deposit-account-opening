package au.com.voltbank.termdeposit.entity;

import au.com.voltbank.converter.DateAttributeConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Builder(builderClassName = "SavingsAccountApplicationBuilder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Data
@Entity
@Table
public class SavingsAccountApplication {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Size(max = 50)
  @Column(length = 64)
  private String accountName;

  @Size(max = 50)
  @Column(length = 32)
  private String accountNumber;

  private String accountBSB;

  private String voltId;

  private String arrangementId;

  private Double rate;

  @Convert(converter = DateAttributeConverter.class)
  private String startDate;

  private String nominatedAccountName;

  private String nominatedAccountBSB;

  private String nominatedAccountNumber;

  private String nominatedAccountId;
}
