package au.com.voltbank.termdeposit.service;

import au.com.voltbank.T24Constants;
import au.com.voltbank.account.model.ProductIdDto;
import au.com.voltbank.account.model.TransactionAccountInfoDto;
import au.com.voltbank.account.model.TransactionAccountListDto;
import au.com.voltbank.t24.T24ResponseBody;
import au.com.voltbank.t24.baas.arrangementservice.model.GetAccountArrangementDetailResponseBodyDto;
import au.com.voltbank.t24.baas.arrangementservice.model.GetAccountArrangementDetailsResponseBodyInnerDto;
import au.com.voltbank.termdeposit.corebanking.service.T24AccountService;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.util.RetailDepositAccountOpeningUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@Slf4j
@Service
public class AccountListService implements AccountList {

  @Autowired private T24AccountService t24AccountService;

  @Autowired private MappingJackson2HttpMessageConverter converter;

  @Override
  public TransactionAccountListDto getAccountsList(Map<String, String> requestParams, String t24Id)
      throws VoltCustomException {

    MultiValueMap<String, String> newRequestParams = transformRequest(requestParams);
    log.debug(
        "Request parameters to get account list transformed to match t24 params {}",
        newRequestParams);
    // voltAuthIdPair is assumed not null, according to Oracle docs
    // https://docs.oracle.com/javase/7/docs/technotes/guides/language/assert.html never assert a
    // method parameter.
    ResponseEntity<T24ResponseBody> t24Response =
        t24AccountService.getAccounts(t24Id, newRequestParams);
    T24ResponseBody t24ResponseBody = t24Response.getBody();
    log.debug("Successfully got the response from T24 for list of accounts");

    TransactionAccountListDto transactionAccountListDto = new TransactionAccountListDto();
    transactionAccountListDto.setData(getTransactionAccountListDto(t24ResponseBody));
    transactionAccountListDto.setPagingInfo(
        RetailDepositAccountOpeningUtil.getHeaderInfo(t24ResponseBody, converter));
    log.debug(
        "Successfully transformed the response header (paging info) from t24 to volt response for Accounts list");

    return transactionAccountListDto;
  }

  private String convertToVoltAmount(String amount) {
    if (amount != null) {
      String[] split = amount.split(",");
      return String.join("", split);
    }
    return amount;
  }

  private MultiValueMap<String, String> transformRequest(Map<String, String> requestParams) {
    final MultiValueMap<String, String> newRequestParams = new LinkedMultiValueMap<>();
    if (!CollectionUtils.isEmpty(requestParams)) {
      for (String key : requestParams.keySet()) {
        if (key.equals(T24Constants.VOLT_PAGESIZE_REQUEST_PARAM)) {
          newRequestParams.put(
              T24Constants.T24_PAGESIZE_REQUEST_PARAM, Arrays.asList(requestParams.get(key)));
        } else if (key.equals(T24Constants.VOLT_PAGESTART_REQUEST_PARAM)) {
          newRequestParams.put(
              T24Constants.T24_PAGESTART_REQUEST_PARAM, Arrays.asList(requestParams.get(key)));
        } else {
          newRequestParams.put(key, Arrays.asList(requestParams.get(key)));
        }
      }
    }

    return newRequestParams;
  }

  private List<TransactionAccountInfoDto> getTransactionAccountListDto(
      T24ResponseBody t24ResponseBody) throws VoltCustomException {

    GetAccountArrangementDetailResponseBodyDto responseAccountArrangementDetails =
        converter.getObjectMapper().convertValue(t24ResponseBody.getBody(),
            new TypeReference<GetAccountArrangementDetailResponseBodyDto>() {
            });

    List<TransactionAccountInfoDto> transactionAccountListDto = new ArrayList<>();
    if (!CollectionUtils.isEmpty(responseAccountArrangementDetails)) {
      for (GetAccountArrangementDetailsResponseBodyInnerDto responseTransaction : responseAccountArrangementDetails) {
        TransactionAccountInfoDto transactionAccountInfoDto = new TransactionAccountInfoDto();
        transactionAccountInfoDto.setAccountName(responseTransaction.getNickname());
        transactionAccountInfoDto.setAccountNumber(responseTransaction.getAcctNumber());
        transactionAccountInfoDto.setAccountType(responseTransaction.getAccountType());
        transactionAccountInfoDto.setInterestPaidYearToDate(
            responseTransaction.getInterestPaidYTD());
        transactionAccountInfoDto.setInterestPaidPreviousYear(
            responseTransaction.getInterestPaidPrevYTD());
        transactionAccountInfoDto.setCurrency(responseTransaction.getCurrency());
        transactionAccountInfoDto.setCurrentBalance(
            convertToVoltAmount(Objects.toString(responseTransaction.getCurrentBalance(), "0")));
        transactionAccountInfoDto.setAvailableBalance(
            convertToVoltAmount(Objects.toString(responseTransaction.getAvailableBalance(), "0")));

        // Amount outstanding is missing

        // Interest rate is missing
        // there are 2 separate interest rates. credit and debit
        // Credit rate is mapped to interest rate
        transactionAccountInfoDto.setInterestRate(responseTransaction.getCreditInterestRate());

        // value date is mapped to account created date
        transactionAccountInfoDto.setAccountCreatedDate(
            RetailDepositAccountOpeningUtil.convertAccountListDateToVoltDate(
                responseTransaction.getValueDate()));

        // There is no mapping for total interest which is available interest
        transactionAccountInfoDto.setBsb(responseTransaction.getSortCode());

        String voltProductId = T24Constants.t24ProductIdToVoltProductId
            .get(responseTransaction.getProductId());
        if (voltProductId != null) {
          transactionAccountInfoDto.setProductId(ProductIdDto.fromValue(voltProductId));
        } else {
          transactionAccountInfoDto.setProductId(ProductIdDto._000);
        }
        transactionAccountListDto.add(transactionAccountInfoDto);
      }
    }
    log.debug(
        "Successfully transformed the response body from t24 to volt response for account list");
    return transactionAccountListDto;
  }
}
