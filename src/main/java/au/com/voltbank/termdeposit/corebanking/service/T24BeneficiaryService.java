package au.com.voltbank.termdeposit.corebanking.service;

import au.com.voltbank.termdeposit.corebanking.T24Client;
import au.com.voltbank.t24.T24RequestBody;
import au.com.voltbank.t24.T24ResponseBody;
import au.com.voltbank.t24.beneficiary.BeneficiaryBodyDto;
import au.com.voltbank.t24.beneficiary.BeneficiaryHeaderDto;
import au.com.voltbank.termdeposit.entity.TermDepositAccountApplication;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.mapper.TermDepositAccountMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@Slf4j
public class T24BeneficiaryService {

  @Autowired private T24Client t24Client;

  private String beneficiaryUrl;

  @Value("${T24_HOST}")
  private String t24_host;

  @PostConstruct
  public void init() {
    beneficiaryUrl = String.format("%s/baaspayment/order/beneficiary", t24_host);
  }

  public ResponseEntity<T24ResponseBody> createBeneficiary(
      TermDepositAccountApplication accountApplication) throws VoltCustomException {
    BeneficiaryBodyDto beneficiaryBodyDto =
        TermDepositAccountMapper.initBeneficiaryDto(accountApplication);
    BeneficiaryHeaderDto headerDto = new BeneficiaryHeaderDto();
    T24RequestBody<BeneficiaryBodyDto, BeneficiaryHeaderDto> t24RequestBody =
        new T24RequestBody<>(headerDto, beneficiaryBodyDto);
    HttpEntity<T24RequestBody> payload =
        new HttpEntity<>(t24RequestBody, t24Client.getHttpRequestHeader());
    return t24Client.post(beneficiaryUrl, payload);
  }
}
