package au.com.voltbank.termdeposit.security;

import com.auth0.spring.security.api.authentication.AuthenticationJsonWebToken;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.security.Principal;
import java.security.Security;
import java.text.SimpleDateFormat;
import java.util.Map;

import static java.util.Base64.getDecoder;

/**
 * This class it to be used specifically for T24 OIDC token claim encryption only. This is because
 * it does not abide by best practices such as salting the password and using an initialization
 * vector.
 *
 * @author shane.doolan@voltbank.com.au
 */
@Slf4j
@Component
public class T24ClaimDecryptor {
  private static final int OFFSET = 0;
  private static final int ITERATIONS = 0;
  private static final byte[] SALT = new byte[0];
  private static final String ALGORITHM = "PBEWITHSHA256AND256BITAES-CBC-BC";
  private static final String TRANSFORMATION = "AES/GCM/NoPadding";
  private static final String PROVIDER = "BC";
  private String TOKEN_KEY_T24_CUSTOMER_ID = "https://voltbank.com.au/t_id";
  public static final String DATE_FORMAT = "yyyy-MM-dd";

  private Cipher decryptionCipher;
  private MappingJackson2HttpMessageConverter converter;

  @Value("${encrypt.claim.password}")
  private String password;

  @PostConstruct
  public void init() throws Exception {
    Security.addProvider(new BouncyCastleProvider());
    PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray());
    SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(ALGORITHM);
    decryptionCipher = Cipher.getInstance(TRANSFORMATION, PROVIDER);
    decryptionCipher.init(
        Cipher.DECRYPT_MODE,
        keyFactory.generateSecret(keySpec),
        new PBEParameterSpec(SALT, ITERATIONS));
    converter = new MappingJackson2HttpMessageConverter();
    ObjectMapper objectMapper = new ObjectMapper();
    SimpleDateFormat smt = new SimpleDateFormat(DATE_FORMAT);
    objectMapper.setDateFormat(smt);
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    converter.setObjectMapper(objectMapper);
  }

  public String decrypt(String encryptedText)
      throws BadPaddingException, IllegalBlockSizeException {
    log.info("Decryption Password:" + password);
    byte[] bytes = Base64.decodeBase64(encryptedText.getBytes());
    byte[] data = decryptionCipher.doFinal(bytes, OFFSET, bytes.length);
		String decryptedString = new String(data);
		log.info("decrypt:after decryption {}",decryptedString);
    return decryptedString;
  }

  public String getEncryptedT24CustomerIdFromToken(Principal principal) throws Exception {
    ObjectMapper mapper = converter.getObjectMapper();
    AuthenticationJsonWebToken authenticationJsonWebToken = (AuthenticationJsonWebToken) principal;
    String token = authenticationJsonWebToken.getToken();
    if (StringUtils.isEmpty(token)) {
      return null;
    }

    Map map = mapper.readValue(new String(getDecoder().decode(token.split("\\.")[1])), Map.class);
		String decrypt = decrypt((String) map.get(TOKEN_KEY_T24_CUSTOMER_ID));
		log.info("readValue: The id after decrypt {} " , decrypt);

		return decrypt((String) map.get(TOKEN_KEY_T24_CUSTOMER_ID));
  }
}
