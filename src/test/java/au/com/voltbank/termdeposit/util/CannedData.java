package au.com.voltbank.termdeposit.util;

import au.com.voltbank.Constants;
import au.com.voltbank.account.model.PagingInfoDto;
import au.com.voltbank.account.model.ProductIdDto;
import au.com.voltbank.account.model.SavingsAccountApplicationDataDto;
import au.com.voltbank.account.model.TdAccountApplicationDataDto;
import au.com.voltbank.account.model.TransactionAccountApplicationDataDto;
import au.com.voltbank.account.model.TransactionAccountInfoDto;
import au.com.voltbank.account.model.TransactionAccountListDto;
import au.com.voltbank.customer.model.CustomerBankingCreateResponseDto;
import au.com.voltbank.security.jwt.KeyBucket;
import au.com.voltbank.sis.model.VoltAuthIdPairDto;
import au.com.voltbank.sis.model.VoltT24IdPairDto;
import au.com.voltbank.t24.account.AccountDetailBodyDto;
import au.com.voltbank.termdeposit.config.VoltTermDepositIndexCode;
import au.com.voltbank.t24.T24ResponseBody;
import au.com.voltbank.t24.account.CreateTDAccountResponseBodyDto;
import au.com.voltbank.t24.account.TDAccountDetailBodyDto;
import au.com.voltbank.termdeposit.dto.Customer;
import au.com.voltbank.termdeposit.entity.SavingsAccountApplication;
import au.com.voltbank.termdeposit.entity.TermDepositAccountApplication;
import au.com.voltbank.termdeposit.entity.TransactionAccountApplication;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.tyro.oss.randomdata.RandomString;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.tyro.oss.randomdata.RandomString.randomNumericString;
import static com.tyro.oss.randomdata.RandomString.randomUUID;
import static io.jsonwebtoken.SignatureAlgorithm.HS256;

public class CannedData {

  public static final String TERM_IN_MONTHS_12 = "12";
  public static final String ARRANGEMENT_ID = "AA18275V435F";
  public static final String EXPIRED_AZURE_TOKEN =
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IndVTG1ZZnNxZFF1V3RWXy1oeFZ0REpKWk00USIsImtpZCI6IndVTG1ZZnNxZFF1V3RWXy1oeFZ0REpKWk00USJ9.eyJhdWQiOiIwMDAwMDAwMi0wMDAwLTAwMDAtYzAwMC0wMDAwMDAwMDAwMDAiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC83MTdhNDQ4OS0zZWIyLTQyNDktYWY5Yy0zM2VkNjlmNDdiMWQvIiwiaWF0IjoxNTQ0NDE5NzUwLCJuYmYiOjE1NDQ0MTk3NTAsImV4cCI6MTU0NDQyMzY1MCwiYWlvIjoiNDJSZ1lGakZYbmF6eS9oaUhjZHRLVVZXMXBldEFBPT0iLCJhcHBpZCI6IjMyZDk1YTU3LWVjMjMtNDE1MS1iOGRmLWMyZGY2NzVlNGE3NSIsImFwcGlkYWNyIjoiMSIsImlkcCI6Imh0dHBzOi8vc3RzLndpbmRvd3MubmV0LzcxN2E0NDg5LTNlYjItNDI0OS1hZjljLTMzZWQ2OWY0N2IxZC8iLCJvaWQiOiIxYzMxYTRjZi1hMzkwLTQwMjYtYjFmNS1hMjJlM2FkOGFmNTMiLCJzdWIiOiIxYzMxYTRjZi1hMzkwLTQwMjYtYjFmNS1hMjJlM2FkOGFmNTMiLCJ0ZW5hbnRfcmVnaW9uX3Njb3BlIjoiT0MiLCJ0aWQiOiI3MTdhNDQ4OS0zZWIyLTQyNDktYWY5Yy0zM2VkNjlmNDdiMWQiLCJ1dGkiOiI3b3VjNHVlYjNVLUk1MGM2ZUE1UEFBIiwidmVyIjoiMS4wIn0.STyFXx_XScZ2p_x-OXd55uJurmEaKphSEq41pZt-HAVaQfC0-9zYXCBO8yFn1fIUhtolQiyCBXIpdfO4Fn9jabZ9pGy5ZndRAj9uRzJoebsNs2nz4NxWgAo07U7J2hwhrD9udsVnoRxaGHkel41CMhZggx-5bgJx5_UZzD8FPMe2liWc5kRze5x2xdeg2TDjXpiRLUfKewuVGQFCgA3oi0EHUnB6xYpwp4VPzV-CUiWCoQ48iKzM9tEvJd75lhPkT1Neh-QEtYVtsR8mWhx0ISIVgDIiPNzHhxCf3rMHL4l_7c1Y7jP_hdOLHk-8SUqLHvLwyWggo7XthQlMbb2NDg";
  public static final KeyBucket KEY_BUCKET;
  public static final String MOCK_T24_ID = "mock_t24_id";
  public static final String MOCK_ACCOUNT_NUMBER = "a_mock_account_number";
  public static final String MOCKED_ACCT_NUMBER_SAVER = "100000085";
  public static final String MOCK_ACCT_NUMBER_SAVER_1 = "100000697";
  public static final String MOCKED_ACCT_NUMBER_TRANS = "100000093";

  private static final String keyJson =
    "  {\"keys\": [\n"
        + "    {\n"
        + "      \"kty\": \"RSA\",\n"
        + "      \"use\": \"sig\",\n"
        + "      \"kid\": \"wULmYfsqdQuWtV_-hxVtDJJZM4Q\",\n"
        + "      \"x5t\": \"wULmYfsqdQuWtV_-hxVtDJJZM4Q\",\n"
        + "      \"n\": \"mtRp-flf1CRQNELJgexsre0aBxDUaIcRysmaYBxWxgH0r2cfdwoe7edWto5rMbkSkXVxxV8969Cz6WDmX3SmOSAdVMu3nVXcIOVIbnII5Rz9W6j5YrInyP_FPQ_XuZ4WMfXuItGXN8bofel1ehBbhS9YEvgFpfPBCvSD1JVCrUD_YZtXFGPS0--9ncTxNPEcso3NJrp-FDESZ8f8nfjRB1mfcePwGdKsuzndL3FGPbLn8hpO4ZXn_KysIma4XBnqvbs0X0AqWEG_g7abL63HM9Ci7QO7PY6Rm5XDci-Kh-conWcKXv5zt5NOKgjOiepr5VN3bjSl8cpT2gtZStFtSw\",\n"
        + "      \"e\": \"AQAB\",\n"
        + "      \"x5c\": [\n"
        + "        \"MIIDBTCCAe2gAwIBAgIQKGZsKfAUzaJHVantyrwVdzANBgkqhkiG9w0BAQsFADAtMSswKQYDVQQDEyJhY2NvdW50cy5hY2Nlc3Njb250cm9sLndpbmRvd3MubmV0MB4XDTE4MTAwMTAwMDAwMFoXDTIwMTAwMTAwMDAwMFowLTErMCkGA1UEAxMiYWNjb3VudHMuYWNjZXNzY29udHJvbC53aW5kb3dzLm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJrUafn5X9QkUDRCyYHsbK3tGgcQ1GiHEcrJmmAcVsYB9K9nH3cKHu3nVraOazG5EpF1ccVfPevQs+lg5l90pjkgHVTLt51V3CDlSG5yCOUc/Vuo+WKyJ8j/xT0P17meFjH17iLRlzfG6H3pdXoQW4UvWBL4BaXzwQr0g9SVQq1A/2GbVxRj0tPvvZ3E8TTxHLKNzSa6fhQxEmfH/J340QdZn3Hj8BnSrLs53S9xRj2y5/IaTuGV5/ysrCJmuFwZ6r27NF9AKlhBv4O2my+txzPQou0Duz2OkZuVw3IviofnKJ1nCl7+c7eTTioIzonqa+VTd240pfHKU9oLWUrRbUsCAwEAAaMhMB8wHQYDVR0OBBYEFFve38eLO2PUMXcqbBC/YDaayIbrMA0GCSqGSIb3DQEBCwUAA4IBAQCWhrem9A2NrOiGesdzbDy2K3k0oxjWMlM/ZGIfPMPtZl4uIzhc+vdDVVFSeV8SKTOEIMjMOJTQ3GJpZEHYlyM7UsGWiMSXqzG5HUxbkPvuEFHx7cl9Ull3AEymB2oVPC9DPtLUXPyDH898QgEEVhAEI+JZc1Yd6mAlY/5nOw5m2Yqm+84JOPWLgFDqfVmz/MH27LS1rnzzc+0hhcm/Nv/x7FmpOeRfh00BjCA4PogJlpjl/z/6+GTYcYFsvKE3jmmXka8tQbBOHgAlMnamFA8xGeDok6QaxOELu8NSWzvyZXM2lJK5WFQPHF2hjnNXs6+RxOovG55Ybpo52c2frhNZ\"\n"
        + "      ]\n"
        + "    },\n"
        + "    {\n"
        + "      \"kty\": \"RSA\",\n"
        + "      \"use\": \"sig\",\n"
        + "      \"kid\": \"nbCwW11w3XkB-xUaXwKRSLjMHGQ\",\n"
        + "      \"x5t\": \"nbCwW11w3XkB-xUaXwKRSLjMHGQ\",\n"
        + "      \"n\": \"u98KvoUHfs2z2YJyfkJzaGFYM58eD0epHfETTwNDl6AL_cTfOklcxM4jrLWvVqqp2sHaH0gFpYPyovN-_akmE_4fkc0Vw_wGM5jDP-jnOJ1vBvbFoF7uBAy4r3ln2ey1PoGUhpkXdDawhIfdAbc7WLtyopHNWQXI336rXiwjvcjL8dHhievDOktsAsilADP5wJT0lyTifONPZOq-XWCw9FtXAQr7DniOC5uDuUaL0mM1UJChiCrDmFOAf6CNdu2SwLinXYauqM9ORElKYEChoEfi51fcsmlsn4mtNPkxstvR7OJiJBpvk7FLeiaBtMnsO5x30DPgrhAagrVn3IaKRQ\",\n"
        + "      \"e\": \"AQAB\",\n"
        + "      \"x5c\": [\n"
        + "        \"MIIDBTCCAe2gAwIBAgIQV68hSN9DrrlCaA3NJ0bnfDANBgkqhkiG9w0BAQsFADAtMSswKQYDVQQDEyJhY2NvdW50cy5hY2Nlc3Njb250cm9sLndpbmRvd3MubmV0MB4XDTE4MTExMTAwMDAwMFoXDTIwMTExMTAwMDAwMFowLTErMCkGA1UEAxMiYWNjb3VudHMuYWNjZXNzY29udHJvbC53aW5kb3dzLm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALvfCr6FB37Ns9mCcn5Cc2hhWDOfHg9HqR3xE08DQ5egC/3E3zpJXMTOI6y1r1aqqdrB2h9IBaWD8qLzfv2pJhP+H5HNFcP8BjOYwz/o5zidbwb2xaBe7gQMuK95Z9nstT6BlIaZF3Q2sISH3QG3O1i7cqKRzVkFyN9+q14sI73Iy/HR4YnrwzpLbALIpQAz+cCU9Jck4nzjT2Tqvl1gsPRbVwEK+w54jgubg7lGi9JjNVCQoYgqw5hTgH+gjXbtksC4p12GrqjPTkRJSmBAoaBH4udX3LJpbJ+JrTT5MbLb0eziYiQab5OxS3omgbTJ7Ducd9Az4K4QGoK1Z9yGikUCAwEAAaMhMB8wHQYDVR0OBBYEFGWLmYFSm5Exg9VcAGSg5sFE1mXgMA0GCSqGSIb3DQEBCwUAA4IBAQB0yTGzyhx+Hz2vwBSo5xCkiIom6h7b946KKiWvgBLeOvAuxOsB15N+bbf51sUfUJ6jBaa1uJjJf27dxwH0oUe2fcmEN76QSrhULYe+k5yyJ7vtCnd6sHEfn9W6iRozv0cb48tESOTlFuwbYDVW+YZxzM9EQHC32CjugURzuN9/rf6nJ9etSeckRMO8QPqyIi4e5sGSDYExxNs7J4prhIbtYT4NRiqc4nWzA/p5wSYOUgAZMSTLD/beSI81UN1Ao9VBBJu3v83d62WL3zHSbpUwtG/utNhSi/n/7Q94claEWJVhBx6LiA1hrU6YZkjRGqBOrWIZkSkh75xW6Xujocy4\"\n"
        + "      ]\n"
        + "    },\n"
        + "    {\n"
        + "      \"kty\": \"RSA\",\n"
        + "      \"use\": \"sig\",\n"
        + "      \"kid\": \"M6pX7RHoraLsprfJeRCjSxuURhc\",\n"
        + "      \"x5t\": \"M6pX7RHoraLsprfJeRCjSxuURhc\",\n"
        + "      \"n\": \"xHScZMPo8FifoDcrgncWQ7mGJtiKhrsho0-uFPXg-OdnRKYudTD7-Bq1MDjcqWRf3IfDVjFJixQS61M7wm9wALDj--lLuJJ9jDUAWTA3xWvQLbiBM-gqU0sj4mc2lWm6nPfqlyYeWtQcSC0sYkLlayNgX4noKDaXivhVOp7bwGXq77MRzeL4-9qrRYKjuzHfZL7kNBCsqO185P0NI2Jtmw-EsqYsrCaHsfNRGRrTvUHUq3hWa859kK_5uNd7TeY2ZEwKVD8ezCmSfR59ZzyxTtuPpkCSHS9OtUvS3mqTYit73qcvprjl3R8hpjXLb8oftfpWr3hFRdpxrwuoQEO4QQ\",\n"
        + "      \"e\": \"AQAB\",\n"
        + "      \"x5c\": [\n"
        + "        \"MIIC8TCCAdmgAwIBAgIQfEWlTVc1uINEc9RBi6qHMjANBgkqhkiG9w0BAQsFADAjMSEwHwYDVQQDExhsb2dpbi5taWNyb3NvZnRvbmxpbmUudXMwHhcNMTgxMDE0MDAwMDAwWhcNMjAxMDE0MDAwMDAwWjAjMSEwHwYDVQQDExhsb2dpbi5taWNyb3NvZnRvbmxpbmUudXMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDEdJxkw+jwWJ+gNyuCdxZDuYYm2IqGuyGjT64U9eD452dEpi51MPv4GrUwONypZF/ch8NWMUmLFBLrUzvCb3AAsOP76Uu4kn2MNQBZMDfFa9AtuIEz6CpTSyPiZzaVabqc9+qXJh5a1BxILSxiQuVrI2BfiegoNpeK+FU6ntvAZervsxHN4vj72qtFgqO7Md9kvuQ0EKyo7Xzk/Q0jYm2bD4SypiysJoex81EZGtO9QdSreFZrzn2Qr/m413tN5jZkTApUPx7MKZJ9Hn1nPLFO24+mQJIdL061S9LeapNiK3vepy+muOXdHyGmNctvyh+1+laveEVF2nGvC6hAQ7hBAgMBAAGjITAfMB0GA1UdDgQWBBQ5TKadw06O0cvXrQbXW0Nb3M3h/DANBgkqhkiG9w0BAQsFAAOCAQEAI48JaFtwOFcYS/3pfS5+7cINrafXAKTL+/+he4q+RMx4TCu/L1dl9zS5W1BeJNO2GUznfI+b5KndrxdlB6qJIDf6TRHh6EqfA18oJP5NOiKhU4pgkF2UMUw4kjxaZ5fQrSoD9omjfHAFNjradnHA7GOAoF4iotvXDWDBWx9K4XNZHWvD11Td66zTg5IaEQDIZ+f8WS6nn/98nAVMDtR9zW7Te5h9kGJGfe6WiHVaGRPpBvqC4iypGHjbRwANwofZvmp5wP08hY1CsnKY5tfP+E2k/iAQgKKa6QoxXToYvP7rsSkglak8N5g/+FJGnq4wP6cOzgZpjdPMwaVt5432GA==\"\n"
        + "      ]\n"
        + "    }\n"
        + "  ]\n"
        + "}";

  static {
    try {
      ObjectMapper mapper = new ObjectMapper();
      mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
      KEY_BUCKET = mapper.readValue(keyJson, KeyBucket.class);
    } catch (IOException e) {
      throw new RuntimeException("Failed to initialize Key Bucket!", e);
    }
  }

  public static Customer getCannedCustomerEntity() {
    return Customer.builder().voltId(randomUUID()).t24CustomerId(randomNumericString(8)).build();
  }

  public static TermDepositAccountApplication getDepositAccountApplication() {
    return TermDepositAccountApplication.builder()
        .accountName("Term deposit test account")
        .accountNumber(randomNumericString(9))
        .userId("666666")
        .t24CustomerId("10000060")
        .arrangementId(randomNumericString(10))
        .status("prospect")
        .actionOnMaturity("REINVEST")
        .termInMonths(TERM_IN_MONTHS_12)
        .rates(2.5)
        .indexCode(VoltTermDepositIndexCode.YEARLY.getValue())
        .amountInWholeDollars(6000)
        .startDate(LocalDate.now().format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT)))
        .linkedAccountName("SUSHANT VERMA")
        .linkedAccountBSB("806043")
        .linkedAccountNumber("12345678")
        .build();
  }

  public static SavingsAccountApplication getSavingsAccountApplication() {
    return SavingsAccountApplication.builder()
        .accountName("Savings test account")
        .arrangementId(randomNumericString(10))
        .accountNumber("1234")
        .accountBSB("000-000")
        .voltId("456789")
        .rate(2.5)
        .startDate(LocalDate.now().format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT)))
        .build();
  }

  public static TransactionAccountApplication getTransactionAccountApplication() {
    return TransactionAccountApplication.builder()
        .accountName("Transaction test account")
        .t24CustomerId("10000060")
        .arrangementId(randomNumericString(10))
        .build();
  }

  public static TdAccountApplicationDataDto getTdAccountApplicationDataDto() {
    TdAccountApplicationDataDto dto =
        new ModelMapper().map(getDepositAccountApplication(), TdAccountApplicationDataDto.class);
    dto.setPercentRate("2.5");
    return dto;
  }

  public static SavingsAccountApplicationDataDto getSavingsAccountApplicationDataDto() {
    return new SavingsAccountApplicationDataDto()
        .percentRate("2.5")
        .accountName("Shane's Holiday Bucket")
        .nominatedAccountBSB("969100")
        .nominatedAccountNumber("12345678")
        .nominatedAccountName("Shane's Linked Account")
        .indexCode("04");
  }

  public static TransactionAccountApplicationDataDto getTransactionAccountApplicationDataDto() {
    return new TransactionAccountApplicationDataDto().accountName("Everyday Account");
  }

  public static CreateTDAccountResponseBodyDto getCreateAccountResponseBody() throws IOException {
    String json =
        "{\"arrangementActivity\":{\"arrangementId\":\""
            + ARRANGEMENT_ID
            + "\",\"activityId\":\"DEPOSITS-NEW-ARRANGEMENT\",\"productId\":\"VB.TD.RETAIL.MATURITY\",\"parties\":[{\"partyRole\":\"OWNER\",\"partyId\":\"10000656\"}],\"currencyId\":\"AUD\",\"effectiveDate\":\"2018-10-02\"}}";
    ObjectMapper mapper = new ObjectMapper();
    return mapper.readValue(json, CreateTDAccountResponseBodyDto.class);
  }

  public static List<LinkedHashMap<String, String>> getTDAccountResponseBodyLinkedHashMap()
      throws IOException {
    String json =
        "[{\n"
            + "            \"accountName\": \"Volt Retail Term Deposits\",\n"
            + "            \"amountOutstanding\": \"0\",\n"
            + "            \"description\": \"Volt Term Deposits(Retail)\",\n"
            + "            \"fixedExpiryDate\": \"20190325\",\n"
            + "            \"productName\": \"Volt Term Deposits(Retail)\",\n"
            + "            \"availableBalance\": \"0\",\n"
            + "            \"nextInterestPaymentDate\": \"20190325\",\n"
            + "            \"reference\": \""
            + ARRANGEMENT_ID
            + "\",\n"
            + "            \"balance\": \"0\",\n"
            + "            \"principalBalance\": \"6000\",\n"
            + "            \"maturityDate\": \"20190625\",\n"
            + "            \"nickname\": \"Volt Retail Term Deposits\",\n"
            + "            \"currency\": \"AUD\",\n"
            + "            \"sortcode\": \"517000\",\n"
            + "            \"interestRate\": \"2.01\",\n"
            + "            \"product\": \"Volt Term Deposits(Retail)\",\n"
            + "            \"amount\": \"0\",\n"
            + "            \"productId\": \"6001\",\n"
            + "            \"currentBalance\": \"0\",\n"
            + "            \"valueDate\": \"20180925\",\n"
            + "            \"interestOnlyExpiryDate\": \"20190325\",\n"
            + "            \"remainingTerm\": \"Matures in 5 Months 22 Days\",\n"
            + "            \"relationShipFlag\": \"OWNER\",\n"
            + "            \"interestPaidPrevYear\": \"0\",\n"
            + "            \"originalTerm\": \"6M\",\n"
            + "            \"interestPaidYTD\": \"0\",\n"
            + "            \"bookingDate\": \"20180324\",\n"
            + "            \"account\": \"100004335\",\n"
            + "            \"startDate\": \"20180924\"\n"
            + "        }]";

    return new ObjectMapper()
        .readValue(json, new TypeReference<List<LinkedHashMap<String, String>>>() {});
  }

  public static TDAccountDetailBodyDto getTDAccountResponseBody() throws IOException {
    List<LinkedHashMap<String, String>> result = getTDAccountResponseBodyLinkedHashMap();
    return new ObjectMapper().convertValue(result.get(0), TDAccountDetailBodyDto.class);
  }

  public static List<LinkedHashMap<String, String>> getAccountDetailsBodyDtoLinkedHashMap()
      throws IOException {
    String json =
        "[\n"
            + "      {\n"
            + "        \"debitInterestRate\": \"2.5\",\n"
            + "        \"creditInterestRate\": \"2.5\",\n"
            + "        \"companyCode\": \"AU0010001\",\n"
            + "        \"productId\": \"1001\",\n"
            + "        \"amountOutstanding\": \"0\",\n"
            + "        \"accountTitle\": \"Saver Account Individual\",\n"
            + "        \"accountType\": \"Volt Saver Account-Individual\",\n"
            + "        \"description\": \"Volt Saver Account-Individual\",\n"
            + "        \"arrangementId\": \"AA190249RD4D\",\n"
            + "        \"valueDate\": \"20190124\",\n"
            + "        \"sortCode\": \"517000\",\n"
            + "        \"arrangementStatus\": \"AUTH\",\n"
            + "        \"relationShipFlag\": \"OWNER\",\n"
            + "        \"acctNumber\": \"100007999\",\n"
            + "        \"principalBalance\": \"0\",\n"
            + "        \"contractDate\": \"20180815\",\n"
            + "        \"remainingLoanTerm\": \"Matures in\",\n"
            + "        \"nextDueDate\": \"20190201\",\n"
            + "        \"nickname\": \"Saver Account Individual\",\n"
            + "        \"bookingDate\": \"20190124\"\n"


            + "      },\n"
            + "      {\n"
            + "        \"debitInterestRate\": \"0.00\",\n"
            + "        \"creditInterestRate\": \"2.5\",\n"
            + "        \"companyCode\": \"AU0010001\",\n"
            + "        \"productId\": \"1021\",\n"
            + "        \"amountOutstanding\": \"0\",\n"
            + "        \"accountTitle\": \"Transaction Account Individual\",\n"
            + "        \"accountType\": \"Volt Transaction Account-Individual\",\n"
            + "        \"description\": \"Volt Transaction Account-Individual\",\n"
            + "        \"arrangementId\": \"AA1902435WGP\",\n"
            + "        \"valueDate\": \"20190124\",\n"
            + "        \"sortCode\": \"517000\",\n"
            + "        \"relationShipFlag\": \"OWNER\",\n"
            + "        \"acctNumber\": \"100008005\",\n"
            + "        \"principalBalance\": \"0\",\n"
            + "        \"arrangementStatus\": \"AUTH\",\n"
            + "        \"contractDate\": \"20180815\",\n"
            + "        \"remainingLoanTerm\": \"Matures in\",\n"
            + "        \"nextDueDate\": \"20190201\",\n"
            + "        \"nickname\": \"Transaction Account Individual\",\n"
            + "        \"bookingDate\": \"20190124\"\n"

            + "      }\n"
            + "    ]";

    return new ObjectMapper()
        .readValue(json, new TypeReference<List<LinkedHashMap<String, String>>>() {});
  }

  public static List<AccountDetailBodyDto> getAccountDetailsBodyDto() throws IOException {
    List<LinkedHashMap<String, String>> result = getAccountDetailsBodyDtoLinkedHashMap();
    return new ObjectMapper()
        .convertValue(result, new TypeReference<List<AccountDetailBodyDto>>() {});
  }

  public static HttpHeaders getAzureTokenRequestHeader() {
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");
    httpHeaders.set("Ocp-Apim-Subscription-Key", "fake key");
    httpHeaders.set("Credentials", "fake secret");
    httpHeaders.set("Authorization", "Bearer fake token");
    return httpHeaders;
  }

  public static List<LinkedHashMap> getBarefootInvestAccountsLinkedHashMap() throws IOException {
    String json =
        "[{\"id\":null,\"accountName\":\"Smile\",\"accountNumber\":null,\"accountBSB\":null,\"voltId\":\"aaaaa\",\"t24CustomerId\":\"84671570\",\"arrangementId\":\"mocked_arrangement_idSmile\",\"rate\":2.5,\"startDate\":null,\"nominatedAccountName\":null,\"nominatedAccountBSB\":null,\"nominatedAccountNumber\":null,\"nominatedAccountId\":null},{\"id\":null,\"accountName\":\"Fire Extinguisher\",\"accountNumber\":null,\"accountBSB\":null,\"voltId\":\"aaaaa\",\"t24CustomerId\":\"84671570\",\"arrangementId\":\"mocked_arrangement_idFire Extinguisher\",\"rate\":2.5,\"startDate\":null,\"nominatedAccountName\":null,\"nominatedAccountBSB\":null,\"nominatedAccountNumber\":null,\"nominatedAccountId\":null},{\"id\":null,\"accountName\":\"Mojo\",\"accountNumber\":null,\"accountBSB\":null,\"voltId\":\"aaaaa\",\"t24CustomerId\":\"84671570\",\"arrangementId\":\"mocked_arrangement_idMojo\",\"rate\":2.5,\"startDate\":null,\"nominatedAccountName\":null,\"nominatedAccountBSB\":null,\"nominatedAccountNumber\":null,\"nominatedAccountId\":null},{\"id\":null,\"accountName\":\"Daily Expenses\",\"accountNumber\":null,\"accountBSB\":null,\"voltId\":\"aaaaa\",\"t24CustomerId\":\"84671570\",\"arrangementId\":\"mocked_arrangement_idDaily Expenses\"},{\"id\":null,\"accountName\":\"Splurge\",\"accountNumber\":null,\"accountBSB\":null,\"voltId\":\"aaaaa\",\"t24CustomerId\":\"84671570\",\"arrangementId\":\"mocked_arrangement_idSplurge\"}]";
    ObjectMapper mapper = new ObjectMapper();
    return mapper.readValue(json, List.class);
  }

  public static Jws<Claims> getIncorrectIsuuerJws() throws UnsupportedEncodingException {
    String jwt =
        Jwts.builder()
            .setExpiration(new Date(System.currentTimeMillis() + 3600000))
            .claim("iss", "Non Azure issuer")
            .signWith(HS256, "secret".getBytes(StandardCharsets.UTF_8))
            .compact();

    return getJws(jwt);
  }

  public static Jws<Claims> getcorrectIsuuerJws() throws UnsupportedEncodingException {
    String jwt =
        Jwts.builder()
            .setExpiration(new Date(System.currentTimeMillis() + 3600000))
            .claim("iss", "https://sts.windows.net/62c58725-7947-4a6b-b212-bf29cdabef7b/")
            .signWith(HS256, "secret".getBytes(StandardCharsets.UTF_8))
            .compact();

    return getJws(jwt);
  }

  private static Jws<Claims> getJws(String jwt) throws UnsupportedEncodingException {
    return Jwts.parser().setSigningKey("secret".getBytes(StandardCharsets.UTF_8)).parseClaimsJws(jwt);
  }

  public static CustomerBankingCreateResponseDto getCustomerBankingDetailsDto() {
    CustomerBankingCreateResponseDto dto = new CustomerBankingCreateResponseDto();
    dto.setStatus(CustomerBankingCreateResponseDto.StatusEnum.ACTIVE);
    dto.setT24CustomerId("mockedT24CustomerId");
    dto.setVoltId("mockedVoltId");
    return dto;
  }

  public static String getMockT24CustomerId() {
    return "mockedT24CustomerId";
  }

  public static String getMockVoltId() {
    return "mockedVoltId";
  }

  public static T24ResponseBody getMockTransactionListDtoWIthValidData() throws Exception {
    String mockJson =
        "{\n"
            + "  \"header\": {\n"
            + "    \"audit\": {\n"
            + "      \"T24_time\": 385,\n"
            + "      \"parse_time\": 0\n"
            + "    },\n"
            + "    \"page_start\": 0,\n"
            + "    \"total_size\": 2,\n"
            + "    \"page_token\": \"d750782a-e998-47d8-ad6a-368153a45ab3\",\n"
            + "    \"page_size\": 50\n"
            + "  },\n"
            + "  \"body\": [\n"
            + "    {\n"
            + "      \"debitCreditIndicator\": \"Debit\",\n"
            + "      \"balance\": \"6,502.64\",\n"
            + "      \"transactionAmount\": \"2.51\",\n"
            + "      \"bankAccountNumber\": \"100000341\",\n"
            + "      \"transactionCode\": \"891\",\n"
            + "      \"valueDate\": \"01/05/2019\",\n"
            + "      \"postingDate\": \"01/05/2019\",\n"
            + "      \"TransactionNarative\": \"Withholding Tax - Resident\",\n"
            + "      \"TransactDescription\": \"Debit Arrangement\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"debitCreditIndicator\": \"Credit\",\n"
            + "      \"balance\": \"6,507.99\",\n"
            + "      \"transactionAmount\": \"5.35\",\n"
            + "      \"bankAccountNumber\": \"100000341\",\n"
            + "      \"transactionCode\": \"831\",\n"
            + "      \"valueDate\": \"01/05/2019\",\n"
            + "      \"postingDate\": \"02/05/2019\",\n"
            + "      \"TransactionNarative\": \"Credit Interest\",\n"
            + "      \"TransactDescription\": \"Interest - Capitalise\"\n"
            + "    }\n"
            + "  ]\n"
            + "}";
    ObjectMapper objectMapper = new ObjectMapper();

    T24ResponseBody t24ResponseBody = objectMapper.readValue(mockJson, T24ResponseBody.class);
    return t24ResponseBody;
  }

  public static T24ResponseBody getMockTransactionListWithNullAndInValidTransactionCode()
      throws Exception {
    String mockJson =
        "{\n"
            + "  \"header\": {\n"
            + "    \"audit\": {\n"
            + "      \"T24_time\": 385,\n"
            + "      \"parse_time\": 0\n"
            + "    },\n"
            + "    \"page_start\": 0,\n"
            + "    \"total_size\": 2,\n"
            + "    \"page_token\": \"d750782a-e998-47d8-ad6a-368153a45ab3\",\n"
            + "    \"page_size\": 50\n"
            + "  },\n"
            + "  \"body\": [\n"
            + "    {\n"
            + "      \"debitCreditIndicator\": \"Debit\",\n"
            + "      \"balance\": \"6,502.64\",\n"
            + "      \"transactionAmount\": \"2.51\",\n"
            + "      \"bankAccountNumber\": \"100000341\",\n"
            + "      \"valueDate\": \"01/05/2019\",\n"
            + "      \"postingDate\": \"01/05/2019\",\n"
            + "      \"TransactionNarative\": \"Withholding Tax - Resident\",\n"
            + "      \"TransactDescription\": \"Debit Arrangement\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"debitCreditIndicator\": \"Credit\",\n"
            + "      \"balance\": \"6,507.99\",\n"
            + "      \"transactionAmount\": \"5.35\",\n"
            + "      \"bankAccountNumber\": \"100000341\",\n"
            + "      \"transactionCode\": \"09187309138\",\n"
            + "      \"valueDate\": \"01/05/2019\",\n"
            + "      \"postingDate\": \"02/05/2019\",\n"
            + "      \"TransactionNarative\": \"Credit Interest\",\n"
            + "      \"TransactDescription\": \"Interest - Capitalise\"\n"
            + "    }\n"
            + "  ]\n"
            + "}";
    ObjectMapper objectMapper = new ObjectMapper();

    T24ResponseBody t24ResponseBody = objectMapper.readValue(mockJson, T24ResponseBody.class);
    return t24ResponseBody;
  }

  public static T24ResponseBody getMockTransactionListDtoWIthEmptyData() throws Exception {
    String mockJson =
        "{\n"
            + "  \"header\": {\n"
            + "    \"audit\": {\n"
            + "      \"T24_time\": 385,\n"
            + "      \"parse_time\": 0\n"
            + "    },\n"
            + "    \"page_start\": 0,\n"
            + "    \"total_size\": 2,\n"
            + "    \"page_token\": \"d750782a-e998-47d8-ad6a-368153a45ab3\",\n"
            + "    \"page_size\": 50\n"
            + "  }\n"
            + "}";

    ObjectMapper objectMapper = new ObjectMapper();
    T24ResponseBody t24ResponseBody = objectMapper.readValue(mockJson, T24ResponseBody.class);
    return t24ResponseBody;
  }

  public static T24ResponseBody getMockTransactionListDtoWithUnorderedTransactionDate()
      throws Exception {
    String mockJson =
        "{\n"
            + "  \"header\": {\n"
            + "    \"audit\": {\n"
            + "      \"T24_time\": 385,\n"
            + "      \"parse_time\": 0\n"
            + "    },\n"
            + "    \"page_start\": 0,\n"
            + "    \"total_size\": 2,\n"
            + "    \"page_token\": \"d750782a-e998-47d8-ad6a-368153a45ab3\",\n"
            + "    \"page_size\": 50\n"
            + "  },\n"
            + "  \"body\": [\n"
            + "    {\n"
            + "      \"postingDate\": \"01/05/2018\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"postingDate\": \"30/05/2018\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"postingDate\": \"01/06/2019\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"postingDate\": \"17/05/2019\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"postingDate\": \"01/12/2017\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"postingDate\": \"17/11/2017\",\n"
            + "      \"transactionCode\": \"891\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"postingDate\": \"17/11/2017\",\n"
            + "      \"transactionCode\": \"466\"\n"
            + "    }\n"
            + "  ]\n"
            + "}";
    ObjectMapper objectMapper = new ObjectMapper();
    T24ResponseBody t24ResponseBody = objectMapper.readValue(mockJson, T24ResponseBody.class);
    return t24ResponseBody;
  }

  public static T24ResponseBody
      getMockTransactionListDtoWIthEmptyDateAndUnknowndebitCreditIndicator() throws Exception {
    String mockJson =
        "{\n"
            + "  \"header\": {\n"
            + "    \"audit\": {\n"
            + "      \"T24_time\": 385,\n"
            + "      \"parse_time\": 0\n"
            + "    },\n"
            + "    \"page_start\": 0,\n"
            + "    \"total_size\": 2,\n"
            + "    \"page_token\": \"d750782a-e998-47d8-ad6a-368153a45ab3\",\n"
            + "    \"page_size\": 50\n"
            + "  },\n"
            + "  \"body\": [\n"
            + "    {\n"
            + "      \"debitCreditIndicator\": \"Exit\",\n"
            + "      \"balance\": \"6,502.64\",\n"
            + "      \"transactionAmount\": \"2.51\",\n"
            + "      \"bankAccountNumber\": \"100000341\",\n"
            + "      \"transactionCode\": \"891\",\n"
            + "      \"valueDate\": \"\",\n"
            + "      \"postingDate\": \"\",\n"
            + "      \"TransactionNarative\": \"Withholding Tax - Resident\",\n"
            + "      \"TransactDescription\": \"Debit Arrangement\"\n"
            + "    }\n"
            + "  ]\n"
            + "}";

    ObjectMapper objectMapper = new ObjectMapper();
    T24ResponseBody t24ResponseBody = objectMapper.readValue(mockJson, T24ResponseBody.class);
    return t24ResponseBody;
  }

  public static T24ResponseBody getMockTransactionAccountListDtoWithValidData() throws Exception {
    String mockJson =
        "{\n"
            + "  \"header\": {\n"
            + "    \"audit\": {\n"
            + "      \"T24_time\": 1002,\n"
            + "      \"parse_time\": 0\n"
            + "    },\n"
            + "    \"page_start\": 0,\n"
            + "    \"total_size\": 8,\n"
            + "    \"page_token\": \"4b7da3d3-edd3-4bed-937b-cde2ee6e4187\",\n"
            + "    \"page_size\": 50\n"
            + "  },\n"
            + "  \"body\": [{\n"
            + "    \"arrangementId\": \"AA19039DSHJJ\",\n"
            + "    \"companyCode\": \"AU0010001\",\n"
            + "    \"arrangementStatus\": \"AUTH\",\n"
            + "    \"contractDate\": \"20190208\",\n"
            + "    \"productId\": \"1001\",\n"
            + "    \"debitInterestRate\": \"0.00\",\n"
            + "    \"accountTitle\": \"Saver Account Individual\",\n"
            + "    \"accountType\": \"Volt Saver Account-Individual\",\n"
            + "    \"currentBalance\": 2107.07,\n"
            + "    \"description\": \"Volt Saver Account-Individual\",\n"
            + "    \"valueDate\": \"20190208\",\n"
            + "    \"sortCode\": \"517000\",\n"
            + "    \"remainingTerm\": \"Matures in\",\n"
            + "    \"relationShipFlag\": \"OWNER\",\n"
            + "    \"availableBalance\": 2107.07,\n"
            + "    \"acctNumber\": " + MOCKED_ACCT_NUMBER_SAVER + ",\n"
            + "    \"balance\": 2107.07,\n"
            + "    \"interestPaidYTD\": \"6.03\",\n"
            + "    \"nickname\": \"Saver Account Individual\",\n"
            + "    \"currency\": \"AUD\",\n"
            + "    \"interestPaidPrevYTD\": \"0.65\",\n"
            + "    \"creditInterestRate\": \"1.15\"\n"
            + "  }, {\n"
            + "    \"arrangementId\": \"AA190395KBHF\",\n"
            + "    \"companyCode\": \"AU0010001\",\n"
            + "    \"arrangementStatus\": \"AUTH\",\n"
            + "    \"contractDate\": \"20190208\",\n"
            + "    \"productId\": \"1021\",\n"
            + "    \"debitInterestRate\": \"0.00\",\n"
            + "    \"accountTitle\": \"Transaction Account Individual\",\n"
            + "    \"accountType\": \"Volt Transaction Account-Individual\",\n"
            + "    \"currentBalance\": 6.631375372E7,\n"
            + "    \"description\": \"Volt Transaction Account-Individual\",\n"
            + "    \"valueDate\": \"20190208\",\n"
            + "    \"sortCode\": \"517000\",\n"
            + "    \"remainingTerm\": \"Matures in\",\n"
            + "    \"relationShipFlag\": \"OWNER\",\n"
            + "    \"availableBalance\": 6.631375372E7,\n"
            + "    \"acctNumber\": " + MOCKED_ACCT_NUMBER_TRANS + ",\n"
            + "    \"balance\": 6.631375372E7,\n"
            + "    \"interestPaidYTD\": \"0\",\n"
            + "    \"nickname\": \"Transaction Account Individual\",\n"
            + "    \"currency\": \"AUD\",\n"
            + "    \"interestPaidPrevYTD\": \"0\",\n"
            + "    \"creditInterestRate\": \"0.00\"\n"
            + "  }, {\n"
            + "    \"arrangementId\": \"AA19044WWBT6\",\n"
            + "    \"companyCode\": \"AU0010001\",\n"
            + "    \"arrangementStatus\": \"AUTH\",\n"
            + "    \"contractDate\": \"20181010\",\n"
            + "    \"productId\": \"1001\",\n"
            + "    \"debitInterestRate\": \"0.00\",\n"
            + "    \"accountTitle\": \"Saver Account Individual\",\n"
            + "    \"accountType\": \"Volt Saver Account-Individual\",\n"
            + "    \"currentBalance\": 926.76,\n"
            + "    \"description\": \"Volt Saver Account-Individual\",\n"
            + "    \"valueDate\": \"20181010\",\n"
            + "    \"sortCode\": \"517000\",\n"
            + "    \"remainingTerm\": \"Matures in\",\n"
            + "    \"relationShipFlag\": \"OWNER\",\n"
            + "    \"availableBalance\": 926.76,\n"
            + "    \"acctNumber\": " + MOCK_ACCT_NUMBER_SAVER_1 + ",\n"
            + "    \"balance\": 926.76,\n"
            + "    \"interestPaidYTD\": \"1.76\",\n"
            + "    \"nickname\": \"Saver Account Individual\",\n"
            + "    \"currency\": \"AUD\",\n"
            + "    \"interestPaidPrevYTD\": \"0\",\n"
            + "    \"creditInterestRate\": \"1.15\"\n"
            + "  }]\n"
            + "}";

    ObjectMapper objectMapper = new ObjectMapper();
    T24ResponseBody t24ResponseBody = objectMapper.readValue(mockJson, T24ResponseBody.class);
    return t24ResponseBody;
  }

  public static T24ResponseBody getMockAccountListWithNullAndInValidProductId() throws Exception {
    String mockJson =
        "{\n"
            + "  \"header\": {\n"
            + "    \"audit\": {\n"
            + "      \"T24_time\": 1002,\n"
            + "      \"parse_time\": 0\n"
            + "    },\n"
            + "    \"page_start\": 0,\n"
            + "    \"total_size\": 8,\n"
            + "    \"page_token\": \"4b7da3d3-edd3-4bed-937b-cde2ee6e4187\",\n"
            + "    \"page_size\": 50\n"
            + "  },\n"
            + "  \"body\": [{\n"
            + "   \n"
            + "    \"productId\": \"76576\",\n"
            + "    \"valueDate\": \"20190208\"\n"
            + "  }, {\n"
            + "    \"arrangementId\": \"AA190395KBHF\",\n"
            + "    \"companyCode\": \"AU0010001\",\n"
            + "    \"arrangementStatus\": \"AUTH\",\n"
            + "    \"contractDate\": \"20190208\",\n"
            + "    \"debitInterestRate\": \"0.00\",\n"
            + "    \"accountTitle\": \"Transaction Account Individual\",\n"
            + "    \"accountType\": \"Volt Transaction Account-Individual\",\n"
            + "    \"currentBalance\": 6.631375372E7,\n"
            + "    \"description\": \"Volt Transaction Account-Individual\",\n"
            + "    \"valueDate\": \"20190208\",\n"
            + "    \"sortCode\": \"517000\",\n"
            + "    \"remainingTerm\": \"Matures in\",\n"
            + "    \"relationShipFlag\": \"OWNER\",\n"
            + "    \"availableBalance\": 6.631375372E7,\n"
            + "    \"acctNumber\": " + MOCKED_ACCT_NUMBER_TRANS + ",\n"
            + "    \"balance\": 6.631375372E7,\n"
            + "    \"interestPaidYTD\": \"0\",\n"
            + "    \"nickname\": \"Transaction Account Individual\",\n"
            + "    \"currency\": \"AUD\",\n"
            + "    \"interestPaidPrevYTD\": \"0\",\n"
            + "    \"creditInterestRate\": \"0.00\"\n"
            + "  }]\n"
            + "}";

    ObjectMapper objectMapper = new ObjectMapper();
    T24ResponseBody t24ResponseBody = objectMapper.readValue(mockJson, T24ResponseBody.class);
    return t24ResponseBody;
  }

  public static T24ResponseBody getMockAccountListwithEmptyBody() throws Exception {
    String mockJson =
        "{\n"
            + "  \"header\": {\n"
            + "    \"audit\": {\n"
            + "      \"T24_time\": 1002,\n"
            + "      \"parse_time\": 0\n"
            + "    },\n"
            + "    \"page_start\": 0,\n"
            + "    \"total_size\": 8,\n"
            + "    \"page_token\": \"4b7da3d3-edd3-4bed-937b-cde2ee6e4187\",\n"
            + "    \"page_size\": 50\n"
            + "  },\n"
            + "  \"body\": []\n"
            + "}";
    ObjectMapper objectMapper = new ObjectMapper();
    T24ResponseBody t24ResponseBody = objectMapper.readValue(mockJson, T24ResponseBody.class);
    return t24ResponseBody;
  }

  public static T24ResponseBody getMockAccountListwithNullValueDateBody() throws Exception {
    String mockJson =
        "{\n"
            + "  \"header\": {\n"
            + "    \"audit\": {\n"
            + "      \"T24_time\": 1002,\n"
            + "      \"parse_time\": 0\n"
            + "    },\n"
            + "    \"page_start\": 0,\n"
            + "    \"total_size\": 8,\n"
            + "    \"page_token\": \"4b7da3d3-edd3-4bed-937b-cde2ee6e4187\",\n"
            + "    \"page_size\": 50\n"
            + "  },\n"
            + "  \"body\": [{\n"
            + "    \"productId\": \"1001\"\n"
            + "  }]\n"
            + "}";
    ObjectMapper objectMapper = new ObjectMapper();
    T24ResponseBody t24ResponseBody = objectMapper.readValue(mockJson, T24ResponseBody.class);
    return t24ResponseBody;
  }

  public static TransactionAccountListDto getTransactionAccountListDto() {
    // :-( cant use builder here because this was generated
    TransactionAccountInfoDto transactionAccountInfoDto = new TransactionAccountInfoDto();
    transactionAccountInfoDto.setProductId(ProductIdDto._000);
    transactionAccountInfoDto.setBsb("000");
    transactionAccountInfoDto.setAvailableBalance("877638763");

    List<TransactionAccountInfoDto> transactionAccountInfoDtoList = new ArrayList<>();
    transactionAccountInfoDtoList.add(transactionAccountInfoDto);

    return new TransactionAccountListDto()
        .data(transactionAccountInfoDtoList)
        .pagingInfo(new PagingInfoDto());
  }

  public static VoltAuthIdPairDto getVoltAuthIdPairDto() {
    String authId = RandomString.randomNumericString(8);
    String voltId = RandomString.randomNumericString(8);

    VoltAuthIdPairDto voltAuthIdPairDto = new VoltAuthIdPairDto();
    voltAuthIdPairDto.setAuthId(authId);
    voltAuthIdPairDto.setVoltId(voltId);
    return voltAuthIdPairDto;
  }

  public static VoltT24IdPairDto getVoltT24IdPairDto() {
    String t24Id = RandomString.randomNumericString(8);
    String voltId = RandomString.randomNumericString(8);

    VoltT24IdPairDto voltT24IdPairDto = new VoltT24IdPairDto();
    voltT24IdPairDto.setT24Id(t24Id);
    voltT24IdPairDto.setVoltId(voltId);
    return voltT24IdPairDto;
  }

  public static <T> T fromJson(String json, Class<T> clazz) throws IOException {
    ObjectMapper objectMapper = new ObjectMapper();
    return objectMapper.readValue(json, clazz);
  }

  public static ResponseEntity<T24ResponseBody> getT24AccountsEntity() {
      Map<String, String> header = new LinkedHashMap<>();
      header.put("id", "ABC123EFG");
      Map<String, String> body = new LinkedHashMap<>();
      body.put("accountType", "Volt Saver Account-Individual");
      body.put("acctNumber", MOCK_ACCOUNT_NUMBER);
      List<Map<String, String>> list = new ArrayList<>();
      list.add(body);
      T24ResponseBody<List, Map, Map> t24ResponseBody = new T24ResponseBody<>();
      t24ResponseBody.setBody(list);
      t24ResponseBody.setHeader(header);
      return ResponseEntity.ok(t24ResponseBody);
  }
  private static String toJson(Object toBeSerialized) throws JsonProcessingException {
    ObjectMapper objectMapper = new ObjectMapper();
    return objectMapper.writeValueAsString(toBeSerialized);
  }
}
