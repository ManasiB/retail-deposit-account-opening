FROM g1vimgacr.azurecr.io/voltbank/java:8-jre-alpine-20190829165915

ADD /target/retail-deposit-account-opening-*.jar retail-deposit-account-opening.jar
ENTRYPOINT ["java","-jar","retail-deposit-account-opening.jar"]

HEALTHCHECK --start-period=90s --timeout=5s \
  CMD curl -f http://localhost:8080/v1/retail_deposit_account_opening/actuator/health || exit 1
