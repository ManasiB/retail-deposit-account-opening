package au.com.voltbank.termdeposit.service;

import au.com.voltbank.RetailDepositAccountOpeningApplication;
import au.com.voltbank.sis.model.GreenIdDto;
import au.com.voltbank.sis.model.SalesforceIdDto;
import au.com.voltbank.sis.model.T24IdDto;
import au.com.voltbank.sis.model.VoltAuthIdPairDto;
import au.com.voltbank.sis.model.VoltGreenIdPairDto;
import au.com.voltbank.sis.model.VoltSalesforceIdPairDto;
import au.com.voltbank.sis.model.VoltT24IdPairDto;
import au.com.voltbank.termdeposit.exception.AuthIdException;
import au.com.voltbank.termdeposit.exception.GreenIdException;
import au.com.voltbank.termdeposit.exception.SalesforceIdException;
import au.com.voltbank.termdeposit.exception.T24IdException;
import au.com.voltbank.termdeposit.exception.VoltIdException;
import com.tyro.oss.randomdata.RandomString;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailDepositAccountOpeningApplication.class)
public class SystemIdStoreServiceTest {

  @Autowired private SystemIdStoreService systemIdStoreService;

  @MockBean(name = "secureRestTemplate")
  private RestTemplate secureRestTemplate;

  @MockBean private AzureActiveDirectoryService azureActiveDirectoryService;

  @Before
  public void setUp() {
    reset(secureRestTemplate, azureActiveDirectoryService);
  }

  @Test
  public void shouldRetrieveAuthIDFromSisService() {
    String voltId = RandomString.randomNumericString(8);

    String expectedAuthId = RandomString.randomNumericString(8);

    VoltAuthIdPairDto expectedVoltAuthIdPair = new VoltAuthIdPairDto();
    expectedVoltAuthIdPair.setAuthId(expectedAuthId);
    expectedVoltAuthIdPair.setVoltId(voltId);

    ArgumentCaptor<String> uriCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    when(secureRestTemplate.exchange(
            uriCaptor.capture(),
            eq(HttpMethod.GET),
            httpEntityCaptor.capture(),
            eq(VoltAuthIdPairDto.class)))
        .thenReturn(new ResponseEntity<>(expectedVoltAuthIdPair, HttpStatus.OK));

    String authId = systemIdStoreService.getAuthId(voltId).getAuthId();

    assertThat(
        uriCaptor.getValue(),
        containsString(String.format("/v1/system_id_store/auth?voltId=%s", voltId)));
    assertThat(
        httpEntityCaptor.getValue().getHeaders().get("Authorization").get(0),
        is("Bearer " + accessToken));
    assertThat(authId, is(expectedAuthId));
  }

  @Test
  public void shouldRetrieveVoltIDFromSisService() {
    String authId = RandomString.randomNumericString(8);

    String expectedVoltId = RandomString.randomNumericString(8);

    VoltAuthIdPairDto expectedVoltAuthIdPair = new VoltAuthIdPairDto();
    expectedVoltAuthIdPair.setAuthId(authId);
    expectedVoltAuthIdPair.setVoltId(expectedVoltId);

    ArgumentCaptor<String> uriCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    when(secureRestTemplate.exchange(
            uriCaptor.capture(),
            eq(HttpMethod.GET),
            httpEntityCaptor.capture(),
            eq(VoltAuthIdPairDto.class)))
        .thenReturn(new ResponseEntity<>(expectedVoltAuthIdPair, HttpStatus.OK));

    String voltId = systemIdStoreService.getVoltId(authId).getVoltId();

    assertThat(
        uriCaptor.getValue(),
        containsString(String.format("/v1/system_id_store/auth?authId=%s", authId)));
    assertThat(
        httpEntityCaptor.getValue().getHeaders().get("Authorization").get(0),
        is("Bearer " + accessToken));
    assertThat(voltId, is(expectedVoltId));
  }

  @Test
  public void shouldRetrieveVoltIdByGreenIdFromSisService() throws GreenIdException {
    String greenId = RandomString.randomNumericString(8);
    String expectedVoltId = RandomString.randomNumericString(8);
    VoltGreenIdPairDto expectedVoltGreenIdPair = new VoltGreenIdPairDto();
    expectedVoltGreenIdPair.setGreenId(greenId);
    expectedVoltGreenIdPair.setVoltId(expectedVoltId);
    ArgumentCaptor<String> uriCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);
    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    when(secureRestTemplate.exchange(
            uriCaptor.capture(),
            eq(HttpMethod.GET),
            httpEntityCaptor.capture(),
            eq(VoltGreenIdPairDto.class)))
        .thenReturn(new ResponseEntity<>(expectedVoltGreenIdPair, HttpStatus.OK));

    String voltId = systemIdStoreService.getVoltIdByGreenId(greenId).getVoltId();

    assertThat(
        uriCaptor.getValue(),
        containsString(String.format("/v1/system_id_store/illion?greenId=%s", greenId)));
    assertThat(
        httpEntityCaptor.getValue().getHeaders().get("Authorization").get(0),
        is("Bearer " + accessToken));
    assertThat(voltId, is(expectedVoltId));
  }

  @Test
  public void shouldRetrieveSalesforceIDFromSisService() {
    // setup
    String voltId = RandomString.randomNumericString(8);

    String expectedSalesforce = RandomString.randomNumericString(8);

    VoltSalesforceIdPairDto expectedVoltSalesforceIdPair = new VoltSalesforceIdPairDto();
    expectedVoltSalesforceIdPair.setSalesforceId(expectedSalesforce);
    expectedVoltSalesforceIdPair.setVoltId(voltId);

    ArgumentCaptor<String> uriCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    when(secureRestTemplate.exchange(
            uriCaptor.capture(),
            eq(HttpMethod.GET),
            httpEntityCaptor.capture(),
            eq(VoltSalesforceIdPairDto.class)))
        .thenReturn(new ResponseEntity<>(expectedVoltSalesforceIdPair, HttpStatus.OK));

    // test
    String salesforceId = systemIdStoreService.getSalesforceId(voltId).get().getSalesforceId();

    // verify
    assertThat(
        uriCaptor.getValue(),
        containsString(String.format("/v1/system_id_store/salesforce?voltId=%s", voltId)));
    assertThat(
        httpEntityCaptor.getValue().getHeaders().get("Authorization").get(0),
        is("Bearer " + accessToken));
    assertThat(salesforceId, is(expectedSalesforce));
  }

  @Test
  public void shouldThrowExceptionWhenCannotRetrieveAuthId() {
    String voltId = RandomString.randomNumericString(8);

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    when(secureRestTemplate.exchange(
            anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(VoltAuthIdPairDto.class)))
        .thenReturn(new ResponseEntity<>(HttpStatus.NOT_FOUND));

    AuthIdException expectedException = null;
    try {
      systemIdStoreService.getAuthId(voltId);
    } catch (Exception e) {
      expectedException = (AuthIdException) e;
    }
    assertThat(
        expectedException.getMessage(),
        is(String.format("AuthId for voltId %s not found", voltId)));
  }

  @Test
  public void shouldThrowExceptionWhenCannotRetrieveVoltId() {
    String authId = RandomString.randomNumericString(8);

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    when(secureRestTemplate.exchange(
            anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(VoltAuthIdPairDto.class)))
        .thenReturn(new ResponseEntity<>(HttpStatus.NOT_FOUND));

    VoltIdException expectedException = null;
    try {
      systemIdStoreService.getVoltId(authId);
    } catch (Exception e) {
      expectedException = (VoltIdException) e;
    }
    assertThat(
        expectedException.getMessage(),
        is(String.format("VoltId for authId %s not found", authId)));
  }

  @Test
  public void shouldThrowExceptionWhenCannotRetrieveVoltIdByGreenId() {
    String greenId = RandomString.randomNumericString(8);

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    when(secureRestTemplate.exchange(
            anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(VoltGreenIdPairDto.class)))
        .thenReturn(new ResponseEntity<>(HttpStatus.NOT_FOUND));

    GreenIdException expectedException = null;
    try {
      systemIdStoreService.getVoltIdByGreenId(greenId);
    } catch (Exception e) {
      expectedException = (GreenIdException) e;
    }
    assertThat(
        expectedException.getMessage(),
        is(String.format("VoltId for greenId %s not found", greenId)));
  }

  @Test
  public void shouldReturnEmptyOptionalWhenSalesforceIdNotFound() {
    String voltId = RandomString.randomNumericString(8);

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    when(secureRestTemplate.exchange(
            anyString(),
            eq(HttpMethod.GET),
            any(HttpEntity.class),
            eq(VoltSalesforceIdPairDto.class)))
        .thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND));

    assertThat(systemIdStoreService.getSalesforceId(voltId), is(Optional.empty()));
  }

  @Test
  public void
      shouldRethrowHttpClientErrorExceptionWhenItIsNotA404ErrorWhenTryingToFetchSalesforceId() {
    String voltId = RandomString.randomNumericString(8);

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    when(secureRestTemplate.exchange(
            anyString(),
            eq(HttpMethod.GET),
            any(HttpEntity.class),
            eq(VoltSalesforceIdPairDto.class)))
        .thenThrow(new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR));

    SalesforceIdException expectedException = null;
    try {
      systemIdStoreService.getSalesforceId(voltId);
    } catch (Exception e) {
      expectedException = (SalesforceIdException) e;
    }
    assertThat(
        expectedException.getMessage(),
        is(String.format("Could not retrieve salesforceId for voltId: %s", voltId)));
    assertThat(expectedException.getCause(), instanceOf(HttpClientErrorException.class));
  }

  @Test
  public void shouldThrowExceptionWhenGetAuthIdRequestFails() {
    String voltId = RandomString.randomNumericString(8);

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    when(secureRestTemplate.exchange(
            anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(String.class)))
        .thenThrow(new RuntimeException("BOOM!"));

    AuthIdException expectedException = null;
    try {
      systemIdStoreService.getAuthId(voltId);
    } catch (Exception e) {
      expectedException = (AuthIdException) e;
    }
    assertThat(
        expectedException.getMessage(),
        is(String.format("Could not retrieve authId for voltId: %s", voltId)));
  }

  @Test
  public void shouldThrowExceptionWhenGetVoltIdRequestFails() {
    String authId = RandomString.randomNumericString(8);

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    when(secureRestTemplate.exchange(
            anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(String.class)))
        .thenThrow(new RuntimeException("BOOM!"));

    VoltIdException expectedException = null;
    try {
      systemIdStoreService.getVoltId(authId);
    } catch (Exception e) {
      expectedException = (VoltIdException) e;
    }
    assertThat(
        expectedException.getMessage(),
        is(String.format("Could not retrieve voltId for authId: %s", authId)));
  }

  @Test
  public void shouldThrowExceptionWhenGetSalesforceIdRequestFails() {
    String voltId = RandomString.randomNumericString(8);

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    when(secureRestTemplate.exchange(
            anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(String.class)))
        .thenThrow(new RuntimeException("BOOM!"));

    SalesforceIdException expectedException = null;
    try {
      systemIdStoreService.getSalesforceId(voltId);
    } catch (Exception e) {
      expectedException = (SalesforceIdException) e;
    }
    assertThat(
        expectedException.getMessage(),
        is(String.format("Could not retrieve salesforceId for voltId: %s", voltId)));
  }

  @Test
  public void shouldCreateVoltIdWithAuthIDInSisService() {
    String authId = RandomString.randomNumericString(8);

    String expectedVoltId = RandomString.randomNumericString(8);

    VoltAuthIdPairDto expectedVoltAuthIdPair = new VoltAuthIdPairDto();
    expectedVoltAuthIdPair.setAuthId(authId);
    expectedVoltAuthIdPair.setVoltId(expectedVoltId);

    ArgumentCaptor<String> uriCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    when(secureRestTemplate.exchange(
            uriCaptor.capture(),
            eq(HttpMethod.POST),
            httpEntityCaptor.capture(),
            eq(VoltAuthIdPairDto.class)))
        .thenReturn(new ResponseEntity<>(expectedVoltAuthIdPair, HttpStatus.CREATED));

    VoltAuthIdPairDto voltAuthIdPairDto = systemIdStoreService.createVoltId(authId);

    assertThat(uriCaptor.getValue(), containsString("/v1/system_id_store/auth"));
    assertThat(
        httpEntityCaptor.getValue().getHeaders().get("Authorization").get(0),
        is("Bearer " + accessToken));
    assertThat(voltAuthIdPairDto.getAuthId(), is(authId));
    assertThat(voltAuthIdPairDto.getVoltId(), is(expectedVoltId));
  }

  @Test
  public void shouldThrowExceptionWhenCannotCreateVoltId() {
    String authId = RandomString.randomNumericString(8);

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    when(secureRestTemplate.exchange(
            anyString(), eq(HttpMethod.POST), any(HttpEntity.class), eq(VoltAuthIdPairDto.class)))
        .thenThrow(new RuntimeException("BOOM!"));

    AuthIdException expectedException = null;
    try {
      systemIdStoreService.createVoltId(authId);
    } catch (Exception e) {
      expectedException = (AuthIdException) e;
    }
    assertThat(
        expectedException.getMessage(),
        is(String.format("Could not create voltId for authId: %s", authId)));
  }

  @Test
  public void shouldCreateGreenIdWithVoltIdInSisService() throws GreenIdException {
    String voltId = RandomString.randomNumericString(8);
    String greenId = RandomString.randomNumericString(8);

    GreenIdDto greenIdDto = new GreenIdDto();
    greenIdDto.setGreenId(greenId);

    VoltGreenIdPairDto expectedVoltGreenIdPair = new VoltGreenIdPairDto();
    expectedVoltGreenIdPair.setGreenId(greenId);
    expectedVoltGreenIdPair.setVoltId(voltId);

    ArgumentCaptor<String> uriCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    when(secureRestTemplate.exchange(
            uriCaptor.capture(),
            eq(HttpMethod.POST),
            httpEntityCaptor.capture(),
            eq(VoltGreenIdPairDto.class)))
        .thenReturn(new ResponseEntity<>(expectedVoltGreenIdPair, HttpStatus.CREATED));

    VoltGreenIdPairDto voltGreenIdPairDto = systemIdStoreService.createGreenId(voltId, greenId);

    assertThat(
        uriCaptor.getValue(),
        containsString(String.format("/v1/system_id_store/illion/%s", voltId)));
    assertThat(
        httpEntityCaptor.getValue().getHeaders().get("Authorization").get(0),
        is("Bearer " + accessToken));
    assertThat(httpEntityCaptor.getValue().getBody(), is(greenIdDto));

    assertThat(expectedVoltGreenIdPair, is(voltGreenIdPairDto));
  }

  @Test
  public void shouldCreateSalesforceIdWithVoltIdInSisService() {
    String voltId = RandomString.randomNumericString(8);
    String salesforceId = RandomString.randomNumericString(8);

    SalesforceIdDto salesforceIdDto = new SalesforceIdDto();
    salesforceIdDto.setSalesforceId(salesforceId);

    VoltSalesforceIdPairDto expectedVoltSalesforceIdPair = new VoltSalesforceIdPairDto();
    expectedVoltSalesforceIdPair.setSalesforceId(salesforceId);
    expectedVoltSalesforceIdPair.setVoltId(voltId);

    ArgumentCaptor<String> uriCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    when(secureRestTemplate.exchange(
            uriCaptor.capture(),
            eq(HttpMethod.POST),
            httpEntityCaptor.capture(),
            eq(VoltSalesforceIdPairDto.class)))
        .thenReturn(new ResponseEntity<>(expectedVoltSalesforceIdPair, HttpStatus.CREATED));

    VoltSalesforceIdPairDto voltSalesforceIdPairDto =
        systemIdStoreService.createSalesforceId(voltId, salesforceId);

    assertThat(
        uriCaptor.getValue(),
        containsString(String.format("/v1/system_id_store/salesforce/%s", voltId)));
    assertThat(
        httpEntityCaptor.getValue().getHeaders().get("Authorization").get(0),
        is("Bearer " + accessToken));
    assertThat(httpEntityCaptor.getValue().getBody(), is(salesforceIdDto));

    assertThat(expectedVoltSalesforceIdPair, is(voltSalesforceIdPairDto));
  }

  @Test
  public void shouldThrowExceptionWhenCannotCreateGreenId() {
    String voltId = RandomString.randomNumericString(8);
    String greenId = RandomString.randomNumericString(8);

    GreenIdDto greenIdDto = new GreenIdDto();
    greenIdDto.setGreenId(greenId);

    VoltGreenIdPairDto expectedVoltGreenIdPaireDto = new VoltGreenIdPairDto();
    expectedVoltGreenIdPaireDto.setVoltId(voltId);
    expectedVoltGreenIdPaireDto.setGreenId(greenId);

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    ArgumentCaptor<String> uriCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);

    when(secureRestTemplate.exchange(
            uriCaptor.capture(),
            eq(HttpMethod.POST),
            httpEntityCaptor.capture(),
            eq(VoltGreenIdPairDto.class)))
        .thenThrow(new RuntimeException("BOOM!"));

    GreenIdException expectedException = null;

    try {
      systemIdStoreService.createGreenId(voltId, greenId);
    } catch (Exception e) {
      expectedException = (GreenIdException) e;
    }

    assertThat(
        expectedException.getMessage(),
        is(String.format("Could not create greenId: %s for voltId: %s", greenId, voltId)));
  }

  @Test
  public void shouldThrowExceptionWhenCannotCreateSalesforceId() {
    String voltId = RandomString.randomNumericString(8);
    String salesforceId = RandomString.randomNumericString(8);

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    when(secureRestTemplate.exchange(
            anyString(),
            eq(HttpMethod.POST),
            any(HttpEntity.class),
            eq(VoltSalesforceIdPairDto.class)))
        .thenThrow(new RuntimeException("BOOM!"));

    SalesforceIdException expectedException = null;
    try {
      systemIdStoreService.createSalesforceId(voltId, salesforceId);
    } catch (Exception e) {
      expectedException = (SalesforceIdException) e;
    }
    assertThat(
        expectedException.getMessage(),
        is(
            String.format(
                "Could not create salesforceId: %s for voltId: %s", salesforceId, voltId)));
  }

  @Test
  public void shouldCreateT24IdWithVoltIdInSisService() throws T24IdException {
    String voltId = RandomString.randomNumericString(8);
    String t24Id = RandomString.randomNumericString(8);

    T24IdDto t24IdDto = new T24IdDto();
    t24IdDto.setT24Id(t24Id);

    VoltT24IdPairDto expectedVoltT24IdPair = new VoltT24IdPairDto();
    expectedVoltT24IdPair.setT24Id(t24Id);
    expectedVoltT24IdPair.setVoltId(voltId);

    ArgumentCaptor<String> uriCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    when(secureRestTemplate.exchange(
            uriCaptor.capture(),
            eq(HttpMethod.POST),
            httpEntityCaptor.capture(),
            eq(VoltT24IdPairDto.class)))
        .thenReturn(new ResponseEntity<>(expectedVoltT24IdPair, HttpStatus.CREATED));

    VoltT24IdPairDto voltSalesforceIdPairDto = systemIdStoreService.createT24Id(voltId, t24Id);

    assertThat(
        uriCaptor.getValue(), containsString(String.format("/v1/system_id_store/t24/%s", voltId)));
    assertThat(
        httpEntityCaptor.getValue().getHeaders().get("Authorization").get(0),
        is("Bearer " + accessToken));
    assertThat(httpEntityCaptor.getValue().getBody(), is(t24IdDto));

    assertThat(expectedVoltT24IdPair, is(voltSalesforceIdPairDto));
  }

  @Test
  public void shouldThrowExceptionWhenCannotCreateT24Id() {
    String voltId = RandomString.randomNumericString(8);
    String t24Id = RandomString.randomNumericString(8);

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    when(secureRestTemplate.exchange(
            anyString(), eq(HttpMethod.POST), any(HttpEntity.class), eq(VoltT24IdPairDto.class)))
        .thenThrow(new RuntimeException("BOOM!"));

    T24IdException expectedException = null;
    try {
      systemIdStoreService.createT24Id(voltId, t24Id);
    } catch (Exception e) {
      expectedException = (T24IdException) e;
    }
    assertThat(
        expectedException.getMessage(),
        is(String.format("Could not create t24Id: %s for voltId: %s", t24Id, voltId)));
  }
}
