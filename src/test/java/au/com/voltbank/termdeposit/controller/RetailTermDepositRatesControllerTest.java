package au.com.voltbank.termdeposit.controller;

import au.com.voltbank.Constants;
import au.com.voltbank.account.model.RetailTermDepositRateDto;
import au.com.voltbank.termdeposit.dto.ErrorResponse;
import au.com.voltbank.termdeposit.entity.RetailTermDepositRate;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.service.RetailTermDepositRatesService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = RetailTermDepositRatesController.class, secure = false)
public class RetailTermDepositRatesControllerTest {

  @Autowired private MockMvc mockMvc;

  @Autowired private ObjectMapper objectMapper;

  private ModelMapper modelMapper;

  @MockBean private RetailTermDepositRatesService ratesService;

  @Before
  public void setUp() {
    if (null == modelMapper) {
      modelMapper = new ModelMapper();
    }
    reset(ratesService);
  }

  @Test
  public void shouldReturn_200_IfNoErrorsOccur_ForGetAllRetailTermDepositRates() throws Exception {
    List<RetailTermDepositRate> return_rates = new ArrayList<>();

    RetailTermDepositRate rate = new RetailTermDepositRate();
    rate.setPercentRate(2.0);
    return_rates.add(rate);

    rate = new RetailTermDepositRate();
    rate.setPercentRate(3.0);
    return_rates.add(rate);

    rate = new RetailTermDepositRate();
    rate.setPercentRate(3.5);
    return_rates.add(rate);

    rate = new RetailTermDepositRate();
    rate.setPercentRate(4.0);
    return_rates.add(rate);

    when(ratesService.fetchLatestRates())
        .thenReturn(
            return_rates.stream()
                .map(item -> modelMapper.map(item, RetailTermDepositRateDto.class))
                .collect(Collectors.toList()));

    this.mockMvc.perform(get("/term_deposit/rates")).andDo(print()).andExpect(status().isOk());

    verify(ratesService).fetchLatestRates();
  }

  @Test
  public void shouldReturn_500_IfExceptionThrown_ForGetAllRetailTermDepositRates()
      throws Exception {

    when(ratesService.fetchLatestRates())
        .thenThrow(
            new VoltCustomException(
                Constants.ERROR_SYSTEM, null, HttpStatus.INTERNAL_SERVER_ERROR.value()));

    this.mockMvc
        .perform(get("/term_deposit/rates"))
        .andDo(print())
        .andExpect(status().isInternalServerError())
        .andExpect(
            content()
                .json(objectMapper.writeValueAsString(new ErrorResponse(Constants.ERROR_SYSTEM))));
  }
}
