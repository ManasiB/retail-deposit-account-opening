package au.com.voltbank.termdeposit.exception;

public class T24IdException extends Exception {

  public T24IdException(String message, Throwable cause) {
    super(message, cause);
  }

  public T24IdException(String message) {
    super(message);
  }
}
