package au.com.voltbank.termdeposit.controller;

import au.com.voltbank.account.model.SavingsAccountApplicationDataDto;
import au.com.voltbank.sis.model.VoltAuthIdPairDto;
import au.com.voltbank.t24.account.AccountDetailBodyDto;
import au.com.voltbank.termdeposit.entity.SavingsAccountApplication;
import au.com.voltbank.termdeposit.exception.SavingsAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.T24IdException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.service.SavingsAccountService;
import au.com.voltbank.termdeposit.service.SystemIdStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static au.com.voltbank.termdeposit.mapper.AccountMapper.mapToSavingsAccountResultDto;

@RestController
public class SavingsAccountController {

  @Autowired private SavingsAccountService savingsAccountService;

  @Autowired private SystemIdStoreService systemIdStoreService;

  @PostMapping("/savings/account")
  public ResponseEntity createAccount(
      @RequestBody SavingsAccountApplicationDataDto accountApplication,
      Authentication authentication)
      throws VoltCustomException, SavingsAccountNotFoundException, T24IdException {
    VoltAuthIdPairDto voltAuthIdPairDto = systemIdStoreService.getVoltId(authentication.getName());
    SavingsAccountApplication result =
        savingsAccountService.create(accountApplication, voltAuthIdPairDto.getVoltId());
    AccountDetailBodyDto accountDetail = savingsAccountService.getSavingsAccountDetails(result);
    return ResponseEntity.status(HttpStatus.CREATED)
        .body(mapToSavingsAccountResultDto(accountDetail));
  }
}
