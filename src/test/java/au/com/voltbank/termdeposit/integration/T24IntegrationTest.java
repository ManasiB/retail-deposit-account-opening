package au.com.voltbank.termdeposit.integration;

import au.com.voltbank.RetailDepositAccountOpeningApplication;
import au.com.voltbank.termdeposit.corebanking.service.T24AccountService;
import au.com.voltbank.termdeposit.corebanking.service.T24BeneficiaryService;
import au.com.voltbank.t24.T24ResponseBody;
import au.com.voltbank.t24.account.AccountDetailBodyDto;
import au.com.voltbank.t24.account.TDAccountDetailBodyDto;
import au.com.voltbank.termdeposit.entity.SavingsAccountApplication;
import au.com.voltbank.termdeposit.entity.TermDepositAccountApplication;
import au.com.voltbank.termdeposit.entity.TransactionAccountApplication;
import au.com.voltbank.termdeposit.exception.SavingsAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.T24IdException;
import au.com.voltbank.termdeposit.exception.TermDepositAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.service.SavingsAccountService;
import au.com.voltbank.termdeposit.service.TermDepositAccountApplicationService;
import au.com.voltbank.termdeposit.util.CannedData;
import au.com.voltbank.util.VoltCommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@Ignore("Prevent creating duplicated customers in build pipeline")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailDepositAccountOpeningApplication.class)
@Transactional
@Slf4j
public class T24IntegrationTest {

  private final String realT24CustomerId = "10000060";
  @Autowired private TermDepositAccountApplicationService termDepositAccountApplicationService;
  @Autowired private T24BeneficiaryService coreBankingBeneficiaryService;
  @Autowired private T24AccountService t24AccountService;
  @Autowired private SavingsAccountService savingsAccountService;

  @Before
  public void setUp() {}

  @Test
  public void create_tdaccount_test() throws VoltCustomException {
    TermDepositAccountApplication application = CannedData.getDepositAccountApplication();
    ResponseEntity<T24ResponseBody> response =
        t24AccountService.createTermDepositAccount(application);
    log.info("customerId and arrangementId created: " + VoltCommonUtils.toJsonString(response));
  }

  @Test
  public void create_transaction_account_test() throws VoltCustomException {
    TransactionAccountApplication application = CannedData.getTransactionAccountApplication();
    ResponseEntity<T24ResponseBody> response =
        t24AccountService.createTransactionAccount(application);
    log.info("customerId and arrangementId created: " + VoltCommonUtils.toJsonString(response));
  }

  @Test
  public void create_savings_account_test() throws VoltCustomException {
    SavingsAccountApplication application = CannedData.getSavingsAccountApplication();
    ResponseEntity<T24ResponseBody> response =
        t24AccountService.createSavingsAccount(realT24CustomerId);
    log.info("savings account created: " + VoltCommonUtils.toJsonString(response));
  }

  @Test
  public void get_savings_account_detail_test()
      throws VoltCustomException, SavingsAccountNotFoundException, T24IdException {
    SavingsAccountApplication application = CannedData.getSavingsAccountApplication();
    AccountDetailBodyDto response = savingsAccountService.getSavingsAccountDetails(application);
    log.info("Retrieved savings account detail: " + VoltCommonUtils.toJsonString(response));
  }

  @Test
  public void test_get_list_of_accounts() throws VoltCustomException {
    ResponseEntity<T24ResponseBody> response = t24AccountService.getAccounts(realT24CustomerId);
    log.info("List of accounts: " + VoltCommonUtils.toJsonString(response));
  }

  @Test
  public void test_get_td_account_detail()
      throws VoltCustomException, TermDepositAccountNotFoundException {
    TDAccountDetailBodyDto result =
        termDepositAccountApplicationService.getTermDepositAccountDetails(
            CannedData.getDepositAccountApplication());
    log.info(result.toString());
  }

  @Test
  public void test_create_beneficiary() throws VoltCustomException {
    TermDepositAccountApplication termDepositAccountApplication =
        CannedData.getDepositAccountApplication();
    ResponseEntity<T24ResponseBody> response =
        coreBankingBeneficiaryService.createBeneficiary(termDepositAccountApplication);
    log.info(response.getBody().toString());
  }
}
