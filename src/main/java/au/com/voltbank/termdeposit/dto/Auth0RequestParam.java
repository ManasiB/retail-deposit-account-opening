package au.com.voltbank.termdeposit.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Auth0RequestParam {
  private String issuer;

  private String audience;

  private String clientId;

  private String clientSecret;
}
