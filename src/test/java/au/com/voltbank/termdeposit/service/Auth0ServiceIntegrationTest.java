package au.com.voltbank.termdeposit.service;

import au.com.voltbank.RetailDepositAccountOpeningApplication;
import com.auth0.exception.Auth0Exception;
import com.auth0.json.mgmt.users.User;
import com.tyro.oss.randomdata.RandomString;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@Ignore("TODO - add integration test phase in build pipeline and switch on.")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailDepositAccountOpeningApplication.class)
public class Auth0ServiceIntegrationTest {

  private static final String PHONE_NUMBER_METADATA_FIELD_NAME = "mobile_number";

  @Autowired private Auth0Service auth0Service;

  @Test
  public void shouldFetchUserInfoFromAuth0() throws Auth0Exception {
    // setup
    String authId = "auth0|5c400eb585486334a30e761a";
    User expectedUser = new User();
    expectedUser.setEmail("vds-integration-test-user@voltbank.com.au");
    Map<String, Object> userAppMetaData = new HashMap<>();
    userAppMetaData.put(PHONE_NUMBER_METADATA_FIELD_NAME, "+61447213507");
    expectedUser.setAppMetadata(userAppMetaData);

    // test
    Optional<User> actualUser = auth0Service.getUserDetailFromAuth(authId);

    // verify
    assertThat(actualUser.isPresent(), is(true));
    assertThat(actualUser.get().getEmail(), is(expectedUser.getEmail()));
    assertThat(
        actualUser.get().getAppMetadata().get(PHONE_NUMBER_METADATA_FIELD_NAME),
        is(expectedUser.getAppMetadata().get(PHONE_NUMBER_METADATA_FIELD_NAME)));
  }

  @Test
  public void shouldSetT24ClaimAsPartOfUserAppMetaData() throws Auth0Exception {
    // setup
    String authId = "auth0|5c400eb585486334a30e761a";
    HashMap<String, Object> appMetadata = new HashMap<>();
    appMetadata.put("claim", "test");

    // test
    auth0Service.addUserAppMetadata(authId, appMetadata);

    // verify
    assertThat(
        auth0Service.getUserDetailFromAuth(authId).get().getAppMetadata().get("claim"), is("test"));

    // clean up
    appMetadata.put("claim", "");
    auth0Service.addUserAppMetadata(authId, appMetadata);
  }

  @Test
  public void shouldReturnOptionalOfNullableIfTheUserDoesNotExist() throws Auth0Exception {
    Optional<User> actualUser = auth0Service.getUserDetailFromAuth("auth0|junk12345678");

    assertThat(actualUser.isPresent(), is(false));
  }

  @Test(expected = Auth0Exception.class)
  public void shouldThrowAuth0ExceptionIfSomethingGoesWrongFetchingUser() throws Auth0Exception {
    String authId = RandomString.randomUUID();

    auth0Service.getUserDetailFromAuth(authId);
  }

  @Test(expected = Auth0Exception.class)
  public void shouldThrowAuth0ExceptionIfSomethingGoesWrongUpdatingUser() throws Auth0Exception {
    String authId = RandomString.randomUUID();

    auth0Service.addUserAppMetadata(authId, null);
  }
}
