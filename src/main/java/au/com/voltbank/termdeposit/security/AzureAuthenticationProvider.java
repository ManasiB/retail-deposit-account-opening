package au.com.voltbank.termdeposit.security;

import au.com.voltbank.security.TokenParser;
import com.auth0.spring.security.api.authentication.PreAuthenticatedAuthenticationJsonWebToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AzureAuthenticationProvider implements AuthenticationProvider {

  @Autowired private TokenParser tokenParser;

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    PreAuthenticatedAuthenticationJsonWebToken enhancedAuthentication =
        (PreAuthenticatedAuthenticationJsonWebToken) authentication;
    if (tokenParser.validateToken(enhancedAuthentication.getToken())) {
      enhancedAuthentication.setAuthenticated(true);
      return enhancedAuthentication;
    }
    return null;
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return true;
  }
}
