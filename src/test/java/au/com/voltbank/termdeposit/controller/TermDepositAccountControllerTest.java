package au.com.voltbank.termdeposit.controller;

import au.com.voltbank.Constants;
import au.com.voltbank.account.model.RetailTermDepositRateDto;
import au.com.voltbank.account.model.TdAccountApplicationDataDto;
import au.com.voltbank.account.model.TdAccountApplicationResultDto;
import au.com.voltbank.sis.model.VoltAuthIdPairDto;
import au.com.voltbank.t24.account.TDAccountDetailBodyDto;
import au.com.voltbank.termdeposit.dto.ErrorResponse;
import au.com.voltbank.termdeposit.entity.TermDepositAccountApplication;
import au.com.voltbank.termdeposit.exception.TermDepositAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.exception.VoltIdException;
import au.com.voltbank.termdeposit.service.CustomerService;
import au.com.voltbank.termdeposit.service.RetailTermDepositRatesService;
import au.com.voltbank.termdeposit.service.SystemIdStoreService;
import au.com.voltbank.termdeposit.service.TermDepositAccountApplicationService;
import au.com.voltbank.termdeposit.util.CannedData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tyro.oss.randomdata.RandomString;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.security.Principal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = TermDepositAccountController.class, secure = false)
public class TermDepositAccountControllerTest {

  @Autowired private MockMvc mockMvc;

  @Autowired private ObjectMapper objectMapper;

  @MockBean private TermDepositAccountApplicationService termDepositAccountApplicationService;

  @MockBean private RetailTermDepositRatesService retailTermDepositRatesService;

  @MockBean private SystemIdStoreService systemIdStoreService;

  private TdAccountApplicationDataDto applicationDataDto;

  private TDAccountDetailBodyDto tdAccountResponseBody;

  private VoltAuthIdPairDto voltAuthIdPairDto;

  @MockBean private CustomerService customerService;

  @Before
  public void setUp() throws VoltCustomException, IOException, TermDepositAccountNotFoundException {
    applicationDataDto = CannedData.getTdAccountApplicationDataDto();
    tdAccountResponseBody = CannedData.getTDAccountResponseBody();
    TermDepositAccountApplication result = CannedData.getDepositAccountApplication();
    when(termDepositAccountApplicationService.create(
            any(TdAccountApplicationDataDto.class), anyString()))
        .thenReturn(result);
    when(termDepositAccountApplicationService.getTermDepositAccountDetails(result))
        .thenReturn(tdAccountResponseBody);

    String authId = RandomString.randomNumericString(8);
    String voltId = RandomString.randomNumericString(8);

    voltAuthIdPairDto = new VoltAuthIdPairDto();
    voltAuthIdPairDto.setAuthId(authId);
    voltAuthIdPairDto.setVoltId(voltId);

    when(retailTermDepositRatesService.validateRate(any(RetailTermDepositRateDto.class)))
        .thenReturn(true);
    when(systemIdStoreService.getVoltId(authId)).thenReturn(voltAuthIdPairDto);
  }

  @After
  public void reset() {
    Mockito.reset(termDepositAccountApplicationService, systemIdStoreService);
  }

  @Test
  public void shouldReturn_201_IfNoErrorsOccur_ForCreateAccount() throws Exception {
    Principal principal = () -> voltAuthIdPairDto.getAuthId();
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);
    String response =
        this.mockMvc
            .perform(
                post("/term_deposit/account")
                    .principal(jwt)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(toJson(applicationDataDto)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString();

    verify(termDepositAccountApplicationService)
        .create(applicationDataDto, voltAuthIdPairDto.getVoltId());
    verify(termDepositAccountApplicationService).getTermDepositAccountDetails(any());

    TdAccountApplicationResultDto resultDto =
        fromJson(response, TdAccountApplicationResultDto.class);
    assertThat(resultDto.getBsb(), is("517000"));
    assertThat(resultDto.getAccountNumber(), is("100004335"));
    assertThat(resultDto.getPrincipalBalanceInWholeDollars(), is(6000));
    assertThat(resultDto.getInterestRate(), is("2.01"));
    assertThat(resultDto.getCancellationDate(), is("2018-03-30"));
    assertThat(resultDto.getMaturityDate(), is("2019-06-25"));
  }

  @Test
  public void shouldReturn_400_IfNoCustomerExists_ForCreateAccount() throws Exception {
    Principal principal = () -> voltAuthIdPairDto.getAuthId();
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);

    when(termDepositAccountApplicationService.create(
            applicationDataDto, voltAuthIdPairDto.getVoltId()))
        .thenThrow(new VoltCustomException("test 400", voltAuthIdPairDto.getVoltId(), 400));

    this.mockMvc
        .perform(
            post("/term_deposit/account")
                .principal(jwt)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(applicationDataDto))
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(content().json(objectMapper.writeValueAsString(new ErrorResponse("0500100"))))
        .andReturn();
  }

  @Test
  public void shouldReturn_404_IfCannotFindTermDepositAccount_AfterCreateAccount()
      throws Exception {
    Principal principal = () -> voltAuthIdPairDto.getAuthId();
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);

    TermDepositAccountApplication expectedTermDepositAccount =
        new TermDepositAccountApplication()
            .toBuilder()
            .arrangementId(RandomString.randomNumericString())
            .t24CustomerId(RandomString.randomNumericString())
            .build();
    when(termDepositAccountApplicationService.create(
            applicationDataDto, voltAuthIdPairDto.getVoltId()))
        .thenReturn(expectedTermDepositAccount);

    when(termDepositAccountApplicationService.getTermDepositAccountDetails(
            expectedTermDepositAccount))
        .thenThrow(new TermDepositAccountNotFoundException("WTF?!!"));

    this.mockMvc
        .perform(
            post("/term_deposit/account")
                .principal(jwt)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(applicationDataDto))
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound())
        .andExpect(content().json(objectMapper.writeValueAsString(new ErrorResponse("0500200"))))
        .andReturn();
  }

  @Test
  public void
      shouldReturn_400_IfCustomerKycStatusIsNonActiveshouldReturn_400_IfCustomerKycStatusIsNonActive()
          throws Exception {
    Principal principal = () -> voltAuthIdPairDto.getAuthId();
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);

    when(termDepositAccountApplicationService.create(
            applicationDataDto, voltAuthIdPairDto.getVoltId()))
        .thenThrow(
            new VoltCustomException(
                Constants.ERROR_SYSTEM,
                "Can not create account for a kyc failed customer - voltId:"
                    + voltAuthIdPairDto.getVoltId(),
                HttpStatus.INTERNAL_SERVER_ERROR.value()));

    this.mockMvc
        .perform(
            post("/term_deposit/account")
                .principal(jwt)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(applicationDataDto))
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(
            content()
                .json(
                    objectMapper.writeValueAsString(
                        new ErrorResponse(Constants.ERROR_CUSTOMER_NOT_FOUND))))
        .andReturn();
  }

  @Test
  public void shouldReturn_500_IfNoneJsonFormattedDataPassedIn_ForCreateAccountApplication()
      throws Exception {
    Principal principal = () -> voltAuthIdPairDto.getAuthId();
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);

    this.mockMvc
        .perform(
            post("/term_deposit/account")
                .principal(jwt)
                .contentType(APPLICATION_JSON)
                .content("xxxx"))
        .andDo(print())
        .andExpect(status().isInternalServerError());

    verifyZeroInteractions(termDepositAccountApplicationService);
  }

  @Test
  public void shouldReturn_400_IfTermInMonthsMissing() throws Exception {
    Principal principal = () -> voltAuthIdPairDto.getAuthId();
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);

    applicationDataDto.setTermInMonths(null);
    this.mockMvc
        .perform(
            post("/term_deposit/account")
                .principal(jwt)
                .contentType(APPLICATION_JSON)
                .content(toJson(applicationDataDto)))
        .andDo(print())
        .andExpect(status().isBadRequest());

    verifyZeroInteractions(termDepositAccountApplicationService);

    applicationDataDto.setTermInMonths(-10);
    this.mockMvc
        .perform(
            post("/term_deposit/account")
                .principal(jwt)
                .contentType(APPLICATION_JSON)
                .content(toJson(applicationDataDto)))
        .andDo(print())
        .andExpect(status().isBadRequest());

    verifyZeroInteractions(termDepositAccountApplicationService);

    applicationDataDto.setTermInMonths(0);
    this.mockMvc
        .perform(
            post("/term_deposit/account")
                .principal(jwt)
                .contentType(APPLICATION_JSON)
                .content(toJson(applicationDataDto)))
        .andDo(print())
        .andExpect(status().isBadRequest());

    verifyZeroInteractions(termDepositAccountApplicationService);
  }

  @Test
  public void shouldReturn_400_IfAmountIsLessThanOrEqualToZero() throws Exception {
    Principal principal = () -> voltAuthIdPairDto.getAuthId();
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);

    applicationDataDto.setAmountInWholeDollars(null);
    this.mockMvc
        .perform(
            post("/term_deposit/account")
                .principal(jwt)
                .contentType(APPLICATION_JSON)
                .content(toJson(applicationDataDto)))
        .andDo(print())
        .andExpect(status().isBadRequest());

    applicationDataDto.setAmountInWholeDollars(-10);
    this.mockMvc
        .perform(
            post("/term_deposit/account")
                .principal(jwt)
                .contentType(APPLICATION_JSON)
                .content(toJson(applicationDataDto)))
        .andDo(print())
        .andExpect(status().isBadRequest());

    applicationDataDto.setAmountInWholeDollars(0);
    this.mockMvc
        .perform(
            post("/term_deposit/account")
                .principal(jwt)
                .contentType(APPLICATION_JSON)
                .content(toJson(applicationDataDto)))
        .andDo(print())
        .andExpect(status().isBadRequest());

    verifyZeroInteractions(termDepositAccountApplicationService);
  }

  @Test
  public void shouldReturn_400_IfLinkedAccountNameMissing() throws Exception {
    Principal principal = () -> voltAuthIdPairDto.getAuthId();
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);

    applicationDataDto.setLinkedAccountName(null);
    this.mockMvc
        .perform(
            post("/term_deposit/account")
                .principal(jwt)
                .contentType(APPLICATION_JSON)
                .content(toJson(applicationDataDto)))
        .andDo(print())
        .andExpect(status().isBadRequest());

    verifyZeroInteractions(termDepositAccountApplicationService);
  }

  @Test
  public void shouldReturn_400_IfLinkedAccountBsbMissing() throws Exception {
    Principal principal = () -> voltAuthIdPairDto.getAuthId();
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);

    applicationDataDto.setLinkedAccountBSB(null);
    this.mockMvc
        .perform(
            post("/term_deposit/account")
                .principal(jwt)
                .contentType(APPLICATION_JSON)
                .content(toJson(applicationDataDto)))
        .andDo(print())
        .andExpect(status().isBadRequest());

    verifyZeroInteractions(termDepositAccountApplicationService);
  }

  @Test
  public void shouldReturn_400_IfLinkedAccountNumberMissing() throws Exception {
    Principal principal = () -> voltAuthIdPairDto.getAuthId();
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);

    applicationDataDto.setLinkedAccountNumber(null);
    this.mockMvc
        .perform(
            post("/term_deposit/account")
                .principal(jwt)
                .contentType(APPLICATION_JSON)
                .content(toJson(applicationDataDto)))
        .andDo(print())
        .andExpect(status().isBadRequest());

    verifyZeroInteractions(termDepositAccountApplicationService);
  }

  @Test
  public void shouldReturn_500_If_voltId_not_exist() throws Exception {
    String authId = RandomString.randomNumericString();
    when(systemIdStoreService.getVoltId(authId))
        .thenThrow(new VoltIdException("Did not find voltId by authId"));
    Principal principal = () -> authId;
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);
    this.mockMvc
        .perform(
            post("/term_deposit/account")
                .principal(jwt)
                .contentType(APPLICATION_JSON)
                .content(toJson(applicationDataDto)))
        .andDo(print())
        .andExpect(status().isInternalServerError());

    verifyZeroInteractions(termDepositAccountApplicationService);
  }

  private String toJson(Object toBeSerialized) throws JsonProcessingException {
    return objectMapper.writeValueAsString(toBeSerialized);
  }

  private <T> T fromJson(String json, Class<T> clazz) throws IOException {
    return objectMapper.readValue(json, clazz);
  }
}
