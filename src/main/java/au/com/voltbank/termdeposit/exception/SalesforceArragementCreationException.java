package au.com.voltbank.termdeposit.exception;

public class SalesforceArragementCreationException extends Exception {

  public SalesforceArragementCreationException(String message) {
    super(message);
  }

  public SalesforceArragementCreationException(String message, Throwable cause) {
    super(message, cause);
  }
}
