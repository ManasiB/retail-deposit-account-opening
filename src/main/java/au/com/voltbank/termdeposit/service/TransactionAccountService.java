package au.com.voltbank.termdeposit.service;

import au.com.voltbank.account.model.TransactionAccountApplicationDataDto;
import au.com.voltbank.customer.model.CustomerBankingCreateResponseDto;
import au.com.voltbank.t24.T24ResponseBody;
import au.com.voltbank.t24.account.AccountDetailBodyDto;
import au.com.voltbank.t24.account.CreateAccountResponseBodyDto;
import au.com.voltbank.termdeposit.corebanking.service.T24AccountService;
import au.com.voltbank.termdeposit.dao.TransactionAccountApplicationRepository;
import au.com.voltbank.termdeposit.entity.TransactionAccountApplication;
import au.com.voltbank.termdeposit.exception.TransactionAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import com.fasterxml.jackson.core.type.TypeReference;
import java.util.List;
import java.util.function.Predicate;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class TransactionAccountService {

  @Autowired private T24AccountService t24AccountService;

  @Autowired
  private TransactionAccountApplicationRepository transactionAccountApplicationRepository;

  @Autowired private MappingJackson2HttpMessageConverter converter;

  @Autowired private CustomerService customerService;

  @Autowired private ModelMapper modelMapper;

  @Transactional
  public TransactionAccountApplication create(
      TransactionAccountApplicationDataDto accountApplicationDto, String voltId)
      throws VoltCustomException {

    TransactionAccountApplication accountApplication = modelMapper
        .map(accountApplicationDto, TransactionAccountApplication.class);

    CustomerBankingCreateResponseDto customer = customerService.createt24CustomerIfnotExist(voltId);
    accountApplication.setT24CustomerId(customer.getT24CustomerId());
    accountApplication.setVoltId(voltId);

    ResponseEntity<T24ResponseBody> response = t24AccountService
        .createTransactionAccount(accountApplication);
    CreateAccountResponseBodyDto responseDto = converter.getObjectMapper()
        .convertValue(response.getBody().getBody(), CreateAccountResponseBodyDto.class);

    String arrangementId = responseDto.getArrangementActivity().getArrangementId();
    accountApplication.setArrangementId(arrangementId);
    transactionAccountApplicationRepository.save(accountApplication);

    return accountApplication;
  }

  public TransactionAccountApplication createAsyncMock(
      TransactionAccountApplicationDataDto accountApplicationDto, String voltId)
      throws VoltCustomException {

    TransactionAccountApplication accountApplication =
        modelMapper.map(accountApplicationDto, TransactionAccountApplication.class);

    CustomerBankingCreateResponseDto customer = customerService.createt24CustomerIfnotExist(voltId);
    accountApplication.setT24CustomerId(customer.getT24CustomerId());
    accountApplication.setVoltId(voltId);
    accountApplication.setArrangementId(
        "mocked_arrangement_id_" + accountApplicationDto.getAccountName());
    return accountApplication;
  }

  public AccountDetailBodyDto getAccountDetails(
      TransactionAccountApplication transactionAccountApplication)
      throws VoltCustomException, TransactionAccountNotFoundException {
    ResponseEntity<T24ResponseBody> responseEntity =
        t24AccountService.getAccounts(transactionAccountApplication.getT24CustomerId());

    List<AccountDetailBodyDto> accountDetailList = converter.getObjectMapper()
        .convertValue(responseEntity.getBody().getBody(),
            new TypeReference<List<AccountDetailBodyDto>>() {
            });

    return accountDetailList.stream()
        .filter(filterAccounts(transactionAccountApplication.getArrangementId())).findFirst()
        .orElseThrow(() -> new TransactionAccountNotFoundException(String.format(
            "Transaction Account not found for arrangementId: %s on customer with t24CustomerId: %s",
            transactionAccountApplication.getArrangementId(),
            transactionAccountApplication.getT24CustomerId())));
  }

  private Predicate<AccountDetailBodyDto> filterAccounts(String arrangementId) {
    return accountDetail -> arrangementId.equals(accountDetail.getArrangementId());
  }
}
