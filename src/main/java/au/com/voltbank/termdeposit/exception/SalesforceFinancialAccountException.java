package au.com.voltbank.termdeposit.exception;

public class SalesforceFinancialAccountException extends Exception {

  public SalesforceFinancialAccountException(String message) {
    super(message);
  }

  public SalesforceFinancialAccountException(String message, Throwable cause) {
    super(message, cause);
  }
}
