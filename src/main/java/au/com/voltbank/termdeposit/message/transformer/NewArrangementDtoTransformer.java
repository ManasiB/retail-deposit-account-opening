package au.com.voltbank.termdeposit.message.transformer;

import au.com.voltbank.termdeposit.dto.NewDepositArrangementDto;
import au.com.voltbank.termdeposit.exception.CloudQueueMessageTransformationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.azure.storage.queue.CloudQueueMessage;
import org.springframework.integration.transformer.GenericTransformer;

@OptimusPrime
public class NewArrangementDtoTransformer
    implements GenericTransformer<NewDepositArrangementDto, CloudQueueMessage> {

  private ObjectMapper objectMapper;

  public NewArrangementDtoTransformer(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  @Override
  public CloudQueueMessage transform(NewDepositArrangementDto newDepositArrangementDto) {
    try {
      return new CloudQueueMessage(objectMapper.writeValueAsString(newDepositArrangementDto));
    } catch (JsonProcessingException e) {
      throw new CloudQueueMessageTransformationException(
          String.format(
              "Failed to serialize NewDepositArrangementDto to CloudQueueMessage for arrangementId %s and voltId %s.",
              newDepositArrangementDto.getArrangementId(), newDepositArrangementDto.getVoltId()),
          e);
    }
  }
}
