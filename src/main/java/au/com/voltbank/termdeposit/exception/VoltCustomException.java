package au.com.voltbank.termdeposit.exception;

import liquibase.util.StringUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@NoArgsConstructor
@Getter
@ToString
@Slf4j
public class VoltCustomException extends Exception {

  private String errCode;

  private String message;

  private int httpErrorCode;

  public VoltCustomException(String errCode, String message, int httpErrorCode) {
    if (!StringUtils.isEmpty(message)) {
      log.error(message);
    }
    this.errCode = errCode;
    this.message = message;
    this.httpErrorCode = httpErrorCode;
  }

  public VoltCustomException(String errCode, String message) {
    if (!StringUtils.isEmpty(message)) {
      log.error(message);
    }
    this.errCode = errCode;
    this.message = message;
  }
}
