package au.com.voltbank.termdeposit.controller;

import au.com.voltbank.account.model.ProductIdDto;
import au.com.voltbank.account.model.TransactionAccountListDto;
import au.com.voltbank.sis.model.VoltAuthIdPairDto;
import au.com.voltbank.termdeposit.dto.ErrorResponse;
import au.com.voltbank.termdeposit.exception.T24IdException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.exception.VoltIdException;
import au.com.voltbank.termdeposit.service.AccountListService;
import au.com.voltbank.termdeposit.service.SystemIdStoreService;
import au.com.voltbank.termdeposit.util.CannedData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tyro.oss.randomdata.RandomString;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.security.Principal;

import static au.com.voltbank.Constants.ERROR_SYSTEM;
import static au.com.voltbank.Constants.ERROR_VOLT_ID_NOT_FOUND;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = AccountListController.class, secure = false)
public class AccountListControllerTest {

  @Autowired private MockMvc mockMvc;

  @Autowired private ObjectMapper objectMapper;

  @MockBean private AccountListService accountListService;

  @MockBean private SystemIdStoreService systemIdStoreService;

  @After
  public void reset() {
    Mockito.reset(accountListService, systemIdStoreService);
  }

  @Test
  public void shouldReturn_201_IfNoErrorsOccur_ForGettingAccounts() throws Exception {
    VoltAuthIdPairDto voltAuthIdPairDto = getVoltAuthIdPairDto();
    Principal principal = voltAuthIdPairDto::getAuthId;
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);

    TransactionAccountListDto transactionAccountListDto = CannedData.getTransactionAccountListDto();

    when(systemIdStoreService.getVoltId(voltAuthIdPairDto.getAuthId()))
        .thenReturn(voltAuthIdPairDto);
    when(systemIdStoreService.getT24CustomerIdForVoltId(voltAuthIdPairDto.getVoltId()))
        .thenReturn(CannedData.getVoltT24IdPairDto());
    when(accountListService.getAccountsList(any(), any())).thenReturn(transactionAccountListDto);

    String response =
        this.mockMvc
            .perform(get("/accounts").principal(jwt).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

    TransactionAccountListDto resultDto = fromJson(response, TransactionAccountListDto.class);

    assertThat(resultDto.getData().get(0).getAvailableBalance(), is("877638763"));
    assertThat(resultDto.getData().get(0).getBsb(), is("000"));
    assertThat(resultDto.getData().get(0).getProductId(), is(ProductIdDto._000));
  }

  @Test
  public void shouldReturn_400_ForSomeExceptionInAccountListService_ForGetAccounts()
      throws Exception {
    VoltAuthIdPairDto voltAuthIdPairDto = getVoltAuthIdPairDto();
    Principal principal = voltAuthIdPairDto::getAuthId;
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);

    when(systemIdStoreService.getVoltId(voltAuthIdPairDto.getAuthId()))
        .thenReturn(voltAuthIdPairDto);
    when(systemIdStoreService.getT24CustomerIdForVoltId(voltAuthIdPairDto.getVoltId()))
        .thenReturn(CannedData.getVoltT24IdPairDto());
    when(accountListService.getAccountsList(any(), any())).thenThrow(new VoltCustomException());

    this.mockMvc
        .perform(get("/accounts").principal(jwt).contentType(APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isBadRequest());
  }

  @Test
  public void shouldReturn_400_If_voltId_not_exist() throws Exception {
    String authId = RandomString.randomNumericString();

    when(systemIdStoreService.getVoltId(authId))
        .thenThrow(new VoltIdException("Did not find voltId by authId"));
    Principal principal = () -> authId;
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);
    this.mockMvc
        .perform(get("/accounts").principal(jwt).contentType(APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isBadRequest())
        .andExpect(content().json(toJson(new ErrorResponse(ERROR_VOLT_ID_NOT_FOUND))));

		verifyZeroInteractions(accountListService);
  }

  @Test
  public void shouldReturn_400_If_t24Id_not_exist() throws Exception {
    String authId = RandomString.randomNumericString();
    VoltAuthIdPairDto voltAuthIdPairDto = getVoltAuthIdPairDto();
    when(systemIdStoreService.getVoltId(authId)).thenReturn(voltAuthIdPairDto);
    when(systemIdStoreService.getT24CustomerIdForVoltId(voltAuthIdPairDto.getVoltId()))
        .thenThrow(new T24IdException("Could not find t24 id"));
    Principal principal = () -> authId;
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);
    this.mockMvc
        .perform(get("/accounts").principal(jwt).contentType(APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isBadRequest())
        .andExpect(content().json(toJson(new ErrorResponse(ERROR_SYSTEM))));

		verifyZeroInteractions(accountListService);
  }

  private String toJson(Object toBeSerialized) throws JsonProcessingException {
    return objectMapper.writeValueAsString(toBeSerialized);
  }

  private <T> T fromJson(String json, Class<T> clazz) throws IOException {
    return objectMapper.readValue(json, clazz);
  }

  private VoltAuthIdPairDto getVoltAuthIdPairDto() {
    String authId = RandomString.randomNumericString(8);
    String voltId = RandomString.randomNumericString(8);

    VoltAuthIdPairDto voltAuthIdPairDto = new VoltAuthIdPairDto();
    voltAuthIdPairDto.setAuthId(authId);
    voltAuthIdPairDto.setVoltId(voltId);
    return voltAuthIdPairDto;
  }
}
