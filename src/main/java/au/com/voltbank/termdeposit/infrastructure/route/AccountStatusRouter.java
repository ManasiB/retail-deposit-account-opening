package au.com.voltbank.termdeposit.infrastructure.route;

import static org.apache.camel.LoggingLevel.DEBUG;
import static org.apache.camel.LoggingLevel.ERROR;

import java.util.ArrayList;
import javax.sql.DataSource;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.Router;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.queue.CloudQueue;

import au.com.voltbank.message.azure.AzureQueue;
import au.com.voltbank.termdeposit.message.processor.StatusProcessor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@Service
public class AccountStatusRouter extends AzureQueue { 


  


	  static final int MAXIMUM_REDELIVERIES_THREE = 3;
	  static final double BACK_OFF_MULTIPLIER = 1.2D;
	  static final long DELAY_ONE_SECOND = 1000L;
	  // producer for sale force financial account creation queue
	  public static final String PRODUCER_CREATE_FINANCIAL_ACCOUNT_FOR_SFCUSTOMER = "direct:createFinancialAccountForSFCustomer";
	  public static final String RID_SALESFORCE_CREATION_SENT_TO_QUEUE = "rid-newfinancialaccountsalesforcequeue";
	  protected static final String QUEUE_NEW_FINANCIAL_ACCOUNT_FOR_SALESFORCE_CUSTOMER = "new-financial-account-for-salesforce-customer";
	  protected static final String RID_SALESFORCE_FINANCIAL_ACCOUNT_CREATION_MSG_RETRIEVED = "rid-financialaccountsalesforcecustomer";
	  protected static final String RID_MSG_RETRIEVED_FROM_ACCOUNT_MANAGEMENT = "rid-retrievemessagefromaccount";
	  protected static final String RID_DELETEMSG = "rid-deleteMsg";
	  protected static final String RID_DEAD_LETTER_CHANNEL = "rid-deadLetterChannel";
	  protected static final String BEAN_CREATE_FINANCIAL_ACCOUNT_SF_CUSTOMER = "beanCreateFinancialAccountForSalesforceCustomer";
	  protected static final String QUEUE_RETREIVE_ACCOUNT_STATUS = "accountmgmt";
	  protected static final String QUEUE_RETREIVE_ACCOUNT_STATUS_DELETE_MESSAGE = "ACCOUNT_MGMT_ARCHIVE";
	  // Azure client
	  protected static final String AZ_CLIENT_SALESFORCE_FINANCIAL_ACCOUNT_CREATE = "AZ_SF_FINANCIAL_ACCOUNT_CREATE";

	  @Bean(QUEUE_RETREIVE_ACCOUNT_STATUS_DELETE_MESSAGE)
	  CloudQueue queue() throws StorageException {
	    return makeClient(QUEUE_RETREIVE_ACCOUNT_STATUS);
	  }

	  @Override
	  public void configure() {
	    // route for create new Salesforce customer and update SIS
		  
		  System.out.println("iN ACCOUNT STATUS Router");
	    from(retrieveMessageFromAccountManagement())
	        .routeId(RID_MSG_RETRIEVED_FROM_ACCOUNT_MANAGEMENT)
	        .choice()
	        .when(exchange -> exchange.getMessage().getBody() != null)
	        .log(DEBUG, "voltId: \"${body.messageContentAsString}\"")
	        // Rest client calls, retry defined
	        .bean(StatusProcessor.class)
/*	        .id(BEAN_CREATE_FINANCIAL_ACCOUNT_SF_CUSTOMER)
	        .id(BEAN_CREATE_FINANCIAL_ACCOUNT_SF_CUSTOMER)
	        .to(deleteSalesforceCustomerCreateMsg())
	        .id(RID_DELETEMSG)*/
	        .otherwise()
	        .log(DEBUG, String.format("empty message, delay %s seconds.", DELAY_ONE_SECOND))
	        .delay(DELAY_ONE_SECOND)
	        .endChoice();

	  }
	  


		  String retrieveMessageFromAccountManagement() {
		    return makeQueueUri(QUEUE_RETREIVE_ACCOUNT_STATUS, QUEUE_RETREIVE_ACCOUNT_STATUS_DELETE_MESSAGE);
		  }


  }


