package au.com.voltbank.termdeposit.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AzureTokenResponse {
  @JsonProperty("token_type")
  private String tokenType;

  @JsonProperty("expires_in")
  private String expiresIn;

  @JsonProperty("ext_expires_in")
  private String extExpiresIn;

  @JsonProperty("expires_on")
  private String expiresOn;

  @JsonProperty("not_before")
  private String notBefore;

  private String resource;

  @JsonProperty("access_token")
  private String accessToken;
}
