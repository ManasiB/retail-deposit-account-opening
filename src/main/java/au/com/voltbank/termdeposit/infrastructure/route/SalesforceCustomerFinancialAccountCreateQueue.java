package au.com.voltbank.termdeposit.infrastructure.route;


import au.com.voltbank.message.azure.AzureQueue;
import au.com.voltbank.termdeposit.exception.SalesforceArragementCreationException;
import au.com.voltbank.termdeposit.exception.SavingsAccountNotFoundException;
import au.com.voltbank.termdeposit.message.processor.SalesforceFinancialAccountCreateProcessor;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.queue.CloudQueue;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.LoggingLevel;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestClientException;

import static org.apache.camel.LoggingLevel.DEBUG;
import static org.apache.camel.LoggingLevel.ERROR;

@Slf4j
@Configuration
public class SalesforceCustomerFinancialAccountCreateQueue extends AzureQueue {

  static final int MAXIMUM_REDELIVERIES_THREE = 3;
  static final double BACK_OFF_MULTIPLIER = 1.2D;
  static final long DELAY_ONE_SECOND = 1000L;
  // producer for sale force financial account creation queue
  public static final String PRODUCER_CREATE_FINANCIAL_ACCOUNT_FOR_SFCUSTOMER = "direct:createFinancialAccountForSFCustomer";
  public static final String RID_SALESFORCE_CREATION_SENT_TO_QUEUE = "rid-newfinancialaccountsalesforcequeue";
  protected static final String QUEUE_NEW_FINANCIAL_ACCOUNT_FOR_SALESFORCE_CUSTOMER = "new-financial-account-for-salesforce-customer";
  protected static final String RID_SALESFORCE_FINANCIAL_ACCOUNT_CREATION_MSG_RETRIEVED = "rid-financialaccountsalesforcecustomer";
  protected static final String RID_DELETEMSG = "rid-deleteMsg";
  protected static final String RID_DEAD_LETTER_CHANNEL = "rid-deadLetterChannel";
  protected static final String BEAN_CREATE_FINANCIAL_ACCOUNT_SF_CUSTOMER = "beanCreateFinancialAccountForSalesforceCustomer";

  // Azure client
  protected static final String AZ_CLIENT_SALESFORCE_FINANCIAL_ACCOUNT_CREATE = "AZ_SF_FINANCIAL_ACCOUNT_CREATE";

  @Bean(AZ_CLIENT_SALESFORCE_FINANCIAL_ACCOUNT_CREATE)
  CloudQueue queue() throws StorageException {
    return makeClient(QUEUE_NEW_FINANCIAL_ACCOUNT_FOR_SALESFORCE_CUSTOMER);
  }

  @Override
  public void configure() {
    onException(SavingsAccountNotFoundException.class, SalesforceArragementCreationException.class)
        .handled(true)
        .log("VoltId \"${body.messageContentAsString}\" does not exist")
        .to(deleteSalesforceCustomerCreateMsg())
        .id(RID_DEAD_LETTER_CHANNEL);

    onException(RestClientException.class)
        .handled(true)
        .retryAttemptedLogLevel(LoggingLevel.WARN)
        .maximumRedeliveries(MAXIMUM_REDELIVERIES_THREE)
        .backOffMultiplier(BACK_OFF_MULTIPLIER)
        .useExponentialBackOff()
        .log(ERROR, "Failed to deliver via RestClient")
        .to(deleteSalesforceCustomerCreateMsg())
        .id(RID_DEAD_LETTER_CHANNEL);

    // route for create new Salesforce customer and update SIS
    from(retrieveCreateAccountLinkForSalesforceCustomerMsg())
        .routeId(RID_SALESFORCE_FINANCIAL_ACCOUNT_CREATION_MSG_RETRIEVED)
        .choice()
        .when(exchange -> exchange.getMessage().getBody() != null)
        .log(DEBUG, "voltId: \"${body.messageContentAsString}\"")
        // Rest client calls, retry defined
        .bean(SalesforceFinancialAccountCreateProcessor.class)
        .id(BEAN_CREATE_FINANCIAL_ACCOUNT_SF_CUSTOMER)
        .to(deleteSalesforceCustomerCreateMsg())
        .id(RID_DELETEMSG)
        .otherwise()
        .log(DEBUG, String.format("empty message, delay %s seconds.", DELAY_ONE_SECOND))
        .delay(DELAY_ONE_SECOND)
        .endChoice();

    // add new message to sales force customer create queue
    from(PRODUCER_CREATE_FINANCIAL_ACCOUNT_FOR_SFCUSTOMER)
        .to(addFinancialAccountForSalesforceCustomerCreateMsg())
        .id(RID_SALESFORCE_CREATION_SENT_TO_QUEUE);
  }

  String deleteSalesforceCustomerCreateMsg() {
    return makeMsgDelUri(QUEUE_NEW_FINANCIAL_ACCOUNT_FOR_SALESFORCE_CUSTOMER, AZ_CLIENT_SALESFORCE_FINANCIAL_ACCOUNT_CREATE);
  }

  String retrieveCreateAccountLinkForSalesforceCustomerMsg() {
    return makeQueueUri(QUEUE_NEW_FINANCIAL_ACCOUNT_FOR_SALESFORCE_CUSTOMER, AZ_CLIENT_SALESFORCE_FINANCIAL_ACCOUNT_CREATE);
  }

  String addFinancialAccountForSalesforceCustomerCreateMsg() {
    return makeMsgAddUri(QUEUE_NEW_FINANCIAL_ACCOUNT_FOR_SALESFORCE_CUSTOMER, AZ_CLIENT_SALESFORCE_FINANCIAL_ACCOUNT_CREATE);
  }
}
