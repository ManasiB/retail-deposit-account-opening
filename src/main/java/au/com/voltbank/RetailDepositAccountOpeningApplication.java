package au.com.voltbank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class RetailDepositAccountOpeningApplication {

  public static void main(String[] args) {
    SpringApplication.run(RetailDepositAccountOpeningApplication.class, args);
  }
}
