package au.com.voltbank.termdeposit.mapper;

import au.com.voltbank.account.model.SavingsAccountApplicationDataDto;
import au.com.voltbank.termdeposit.entity.SavingsAccountApplication;
import au.com.voltbank.termdeposit.util.CannedData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class SavingsAccountMapperTest {

  @Test
  public void shouldMapDtoToEntity() {
    // setUp
    SavingsAccountApplicationDataDto dto = CannedData.getSavingsAccountApplicationDataDto();

    // test
    SavingsAccountApplication entity = AccountMapper.mapToSavingsAccountApplicationEntity(dto);
    assertThat(entity.getAccountName(), is("Shane's Holiday Bucket"));
    assertThat(entity.getNominatedAccountBSB(), is("969100"));
    assertThat(entity.getNominatedAccountNumber(), is("12345678"));
    assertThat(entity.getNominatedAccountName(), is("Shane's Linked Account"));
  }
}
