package au.com.voltbank.termdeposit.exception;

public class VoltIdException extends RuntimeException {

  public VoltIdException(String message) {
    super(message);
  }

  public VoltIdException(String message, Throwable cause) {
    super(message, cause);
  }
}
