import au.com.voltbank.RetailDepositAccountOpeningApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=RetailDepositAccountOpeningApplication.class)
public class ApplicationIT {

    @Test
    public void applicationContextLoaded() {
    }

    @Test
    public void applicationContextTest() {
        RetailDepositAccountOpeningApplication.main(new String[] {});
    }

}
