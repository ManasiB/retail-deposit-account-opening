package au.com.voltbank.termdeposit.service;

import au.com.voltbank.Constants;
import au.com.voltbank.account.model.RetailTermDepositRateDto;
import au.com.voltbank.termdeposit.dao.RetailTermDepositRatesRepository;
import au.com.voltbank.termdeposit.entity.RetailTermDepositRate;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import io.jsonwebtoken.lang.Collections;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RetailTermDepositRatesService {

  @Autowired private RetailTermDepositRatesRepository rtdRatesDao;

  @Autowired private ModelMapper modelMapper;

  /**
   * 1. Make sure the stored rates are up to date. 2. Retrieve stored rates from table and return
   *
   * @return stored rates in rates table
   */
  public List<RetailTermDepositRateDto> fetchLatestRates() throws VoltCustomException {
    List<RetailTermDepositRate> ratesList = rtdRatesDao.findLatestRates(new Date());
    if (Collections.isEmpty(ratesList)) {
      throw new VoltCustomException(
          Constants.ERROR_SYSTEM,
          "Rates table is empty!",
          HttpStatus.INTERNAL_SERVER_ERROR.value());
    }

    return ratesList.stream()
        .map(item -> modelMapper.map(item, RetailTermDepositRateDto.class))
        .collect(Collectors.toList());
  }

  /**
   * Validate if it's a legit rate submitted
   *
   * @param dto Rates submited by front end
   * @return
   */
  public boolean validateRate(RetailTermDepositRateDto dto) {
    List<RetailTermDepositRate> rateList =
        rtdRatesDao.findTargetRateByTermAndIndexCode(
            dto.getTermInMonths(), dto.getIndexCode(), new Date());
    if (Collections.isEmpty(rateList)) {
      return false;
    }
    return dto.getPercentRate().equals(rateList.get(0).getPercentRate());
  }
}
