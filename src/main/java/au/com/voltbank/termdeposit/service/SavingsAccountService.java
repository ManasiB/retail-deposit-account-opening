package au.com.voltbank.termdeposit.service;

import au.com.voltbank.account.model.SavingsAccountApplicationDataDto;
import au.com.voltbank.customer.model.CustomerBankingCreateResponseDto;
import au.com.voltbank.sis.model.VoltT24IdPairDto;
import au.com.voltbank.t24.T24ResponseBody;
import au.com.voltbank.t24.account.AccountDetailBodyDto;
import au.com.voltbank.t24.account.CreateAccountResponseBodyDto;
import au.com.voltbank.termdeposit.corebanking.service.T24AccountService;
import au.com.voltbank.termdeposit.dao.SavingsAccountApplicationRepository;
import au.com.voltbank.termdeposit.entity.SavingsAccountApplication;
import au.com.voltbank.termdeposit.exception.SavingsAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.T24IdException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.infrastructure.route.SalesforceCustomerFinancialAccountCreateQueue;
import au.com.voltbank.termdeposit.mapper.AccountMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import java.util.List;
import java.util.function.Predicate;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SavingsAccountService {

  @Autowired private T24AccountService t24AccountService;

  @Autowired private SavingsAccountApplicationRepository savingsAccountApplicationRepository;

  @Autowired private MappingJackson2HttpMessageConverter converter;

  @Autowired private CustomerService customerService;

  @Autowired private SystemIdStoreService systemIdStoreService;

  @EndpointInject(uri = SalesforceCustomerFinancialAccountCreateQueue.PRODUCER_CREATE_FINANCIAL_ACCOUNT_FOR_SFCUSTOMER)
  private ProducerTemplate salesforceCustomerFinancialAccountCreateQueue;

  public SavingsAccountApplication create(SavingsAccountApplicationDataDto accountApplicationDto,
      String voltId) throws VoltCustomException {
    SavingsAccountApplication accountApplication = AccountMapper
        .mapToSavingsAccountApplicationEntity(accountApplicationDto);
    CustomerBankingCreateResponseDto customerBankingDetailsDto = customerService
        .createt24CustomerIfnotExist(voltId);
    ResponseEntity<T24ResponseBody> response = t24AccountService
        .createSavingsAccount(customerBankingDetailsDto.getT24CustomerId());
    CreateAccountResponseBodyDto responseDto = converter.getObjectMapper()
        .convertValue(response.getBody().getBody(), CreateAccountResponseBodyDto.class);
    String arrangementId = responseDto.getArrangementActivity().getArrangementId();
    accountApplication.setVoltId(voltId);
    accountApplication.setArrangementId(arrangementId);
    savingsAccountApplicationRepository.save(accountApplication);
    salesforceCustomerFinancialAccountCreateQueue.sendBody(voltId);
    return accountApplication;
  }

  public AccountDetailBodyDto getSavingsAccountDetails(
      SavingsAccountApplication savingsAccountApplication)
      throws SavingsAccountNotFoundException, VoltCustomException, T24IdException {
    VoltT24IdPairDto voltT24IdPairDto = systemIdStoreService
        .getT24CustomerIdForVoltId(savingsAccountApplication.getVoltId());
    ResponseEntity<T24ResponseBody> responseEntity = t24AccountService
        .getAccounts(voltT24IdPairDto.getT24Id());

    List<AccountDetailBodyDto> accountDetailList = converter.getObjectMapper()
        .convertValue(responseEntity.getBody().getBody(),
            new TypeReference<List<AccountDetailBodyDto>>() {
            });

    AccountDetailBodyDto accountDetailBodyDto = accountDetailList.stream()
        .filter(filterAccounts(savingsAccountApplication.getArrangementId())).findFirst()
        .orElseThrow(() -> new SavingsAccountNotFoundException(String.format(
            "Savings Account not found for arrangementId: %s on customer with t24CustomerId: %s",
            savingsAccountApplication.getArrangementId(), voltT24IdPairDto.getT24Id())));
    return accountDetailBodyDto;
  }

  private Predicate<AccountDetailBodyDto> filterAccounts(String arrangementId) {
    final Predicate<AccountDetailBodyDto> accountDetailBodyDtoPredicate = accountDetail -> arrangementId
        .equals(accountDetail.getArrangementId());
    return accountDetailBodyDtoPredicate;
  }
}
