package au.com.voltbank.termdeposit.service;

import au.com.voltbank.RetailDepositAccountOpeningApplication;
import au.com.voltbank.customer.model.CustomerBankingCreateResponseDto;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.util.CannedData;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static au.com.voltbank.termdeposit.service.CustomerService.CUSTOMER_MANAGEMENT_PATH;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailDepositAccountOpeningApplication.class)
public class CustomerServiceTest {

  @Autowired private CustomerService customerService;

  @MockBean private RestTemplate secureRestTemplate;

  @MockBean private AzureActiveDirectoryService azureActiveDirectoryService;

  private String CUSTOMER_MANAGEMENT_URL = "";

  @Value("${CUSTOMER_MANAGEMENT_HOST}")
  private String customer_management_host;

  private String PLACE_HOLDER_URL = "http://voltbank.com.au";

  @Before
  public void setUp() {
    Mockito.reset(secureRestTemplate, azureActiveDirectoryService);
    if (StringUtils.isEmpty(CUSTOMER_MANAGEMENT_URL)) {
      if (StringUtils.isEmpty(customer_management_host)) {
        customer_management_host = PLACE_HOLDER_URL;
      }
      CUSTOMER_MANAGEMENT_URL = StringUtils.join(customer_management_host, CUSTOMER_MANAGEMENT_PATH, "mockedVoltId");
    }
  }

  @Test
  public void should_get_t24Id_from_customerManagement() throws VoltCustomException {
    CustomerBankingCreateResponseDto bankingDetailsDto = CannedData.getCustomerBankingDetailsDto();
    when(azureActiveDirectoryService.getAccessToken(any())).thenReturn("a_mock_azure_token");
    when(secureRestTemplate.postForEntity(anyString(), any(), any()))
        .thenReturn(ResponseEntity.ok().body(bankingDetailsDto));
    CustomerBankingCreateResponseDto result =
        customerService.createt24CustomerIfnotExist("mockedVoltId");
    assertThat("mockedT24CustomerId", is(result.getT24CustomerId()));
    assertThat("mockedVoltId", is(result.getVoltId()));
    assertThat("ACTIVE", is(result.getStatus().toString()));
  }

  @Test(expected = VoltCustomException.class)
  public void should_throw_volt_custom_exception() throws VoltCustomException {
    when(azureActiveDirectoryService.getAccessToken(any())).thenReturn("a_mock_azure_token");
    Mockito.when(secureRestTemplate.postForEntity(anyString(), any(), any()))
        .thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST));
    customerService.createt24CustomerIfnotExist("mockedVoltId");
  }
}
