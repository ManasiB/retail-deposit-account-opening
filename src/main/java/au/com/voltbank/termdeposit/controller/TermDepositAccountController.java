package au.com.voltbank.termdeposit.controller;

import au.com.voltbank.Constants;
import au.com.voltbank.account.model.RetailTermDepositRateDto;
import au.com.voltbank.account.model.TdAccountApplicationDataDto;
import au.com.voltbank.sis.model.VoltAuthIdPairDto;
import au.com.voltbank.termdeposit.config.VoltTermDepositIndexCode;
import au.com.voltbank.t24.account.TDAccountDetailBodyDto;
import au.com.voltbank.termdeposit.entity.TermDepositAccountApplication;
import au.com.voltbank.termdeposit.exception.TermDepositAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.service.RetailTermDepositRatesService;
import au.com.voltbank.termdeposit.service.SystemIdStoreService;
import au.com.voltbank.termdeposit.service.TermDepositAccountApplicationService;
import liquibase.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

import static au.com.voltbank.termdeposit.mapper.TermDepositAccountMapper.mapToResultDto;

@RestController
public class TermDepositAccountController {

  @Autowired private TermDepositAccountApplicationService termDepositAccountApplicationService;

  @Autowired private RetailTermDepositRatesService retailTermDepositRatesService;

  @Autowired private SystemIdStoreService systemIdStoreService;

  @PostMapping("/term_deposit/account")
  public ResponseEntity createAccount(
      @Validated @RequestBody TdAccountApplicationDataDto accountApplication,
      Authentication authentication)
      throws TermDepositAccountNotFoundException, VoltCustomException {
    validateParameters(accountApplication);
    VoltAuthIdPairDto voltAuthIdPairDto = systemIdStoreService.getVoltId(authentication.getName());
    TermDepositAccountApplication result =
        termDepositAccountApplicationService.create(
            accountApplication, voltAuthIdPairDto.getVoltId());
    TDAccountDetailBodyDto tdAccountDetail =
        termDepositAccountApplicationService.getTermDepositAccountDetails(result);
    return ResponseEntity.status(HttpStatus.CREATED).body(mapToResultDto(tdAccountDetail));
  }

  private void validateParameters(TdAccountApplicationDataDto accountApplication)
      throws VoltCustomException {
    if (accountApplication.getAmountInWholeDollars() <= 0
        || accountApplication.getTermInMonths() <= 0
        || !isActionOnMaturityValid(accountApplication.getActionOnMaturity())) {
      throw new VoltCustomException(
          Constants.ERROR_SYSTEM,
          "Validation error during account creation",
          HttpStatus.INTERNAL_SERVER_ERROR.value());
    }

    RetailTermDepositRateDto dto = new RetailTermDepositRateDto();
    dto.setTermInMonths(accountApplication.getTermInMonths());

    String indexCode = accountApplication.getIndexCode();
    indexCode =
        StringUtils.isEmpty(indexCode) ? VoltTermDepositIndexCode.YEARLY.getValue() : indexCode;
    dto.setIndexCode(indexCode);

    dto.setPercentRate(Double.parseDouble(accountApplication.getPercentRate()));
    if (!retailTermDepositRatesService.validateRate(dto)) {
      throw new VoltCustomException(
          Constants.ERROR_SYSTEM,
          "Validation failed on submitted rates",
          HttpStatus.INTERNAL_SERVER_ERROR.value());
    }
  }

  private boolean isActionOnMaturityValid(
      TdAccountApplicationDataDto.@NotNull ActionOnMaturityEnum actionOnMaturity) {
    return actionOnMaturity.equals(TdAccountApplicationDataDto.ActionOnMaturityEnum.REINVEST)
        || actionOnMaturity.equals(TdAccountApplicationDataDto.ActionOnMaturityEnum.WITHDRAW);
  }
}
