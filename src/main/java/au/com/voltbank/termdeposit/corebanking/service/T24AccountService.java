package au.com.voltbank.termdeposit.corebanking.service;

import au.com.voltbank.t24.T24RequestBody;
import au.com.voltbank.t24.T24ResponseBody;
import au.com.voltbank.t24.account.AccountHeaderDto;
import au.com.voltbank.t24.account.T24CreateDepositAccountDto;
import au.com.voltbank.t24.account.T24CreateSavingsAccountDto;
import au.com.voltbank.t24.account.T24CreateTransactionAccountDto;
import au.com.voltbank.t24.account.T24PartyDto;
import au.com.voltbank.termdeposit.corebanking.T24Client;
import au.com.voltbank.termdeposit.entity.TermDepositAccountApplication;
import au.com.voltbank.termdeposit.entity.TransactionAccountApplication;
import au.com.voltbank.termdeposit.exception.TransactionAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.mapper.TermDepositAccountMapper;
import au.com.voltbank.termdeposit.mapper.t24.T24QuirkMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import javax.annotation.PostConstruct;
import java.time.LocalDate;

import static java.util.Collections.singletonList;

@Service
public class T24AccountService {

  @Value("${T24_HOST}")
  private String t24_host;

  @Autowired private T24Client t24Client;

  private String createTermDepositUrl;

  private String createAccountUrl;

  private String getTermDepositUrl;

  private String getAccountsUrl;

  private String getTransactionsForAnAccountUrl;

  @PostConstruct
  public void init() {
    createTermDepositUrl =
        String.format(
            "%s/voltarrangementservice/product/createDepositArrangement",
            t24_host);
    createAccountUrl =
        String.format(
            "%s/voltarrangementservice/product/createAccountArrangement",
            t24_host);
    getTermDepositUrl =
        String.format(
            "%s/baasarrangementservice/product/getDepositArrangementDetails",
            t24_host);
    getAccountsUrl =
        String.format(
            "%s/baasarrangementservice/product/getAccountArrangementDetails",
            t24_host);
    getTransactionsForAnAccountUrl =
        String.format(
            "%s/baasarrangementservice/product/getTransactionList", t24_host);
  }

  public ResponseEntity<T24ResponseBody> createTermDepositAccount(
      TermDepositAccountApplication accountApplication) throws VoltCustomException {

    T24CreateDepositAccountDto t24CreateDepositAccountDto =
        TermDepositAccountMapper.toDto(accountApplication);
    AccountHeaderDto headerDto = new AccountHeaderDto();
    T24RequestBody<T24CreateDepositAccountDto, AccountHeaderDto> t24RequestBody =
        new T24RequestBody<>(headerDto, t24CreateDepositAccountDto);
    HttpEntity<T24RequestBody> payload =
        new HttpEntity<>(t24RequestBody, t24Client.getHttpRequestHeader());
    return t24Client.post(createTermDepositUrl, payload);
  }

  public ResponseEntity<T24ResponseBody> createSavingsAccount(String t24CustomerId)
      throws VoltCustomException {

    T24CreateSavingsAccountDto body =
        T24CreateSavingsAccountDto.builder()
            .effectiveDate(T24QuirkMapper.getT24Date(LocalDate.now()))
            .parties(singletonList(T24PartyDto.builder().partyId(t24CustomerId).build()))
            .build();

    T24RequestBody<T24CreateSavingsAccountDto, AccountHeaderDto> t24RequestBody =
        new T24RequestBody<>(new AccountHeaderDto(), body);
    HttpEntity<T24RequestBody> payload =
        new HttpEntity<>(t24RequestBody, t24Client.getHttpRequestHeader());
    return t24Client.post(createAccountUrl, payload);
  }

  public ResponseEntity<T24ResponseBody> createTransactionAccount(
      TransactionAccountApplication accountApplication) throws VoltCustomException {

    T24CreateTransactionAccountDto body =
        T24CreateTransactionAccountDto.builder()
            .effectiveDate(T24QuirkMapper.getT24Date(LocalDate.now()))
            .parties(
                singletonList(
                    T24PartyDto.builder().partyId(accountApplication.getT24CustomerId()).build()))
            .build();

    T24RequestBody<T24CreateTransactionAccountDto, AccountHeaderDto> t24RequestBody =
        new T24RequestBody<>(new AccountHeaderDto(), body);
    HttpEntity<T24RequestBody> payload =
        new HttpEntity<>(t24RequestBody, t24Client.getHttpRequestHeader());
    return t24Client.post(createAccountUrl, payload);
  }

  public ResponseEntity<T24ResponseBody> getTermDepositAccounts(String t24CustomerId)
      throws VoltCustomException {
    return t24Client.get(getTermDepositUrl, t24CustomerId, t24Client.getHttpRequestHeader());
  }

  public ResponseEntity<T24ResponseBody> getAccounts(String t24CustomerId)
      throws VoltCustomException {
    return t24Client.get(getAccountsUrl, t24CustomerId, t24Client.getHttpRequestHeader());
  }

  public ResponseEntity<T24ResponseBody> getAccounts(
      String t24CustomerId, MultiValueMap<String, String> requestParams)
      throws VoltCustomException {
    return t24Client.get(
        getAccountsUrl, t24CustomerId, t24Client.getHttpRequestHeader(), requestParams);
  }

  public ResponseEntity<T24ResponseBody> getTransactionsListForAnAccount(
      String accountNumber, MultiValueMap<String, String> requestParams)
      throws VoltCustomException, TransactionAccountNotFoundException {

    try {
      return t24Client.get(
          getTransactionsForAnAccountUrl,
          accountNumber,
          t24Client.getHttpRequestHeader(),
          requestParams);
    } catch (VoltCustomException ex) {
      if (ex.getHttpErrorCode() == HttpStatus.NOT_FOUND.value()) {
        throw new TransactionAccountNotFoundException(
            String.format("Transaction Account not found for account number: %s ", accountNumber));
      } else throw ex;
    }
  }
}
