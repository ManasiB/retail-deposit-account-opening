package au.com.voltbank.termdeposit.service;

import au.com.voltbank.sis.model.AuthIdDto;
import au.com.voltbank.sis.model.GreenIdDto;
import au.com.voltbank.sis.model.SalesforceIdDto;
import au.com.voltbank.sis.model.T24IdDto;
import au.com.voltbank.sis.model.VoltAuthIdPairDto;
import au.com.voltbank.sis.model.VoltGreenIdPairDto;
import au.com.voltbank.sis.model.VoltSalesforceIdPairDto;
import au.com.voltbank.sis.model.VoltT24IdPairDto;
import au.com.voltbank.termdeposit.exception.AuthIdException;
import au.com.voltbank.termdeposit.exception.GreenIdException;
import au.com.voltbank.termdeposit.exception.SalesforceIdException;
import au.com.voltbank.termdeposit.exception.T24IdException;
import au.com.voltbank.termdeposit.exception.VoltIdException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;

import static org.apache.commons.lang3.StringUtils.join;

@Slf4j
@Service
public class SystemIdStoreService {
  
  @Value("${SYSTEM_ID_STORE_HOST}")
  private String system_id_store_host = "SYSTEM_ID_STORE_HOST";

  @Autowired private RestTemplate secureRestTemplate;

  @Autowired private AzureActiveDirectoryService azureActiveDirectoryService;

  @Value("${system.id.store.api.resource.id}")
  private String resourceId;

  public VoltAuthIdPairDto getAuthId(String voltId) throws AuthIdException {
    try {
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.add(
          "Authorization", "Bearer " + azureActiveDirectoryService.getAccessToken(resourceId));
      HttpEntity<String> entity = new HttpEntity<>("parameters", httpHeaders);

      String uri =
          UriComponentsBuilder.fromUriString(
                  join(system_id_store_host, "/v1/system_id_store/auth"))
              .queryParam("voltId", voltId)
              .toUriString();
      ResponseEntity<VoltAuthIdPairDto> responseEntity =
          secureRestTemplate.exchange(uri, HttpMethod.GET, entity, VoltAuthIdPairDto.class);
      if (responseEntity.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
        throw new AuthIdException(String.format("AuthId for voltId %s not found", voltId));
      }
      return responseEntity.getBody();
    } catch (AuthIdException e) {
      throw e;
    } catch (Exception e) {
      throw new AuthIdException(
          String.format("Could not retrieve authId for voltId: %s", voltId), e);
    }
  }

  public VoltAuthIdPairDto getVoltId(String authId) {
    try {
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.add(
          "Authorization", "Bearer " + azureActiveDirectoryService.getAccessToken(resourceId));
      HttpEntity<String> entity = new HttpEntity<>("parameters", httpHeaders);

      String uri =
          UriComponentsBuilder.fromUriString(
                  join(system_id_store_host, "/v1/system_id_store/auth"))
              .queryParam("authId", authId)
              .toUriString();
      ResponseEntity<VoltAuthIdPairDto> responseEntity =
          secureRestTemplate.exchange(uri, HttpMethod.GET, entity, VoltAuthIdPairDto.class);
      if (responseEntity.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
        throw new VoltIdException(String.format("VoltId for authId %s not found", authId));
      }
      return responseEntity.getBody();
    } catch (VoltIdException e) {
      throw e;
    } catch (Exception e) {
      throw new VoltIdException(
          String.format("Could not retrieve voltId for authId: %s", authId), e);
    }
  }

  public VoltGreenIdPairDto getVoltIdByGreenId(String greenId) throws GreenIdException {
    try {
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.add(
          "Authorization", "Bearer " + azureActiveDirectoryService.getAccessToken(resourceId));
      HttpEntity<String> entity = new HttpEntity<>("parameters", httpHeaders);

      String uri =
          UriComponentsBuilder.fromUriString(
                  join(system_id_store_host, "/v1/system_id_store/illion"))
              .queryParam("greenId", greenId)
              .toUriString();
      ResponseEntity<VoltGreenIdPairDto> responseEntity =
          secureRestTemplate.exchange(uri, HttpMethod.GET, entity, VoltGreenIdPairDto.class);
      if (responseEntity.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
        throw new GreenIdException(String.format("VoltId for greenId %s not found", greenId));
      }
      return responseEntity.getBody();
    } catch (GreenIdException e) {
      throw e;
    } catch (Exception e) {
      throw new GreenIdException(
          String.format("Could not retrieve voltId for greenId: %s", greenId), e);
    }
  }

  public Optional<VoltSalesforceIdPairDto> getSalesforceId(String voltId) throws AuthIdException {
    try {
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.add(
          "Authorization", "Bearer " + azureActiveDirectoryService.getAccessToken(resourceId));
      HttpEntity<String> entity = new HttpEntity<>("parameters", httpHeaders);

      String uri =
          UriComponentsBuilder.fromUriString(
                  join(system_id_store_host, "/v1/system_id_store/salesforce"))
              .queryParam("voltId", voltId)
              .toUriString();
      return Optional.of(
          secureRestTemplate
              .exchange(uri, HttpMethod.GET, entity, VoltSalesforceIdPairDto.class)
              .getBody());
    } catch (HttpClientErrorException e) {
      if (HttpStatus.NOT_FOUND.equals(e.getStatusCode())) {
        return Optional.empty();
      }
      throw new SalesforceIdException(
          String.format("Could not retrieve salesforceId for voltId: %s", voltId), e);
    } catch (Exception e) {
      throw new SalesforceIdException(
          String.format("Could not retrieve salesforceId for voltId: %s", voltId), e);
    }
  }

  public VoltAuthIdPairDto createVoltId(String authId) {
    try {
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.add(
          "Authorization", "Bearer " + azureActiveDirectoryService.getAccessToken(resourceId));

      AuthIdDto authIdDto = new AuthIdDto();
      authIdDto.authId(authId);
      HttpEntity<AuthIdDto> entity = new HttpEntity<>(authIdDto, httpHeaders);

      String uri =
          UriComponentsBuilder.fromUriString(
                  join(system_id_store_host, "/v1/system_id_store/auth"))
              .toUriString();
      return secureRestTemplate
          .exchange(uri, HttpMethod.POST, entity, VoltAuthIdPairDto.class)
          .getBody();
    } catch (Exception e) {
      throw new AuthIdException(String.format("Could not create voltId for authId: %s", authId), e);
    }
  }

  public VoltSalesforceIdPairDto createSalesforceId(String voltId, String salesforceId) {
    try {
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.add(
          "Authorization", "Bearer " + azureActiveDirectoryService.getAccessToken(resourceId));

      SalesforceIdDto salesforceIdDto = new SalesforceIdDto();
      salesforceIdDto.setSalesforceId(salesforceId);
      HttpEntity<SalesforceIdDto> entity = new HttpEntity<>(salesforceIdDto, httpHeaders);

      String uri =
          UriComponentsBuilder.fromUriString(
                  join(
                      system_id_store_host,
                      String.format("/v1/system_id_store/salesforce/%s", voltId)))
              .toUriString();
      return secureRestTemplate
          .exchange(uri, HttpMethod.POST, entity, VoltSalesforceIdPairDto.class)
          .getBody();
    } catch (Exception e) {
      throw new SalesforceIdException(
          String.format("Could not create salesforceId: %s for voltId: %s", salesforceId, voltId),
          e);
    }
  }

  public VoltT24IdPairDto createT24Id(String voltId, String t24Id) throws T24IdException {
    try {
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.add(
          "Authorization", "Bearer " + azureActiveDirectoryService.getAccessToken(resourceId));
      T24IdDto t24IdDto = new T24IdDto();
      t24IdDto.setT24Id(t24Id);
      String uri =
          UriComponentsBuilder.fromUriString(
                  join(
                      system_id_store_host,
                      String.format("/v1/system_id_store/t24/%s", voltId)))
              .toUriString();
      return secureRestTemplate
          .exchange(
              uri, HttpMethod.POST, new HttpEntity<>(t24IdDto, httpHeaders), VoltT24IdPairDto.class)
          .getBody();
    } catch (Exception e) {
      throw new T24IdException(
          String.format("Could not create t24Id: %s for voltId: %s", t24Id, voltId), e);
    }
  }

  public VoltGreenIdPairDto createGreenId(String voltId, String greenId) throws GreenIdException {
    try {
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.add(
          "Authorization", "Bearer " + azureActiveDirectoryService.getAccessToken(resourceId));
      GreenIdDto greenIdDto = new GreenIdDto();
      greenIdDto.setGreenId(greenId);
      String uri =
          UriComponentsBuilder.fromUriString(
                  join(
                      system_id_store_host,
                      String.format("/v1/system_id_store/illion/%s", voltId)))
              .toUriString();
      return secureRestTemplate
          .exchange(
              uri,
              HttpMethod.POST,
              new HttpEntity<>(greenIdDto, httpHeaders),
              VoltGreenIdPairDto.class)
          .getBody();
    } catch (Exception e) {
      throw new GreenIdException(
          String.format("Could not create greenId: %s for voltId: %s", greenId, voltId), e);
    }
  }

  public VoltT24IdPairDto getT24CustomerIdForVoltId(String voltId) throws T24IdException {
    try {
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.add(
          "Authorization", "Bearer " + azureActiveDirectoryService.getAccessToken(resourceId));
      HttpEntity<String> entity = new HttpEntity<>("parameters", httpHeaders);
      String uri =
          UriComponentsBuilder.fromUriString(
                  join(
                      system_id_store_host,
                      String.format("/v1/system_id_store/t24")))
              .queryParam("voltId", voltId)
              .toUriString();
      return secureRestTemplate
          .exchange(uri, HttpMethod.GET, entity, VoltT24IdPairDto.class)
          .getBody();
    } catch (Exception e) {
      log.error(String.format("Could not retrieve T24 Id for voltId: %s", voltId), e);
      throw new T24IdException(
          String.format("Could not retrieve T24 Id for voltId: %s", voltId), e);
    }
  }
}
