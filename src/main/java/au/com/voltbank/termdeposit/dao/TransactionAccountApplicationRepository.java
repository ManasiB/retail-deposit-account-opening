package au.com.voltbank.termdeposit.dao;

import au.com.voltbank.termdeposit.entity.TransactionAccountApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/** Spring Data JPA repository for the User entity. */
@Repository
public interface TransactionAccountApplicationRepository
    extends JpaRepository<TransactionAccountApplication, Long> {

  Optional<TransactionAccountApplication> findOneByVoltId(String voltId);
}
