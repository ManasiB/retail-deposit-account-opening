package au.com.voltbank.termdeposit.service;

import au.com.voltbank.account.model.TdAccountApplicationDataDto;
import au.com.voltbank.customer.model.CustomerBankingCreateResponseDto;
import au.com.voltbank.termdeposit.corebanking.service.T24AccountService;
import au.com.voltbank.t24.T24ResponseBody;
import au.com.voltbank.t24.account.CreateTDAccountResponseBodyDto;
import au.com.voltbank.t24.account.TDAccountDetailBodyDto;
import au.com.voltbank.termdeposit.entity.TermDepositAccountApplication;
import au.com.voltbank.termdeposit.exception.TermDepositAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Predicate;

@Service
@Slf4j
public class TermDepositAccountApplicationService {

  @Autowired private T24AccountService coreBankingTDAccountService;

  @Autowired private MappingJackson2HttpMessageConverter converter;

  @Autowired private CustomerService customerService;

  @Autowired private ModelMapper modelMapper;

  public TermDepositAccountApplication create(
      TdAccountApplicationDataDto accountApplicationDto, String voltId) throws VoltCustomException {
    TermDepositAccountApplication accountApplication =
        modelMapper.map(accountApplicationDto, TermDepositAccountApplication.class);

    CustomerBankingCreateResponseDto customer = customerService.createt24CustomerIfnotExist(voltId);
    String t24CustomerId = customer.getT24CustomerId();
    accountApplication.setT24CustomerId(t24CustomerId);

    ResponseEntity<T24ResponseBody> response =
        coreBankingTDAccountService.createTermDepositAccount(accountApplication);
    CreateTDAccountResponseBodyDto responseDto =
        converter
            .getObjectMapper()
            .convertValue(response.getBody().getBody(), CreateTDAccountResponseBodyDto.class);
    String arrangementId = responseDto.getArrangementActivity().getArrangementId();
    accountApplication.setArrangementId(arrangementId);

    return accountApplication;
  }

  public TDAccountDetailBodyDto getTermDepositAccountDetails(
      TermDepositAccountApplication termDepositAccountApplication)
      throws TermDepositAccountNotFoundException, VoltCustomException {
    ResponseEntity<T24ResponseBody> responseEntity =
        coreBankingTDAccountService.getTermDepositAccounts(
            termDepositAccountApplication.getT24CustomerId());

    List<TDAccountDetailBodyDto> accountDetailList =
        converter
            .getObjectMapper()
            .convertValue(
                responseEntity.getBody().getBody(),
                new TypeReference<List<TDAccountDetailBodyDto>>() {});
    return accountDetailList.stream()
        .filter(filterAccounts(termDepositAccountApplication.getArrangementId()))
        .findFirst()
        .orElseThrow(
            () ->
                new TermDepositAccountNotFoundException(
                    String.format(
                        "Term Deposit Account not found for arrangementId: %s on customer with t24CustomerId: %s",
                        termDepositAccountApplication.getArrangementId(),
                        termDepositAccountApplication.getT24CustomerId())));
  }

  private Predicate<TDAccountDetailBodyDto> filterAccounts(String arrangementId) {
    return tdAccountDetail -> arrangementId.equals(tdAccountDetail.getReference());
  }
}
