package au.com.voltbank.termdeposit.exception;

public class DuplicateVoltIdException extends Exception {

  public DuplicateVoltIdException(String voltId) {
    super(String.format("A customer already exists with the voltId: %s", voltId));
  }
}
