package au.com.voltbank.termdeposit.exception;

public class SavingsAccountNotFoundException extends Exception {

  public SavingsAccountNotFoundException(String message) {
    super(message);
  }
}
