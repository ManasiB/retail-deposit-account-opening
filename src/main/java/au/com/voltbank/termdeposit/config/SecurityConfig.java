package au.com.voltbank.termdeposit.config;

import au.com.voltbank.termdeposit.security.Auth0AuthenticationProvider;
import au.com.voltbank.termdeposit.security.AzureAuthenticationProvider;
import com.auth0.spring.security.api.BearerSecurityContextRepository;
import com.auth0.spring.security.api.JwtAuthenticationEntryPoint;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.security.PublicKey;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@EnableWebSecurity
@Configuration
@Slf4j
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private static final int CACHE_MAX_SIZE = 5;
  private static final int CACHE_EXPIRATION_HOURS = 24;

  @Value("${auth0.user.api.issuer}")
  private String auth0UserIssuer;

  @Value("${auth0.user.api.audience}")
  private String auth0UserAudience;

  @Autowired private AzureAuthenticationProvider azureAuthenticationProvider;

  @Autowired private Auth0AuthenticationProvider auth0AuthenticationProvider;

  @Bean
  CorsConfigurationSource corsConfigurationSource() {
    CorsConfiguration configuration = new CorsConfiguration();
    configuration.setAllowedMethods(Arrays.asList("GET", "POST"));
    configuration.addAllowedHeader("Authorization");
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/**", configuration);
    return source;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {

    http.authenticationProvider(
            auth0AuthenticationProvider.initProvider(auth0UserIssuer, auth0UserAudience))
        .authenticationProvider(azureAuthenticationProvider)
        .securityContext()
        .securityContextRepository(new BearerSecurityContextRepository())
        .and()
        .exceptionHandling()
        .authenticationEntryPoint(new JwtAuthenticationEntryPoint())
        .and()
        .httpBasic()
        .disable()
        .csrf()
        .disable()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .authorizeRequests()
        .antMatchers(HttpMethod.GET, "/actuator/health").permitAll()
        .antMatchers(HttpMethod.PUT, "/**")
        .authenticated()
        .antMatchers(HttpMethod.GET, "/**")
        .authenticated()
        .antMatchers(HttpMethod.POST, "/**")
        .authenticated();
  }

  @Bean
  public Cache<String, PublicKey> publicKeyCache() {
    return CacheBuilder.newBuilder()
        .maximumSize(CACHE_MAX_SIZE)
        .expireAfterWrite(CACHE_EXPIRATION_HOURS, TimeUnit.HOURS)
        .build();
  }

  @Bean
  public JwtParser newJwtParser() {
    return Jwts.parser();
  }
}
