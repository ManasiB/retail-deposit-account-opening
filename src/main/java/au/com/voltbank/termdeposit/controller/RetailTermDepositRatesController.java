package au.com.voltbank.termdeposit.controller;

import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.service.RetailTermDepositRatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RetailTermDepositRatesController {

  @Autowired private RetailTermDepositRatesService ratesService;

  @GetMapping(value = "/term_deposit/rates")
  public ResponseEntity returnRates() throws VoltCustomException {
    return ResponseEntity.status(HttpStatus.OK).body(ratesService.fetchLatestRates());
  }
}
