package au.com.voltbank.termdeposit.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class AzureTokenRequestParam {

  private String grantType;

  private String clientId;

  private String clientSecret;
}
