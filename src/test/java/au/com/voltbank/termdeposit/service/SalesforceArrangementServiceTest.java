package au.com.voltbank.termdeposit.service;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import au.com.voltbank.RetailDepositAccountOpeningApplication;
import au.com.voltbank.salesforce.arrangement.model.ArrangementPostDto;
import au.com.voltbank.salesforce.arrangement.model.SavingsArrangementDataDto;
import au.com.voltbank.salesforce.arrangement.model.SuccessResponseDto;
import au.com.voltbank.sis.model.VoltSalesforceIdPairDto;
import au.com.voltbank.t24.account.AccountDetailBodyDto;
import au.com.voltbank.t24.account.TDAccountDetailBodyDto;
import au.com.voltbank.termdeposit.entity.SavingsAccountApplication;
import au.com.voltbank.termdeposit.entity.TermDepositAccountApplication;
import au.com.voltbank.termdeposit.exception.SalesforceArragementCreationException;
import au.com.voltbank.termdeposit.util.CannedData;
import com.tyro.oss.randomdata.RandomString;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailDepositAccountOpeningApplication.class)
public class SalesforceArrangementServiceTest {

  final String SF_ARRANGEMENT_ENDPOINT = "/arrangement";
  final String SF_ARRANGEMENT_CREATION_ERROR = "Could not create Salesforce arrangement for VoltId: %s";
  final String SF_ID_NOTFOUND = "Could not find SalesforceId for VoltId: %s";

  @Autowired private SalesforceArrangementService salesforceArrangementService;

  @MockBean(name = "secureRestTemplate")
  private RestTemplate secureRestTemplate;

  @MockBean private SystemIdStoreService systemIdStoreService;

  @MockBean private AzureActiveDirectoryService azureActiveDirectoryService;

  @MockBean private TermDepositAccountApplicationService termDepositAccountApplicationService;

  @MockBean private SavingsAccountService savingsAccountService;

  @Before
  public void setUp() {
    reset(secureRestTemplate, azureActiveDirectoryService);
  }

  @Test
  public void shouldCallSalesforceToCreateArrangementAndGetSuccessfulResponse() throws Exception {

    // setup
    TermDepositAccountApplication expectedTermDepositAccount =
        CannedData.getDepositAccountApplication().toBuilder().build();

    ArgumentCaptor<String> uriCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);

    String expectedSalesforceId = RandomString.randomUUID();
    VoltSalesforceIdPairDto expectedSisResponse = new VoltSalesforceIdPairDto();
    expectedSisResponse.setSalesforceId(expectedSalesforceId);
    when(systemIdStoreService.getSalesforceId(expectedTermDepositAccount.getUserId()))
        .thenReturn(Optional.of(expectedSisResponse));

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    TDAccountDetailBodyDto expectedtdAccountDetailsDto = CannedData.getTDAccountResponseBody();
    when(termDepositAccountApplicationService.getTermDepositAccountDetails(
            expectedTermDepositAccount))
        .thenReturn(expectedtdAccountDetailsDto);

    SuccessResponseDto successResponseDto = new SuccessResponseDto();
    successResponseDto.setSuccess("true");
    successResponseDto.setId(expectedSalesforceId);

    when(secureRestTemplate.postForEntity(
            uriCaptor.capture(), httpEntityCaptor.capture(), eq(SuccessResponseDto.class)))
        .thenReturn(new ResponseEntity<>(successResponseDto, HttpStatus.OK));

    // test
    SuccessResponseDto responseDto =
        salesforceArrangementService.create(expectedTermDepositAccount);

    // verify
    assertThat(
        httpEntityCaptor.getValue().getHeaders().get("Authorization").get(0),
        is("Bearer " + accessToken));
    assertThat(responseDto.getId(), is(expectedSalesforceId));

    assertThat(uriCaptor.getValue(), containsString(SF_ARRANGEMENT_ENDPOINT));

    ArrangementPostDto arrangementPostDto =
        (ArrangementPostDto) httpEntityCaptor.getValue().getBody();
    assertThat(arrangementPostDto.getCustomerID(), is(expectedSalesforceId));

    assertThat(
        arrangementPostDto.getLinkedAccountName(),
        is(expectedTermDepositAccount.getLinkedAccountName()));
    assertThat(
        arrangementPostDto.getLinkedAccountBSB(),
        is(expectedTermDepositAccount.getLinkedAccountBSB()));
    assertThat(
        arrangementPostDto.getLinkedAccountNumber(),
        is(expectedTermDepositAccount.getLinkedAccountNumber()));

    assertThat(arrangementPostDto.getAccountNumber(), is(expectedtdAccountDetailsDto.getAccount()));
    assertThat(arrangementPostDto.getBsbNumber(), is(expectedtdAccountDetailsDto.getSortcode()));
    assertThat(
        arrangementPostDto.getPrincipalBalanceInWholeDollars(),
        is(expectedtdAccountDetailsDto.getPrincipalBalance()));
    assertThat(
        arrangementPostDto.getMaturityDate(), is(expectedtdAccountDetailsDto.getMaturityDate()));
    assertThat(arrangementPostDto.getOpenDate(), is(expectedtdAccountDetailsDto.getStartDate()));
    assertThat(
        arrangementPostDto.getInterestRate().toString(),
        is(expectedtdAccountDetailsDto.getInterestRate()));
  }

  @Test
  public void shouldThrowExceptionWhenCannotCreateSalesforceCustomer() throws Exception {

    // setup
    TermDepositAccountApplication expectedTermDepositAccount =
        CannedData.getDepositAccountApplication().toBuilder().build();

    ArgumentCaptor<String> uriCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);

    String expectedSalesforceId = RandomString.randomUUID();
    VoltSalesforceIdPairDto expectedSisResponse = new VoltSalesforceIdPairDto();
    expectedSisResponse.setSalesforceId(expectedSalesforceId);

    when(systemIdStoreService.getSalesforceId(expectedTermDepositAccount.getUserId()))
        .thenReturn(Optional.of(expectedSisResponse));

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    TDAccountDetailBodyDto expectedtdAccountDetailsDto = CannedData.getTDAccountResponseBody();
    when(termDepositAccountApplicationService.getTermDepositAccountDetails(
            expectedTermDepositAccount))
        .thenReturn(expectedtdAccountDetailsDto);

    when(secureRestTemplate.postForEntity(
            uriCaptor.capture(), httpEntityCaptor.capture(), eq(SuccessResponseDto.class)))
        .thenThrow(new RuntimeException("BOOM!"));

    // test
    SalesforceArragementCreationException expectedException = null;
    try {
      salesforceArrangementService.create(expectedTermDepositAccount);
    } catch (Exception e) {
      expectedException = (SalesforceArragementCreationException) e;
    }

    assertThat(uriCaptor.getValue(), containsString(SF_ARRANGEMENT_ENDPOINT));
    assertThat(expectedException.getMessage(), is(String
        .format(SF_ARRANGEMENT_CREATION_ERROR, expectedTermDepositAccount.getUserId())));
    assertThat(expectedException.getCause(), instanceOf(RuntimeException.class));
  }

  @Test
  public void shouldThrowExceptionWhenSalesforceIdDoesNotExist() {

    // setup
    TermDepositAccountApplication expectedTermDepositAccount =
        CannedData.getDepositAccountApplication().toBuilder().build();

    String expectedSalesforceId = RandomString.randomUUID();
    VoltSalesforceIdPairDto expectedSisResponse = new VoltSalesforceIdPairDto();
    expectedSisResponse.setSalesforceId(expectedSalesforceId);

    when(systemIdStoreService.getSalesforceId(expectedTermDepositAccount.getUserId()))
        .thenReturn(Optional.empty());

    // test
    SalesforceArragementCreationException expectedException = null;
    try {
      salesforceArrangementService.create(expectedTermDepositAccount);
    } catch (Exception e) {
      expectedException = (SalesforceArragementCreationException) e;
    }

    // verify
    assertThat(expectedException.getMessage(), is(String
        .format(SF_ARRANGEMENT_CREATION_ERROR, expectedTermDepositAccount.getUserId())));
    Throwable cause = expectedException.getCause();
    assertThat(cause, instanceOf(SalesforceArragementCreationException.class));
    assertThat(cause.getMessage(), is(String
        .format(SF_ID_NOTFOUND, expectedTermDepositAccount.getUserId())));
  }

  @Test
  public void shouldCallSalesforceToCreateArrangementAndGetSuccessfulResponseForFinancialAccount() throws Exception {

    SavingsAccountApplication savingsAccountApplication = CannedData.getSavingsAccountApplication().toBuilder().build();

    ArgumentCaptor<String> uriCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);

    String expectedSalesforceId = RandomString.randomUUID();
    VoltSalesforceIdPairDto expectedSisResponse = new VoltSalesforceIdPairDto();
    expectedSisResponse.setSalesforceId(expectedSalesforceId);
    when(systemIdStoreService.getSalesforceId(any()))
        .thenReturn(Optional.of(expectedSisResponse));

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    List<AccountDetailBodyDto> expectedAccountDetailsDto = CannedData.getAccountDetailsBodyDto();
    when(savingsAccountService.getSavingsAccountDetails(
        any()))
        .thenReturn(expectedAccountDetailsDto.get(0));

    SuccessResponseDto successResponseDto = new SuccessResponseDto();
    successResponseDto.setSuccess("true");
    successResponseDto.setId(expectedSalesforceId);

    when(secureRestTemplate.postForEntity(
        uriCaptor.capture(), httpEntityCaptor.capture(), eq(SuccessResponseDto.class)))
        .thenReturn(new ResponseEntity<>(successResponseDto, HttpStatus.OK));

    // test
    SuccessResponseDto responseDto =
        salesforceArrangementService.createSavingsAccount(savingsAccountApplication);

    // verify
    assertThat(
        httpEntityCaptor.getValue().getHeaders().get("Authorization").get(0),
        is("Bearer " + accessToken));
    assertThat(responseDto.getId(), is(expectedSalesforceId));

    assertThat(uriCaptor.getValue(), containsString(SF_ARRANGEMENT_ENDPOINT));

    SavingsArrangementDataDto arrangementDataDto =
        (SavingsArrangementDataDto) httpEntityCaptor.getValue().getBody();

    assertThat(
        arrangementDataDto.getBankAccountNumberC(),
        is(expectedAccountDetailsDto.get(0).getAcctNumber()));

    assertThat(
        arrangementDataDto.getFinServInterestRateC().toString(),
        is(expectedAccountDetailsDto.get(0).getCreditInterestRate()));

    assertThat(
        arrangementDataDto.getAccountTypeC(),
        is(expectedAccountDetailsDto.get(0).getAccountType()));

    assertThat(
        arrangementDataDto.getFinServInterestRateC().toString(),
        is(expectedAccountDetailsDto.get(0).getCreditInterestRate()));
  }


  @Test
  public void shouldThrowExceptionWhenCannotCreateSalesforceFinancialAccount() throws Exception {

    // setup
    SavingsAccountApplication savingsAccountApplication =
        CannedData.getSavingsAccountApplication().toBuilder().build();

    ArgumentCaptor<String> uriCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);

    String expectedSalesforceId = RandomString.randomUUID();
    VoltSalesforceIdPairDto expectedSisResponse = new VoltSalesforceIdPairDto();
    expectedSisResponse.setSalesforceId(expectedSalesforceId);

    when(systemIdStoreService.getSalesforceId(any()))
        .thenReturn(Optional.of(expectedSisResponse));

    String accessToken = RandomString.randomUUID();
    when(azureActiveDirectoryService.getAccessToken(anyString())).thenReturn(accessToken);

    List<AccountDetailBodyDto> expectedtdAccountDetailsDto = CannedData.getAccountDetailsBodyDto();
    when(savingsAccountService.getSavingsAccountDetails(
        any()))
        .thenReturn(expectedtdAccountDetailsDto.get(0));

    when(secureRestTemplate.postForEntity(
        uriCaptor.capture(), httpEntityCaptor.capture(), eq(SuccessResponseDto.class)))
        .thenThrow(new RuntimeException("BOOM!"));

    // test
    SalesforceArragementCreationException expectedException = null;
    try {
      salesforceArrangementService.createSavingsAccount(savingsAccountApplication);
    } catch (Exception e) {
      expectedException = (SalesforceArragementCreationException) e;
    }
    assertThat(expectedException.getMessage(), is(String
        .format(SF_ARRANGEMENT_CREATION_ERROR, savingsAccountApplication.getVoltId())));
    assertThat(expectedException.getCause(), instanceOf(RuntimeException.class));
  }

  @Test
  public void shouldThrowExceptionWhenSalesforceIdDoesNotExistWhileCreatingFinancialAccount() {

    // setup
    SavingsAccountApplication savingsAccountApplication =
        CannedData.getSavingsAccountApplication().toBuilder().build();

    String expectedSalesforceId = RandomString.randomUUID();
    VoltSalesforceIdPairDto expectedSisResponse = new VoltSalesforceIdPairDto();
    expectedSisResponse.setSalesforceId(expectedSalesforceId);

    when(systemIdStoreService.getSalesforceId(any()))
        .thenReturn(Optional.empty());

    // test
    SalesforceArragementCreationException expectedException = null;
    try {
      salesforceArrangementService.createSavingsAccount(savingsAccountApplication);
    } catch (Exception e) {
      expectedException = (SalesforceArragementCreationException) e;
    }

    assertThat(expectedException.getMessage(), is(String
        .format(SF_ARRANGEMENT_CREATION_ERROR, savingsAccountApplication.getVoltId())));
    Throwable cause = expectedException.getCause();
    assertThat(cause, instanceOf(SalesforceArragementCreationException.class));
    assertThat(cause.getMessage(), is(String
        .format(SF_ID_NOTFOUND, savingsAccountApplication.getVoltId())));
  }
}
