package au.com.voltbank.termdeposit.integration;

import au.com.voltbank.RetailDepositAccountOpeningApplication;
import au.com.voltbank.termdeposit.service.Auth0Service;
import au.com.voltbank.termdeposit.service.AzureActiveDirectoryService;
import com.auth0.exception.Auth0Exception;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@Ignore("Skip integration test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailDepositAccountOpeningApplication.class)
public class OAuthIntegrationTest {

  @Autowired private AzureActiveDirectoryService azureActiveDirectoryService;

  @Autowired private Auth0Service auth0Service;

  @Value("${t24.api.resource.id}")
  private String resourceId;

  @Test
  public void shouldReturnTokenFromAzure() {
    String accessToken = azureActiveDirectoryService.getAccessToken(resourceId);
    System.out.println(accessToken);
    Assert.assertNotNull(accessToken);
  }

  @Test
  public void shouldReturnAuth0Token() throws Auth0Exception {
    String accessToken = auth0Service.getAccessToken();
    System.out.println(accessToken);
    Assert.assertNotNull(accessToken);
  }
}
