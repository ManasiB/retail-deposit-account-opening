package au.com.voltbank.termdeposit.message.transformer;

import au.com.voltbank.termdeposit.dto.NewDepositArrangementDto;
import au.com.voltbank.termdeposit.exception.CloudQueueMessageTransformationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.queue.CloudQueueMessage;
import com.tyro.oss.randomdata.RandomString;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertThat;

public class NewDepositArrangementDtoTransformerTest {

  @Test
  public void shouldTransformNewArrangementDtoToCloudQueueMessage()
      throws StorageException, JsonProcessingException {
    // setup
    ObjectMapper objectMapper = new ObjectMapper();
    NewArrangementDtoTransformer optimusPrime = new NewArrangementDtoTransformer(objectMapper);

    NewDepositArrangementDto expectedNewDepositArrangementDto =
        new NewDepositArrangementDto(
            RandomString.randomNumericString(), RandomString.randomNumericString());

    // test
    CloudQueueMessage cloudQueueMessage = optimusPrime.transform(expectedNewDepositArrangementDto);

    // verify
    assertThat(
        cloudQueueMessage.getMessageContentAsString(),
        CoreMatchers.is(objectMapper.writeValueAsString(expectedNewDepositArrangementDto)));
  }

  @Test
  public void
      shouldCatchJsonProcessingExceptionAndRethrowAsCloudQueueMessageTransformationException()
          throws StorageException, JsonProcessingException {
    // setup
    ObjectMapper objectMapper = Mockito.mock(ObjectMapper.class);
    NewArrangementDtoTransformer optimusPrime = new NewArrangementDtoTransformer(objectMapper);

    NewDepositArrangementDto expectedNewDepositArrangementDto =
        new NewDepositArrangementDto(
            RandomString.randomNumericString(), RandomString.randomNumericString());

    Mockito.when(objectMapper.writeValueAsString(expectedNewDepositArrangementDto))
        .thenThrow(JsonProcessingException.class);

    // test
    CloudQueueMessageTransformationException expectedException = null;
    try {
      optimusPrime.transform(expectedNewDepositArrangementDto);
    } catch (Exception e) {
      expectedException = (CloudQueueMessageTransformationException) e;
    }

    // verify
    assertThat(
        expectedException.getMessage(),
        CoreMatchers.is(
            String.format(
                "Failed to serialize NewDepositArrangementDto to CloudQueueMessage for arrangementId %s and voltId %s.",
                expectedNewDepositArrangementDto.getArrangementId(),
                expectedNewDepositArrangementDto.getVoltId())));
  }
}
