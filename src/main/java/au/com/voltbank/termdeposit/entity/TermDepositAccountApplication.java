package au.com.voltbank.termdeposit.entity;

import au.com.voltbank.converter.DateAttributeConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Builder(builderClassName = "TermDepositAccountApplicationBuilder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Data
@Entity
@Table
public class TermDepositAccountApplication {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Size(max = 50)
  @Column(length = 64)
  private String accountName;

  @Size(max = 50)
  @Column(length = 32)
  private String accountNumber;

  @Column(name = "amountInWholeDollar")
  private int amountInWholeDollars;

  private String userId;

  private String t24CustomerId;

  private String arrangementId;

  private String termInMonths;

  private String indexCode;

  private Double rates;

  private String status;

  private String actionOnMaturity;

  @Convert(converter = DateAttributeConverter.class)
  private String startDate;

  private String linkedAccountName;

  private String linkedAccountBSB;

  private String linkedAccountNumber;
  
  private boolean fundingStatus;
}
