package au.com.voltbank.termdeposit.controller;

import au.com.voltbank.account.model.PagingInfoDto;
import au.com.voltbank.account.model.TransactionInfoArrayDto;
import au.com.voltbank.account.model.TransactionListDto;
import au.com.voltbank.sis.model.VoltAuthIdPairDto;
import au.com.voltbank.t24.util.T24ClaimDecryptor;
import au.com.voltbank.termdeposit.corebanking.service.T24AccountService;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.service.TransactionListService;
import au.com.voltbank.termdeposit.util.CannedData;
import com.tyro.oss.randomdata.RandomString;
import org.junit.After;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.security.Principal;

import static au.com.voltbank.Constants.ERROR_TRANSACTION_ACCOUNT_NOT_FOUND;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.integration.json.SimpleJsonSerializer.toJson;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = TransactionsListController.class, secure = false)
@Ignore
public class TransactionsListControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private T24ClaimDecryptor t24ClaimDecryptor;

	@MockBean
	private TransactionListService transactionListService;

	@MockBean
	private T24AccountService t24AccountService;

	@After
	public void reset() {
		Mockito.reset(transactionListService);
	}

	@Test
	public void shouldReturn_200_IfNoErrorsOccur_ForGetTransactions() throws Exception {
		VoltAuthIdPairDto voltAuthIdPairDto = CannedData.getVoltAuthIdPairDto();
		Principal principal = voltAuthIdPairDto::getAuthId;
		Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);

		TransactionListDto transactionListDto = new TransactionListDto();
		transactionListDto.setPagingInfo(new PagingInfoDto());
		transactionListDto.setData(new TransactionInfoArrayDto());

		when(transactionListService.getTransactions(any(), any(), any()))
				.thenReturn(transactionListDto);

		String response =
				this.mockMvc
						.perform(
								get(TransactionsListController.PATH_ACCOUNTS_ACCOUNT_NUMBER_TRANSACTIONS, "1234")
										.principal(jwt)
										.contentType(MediaType.APPLICATION_JSON)
										.content(toJson(transactionListDto)))
						.andExpect(status().isOk())
						.andReturn()
						.getResponse()
						.getContentAsString();
		TransactionListDto transactionListDtoResponse =
				CannedData.fromJson(response, TransactionListDto.class);
		Assert.assertNotNull(transactionListDtoResponse);
		Assert.assertNotNull(transactionListDtoResponse.getPagingInfo());
		Assert.assertNotNull(transactionListDtoResponse.getData());
	}

	@Test
	public void shouldReturn_200_WIthEmptyResponse_ForGetTransactions() throws Exception {
		VoltAuthIdPairDto voltAuthIdPairDto = CannedData.getVoltAuthIdPairDto();
		Principal principal = voltAuthIdPairDto::getAuthId;
		Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);

		this.mockMvc
				.perform(
						get(TransactionsListController.PATH_ACCOUNTS_ACCOUNT_NUMBER_TRANSACTIONS, "1234")
								.principal(jwt)
								.accept(APPLICATION_JSON)
								.contentType(APPLICATION_JSON)
								.content(""))
				.andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	public void shouldReturn_404_If_accountNumber_does_not_exist() throws Exception {
		String authId = RandomString.randomNumericString();

		when(transactionListService.getTransactions(any(), any(), any()))
				.thenThrow(
						new VoltCustomException(ERROR_TRANSACTION_ACCOUNT_NOT_FOUND, "dummy exception", 404));
		TransactionListDto transactionListDto = new TransactionListDto();
		Principal principal = () -> authId;
		Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);
		this.mockMvc
				.perform(
						get(TransactionsListController.PATH_ACCOUNTS_ACCOUNT_NUMBER_TRANSACTIONS, "1234")
								.principal(jwt)
								.contentType(APPLICATION_JSON)
								.content(toJson(transactionListDto))
								.accept(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isNotFound());
	}
}
