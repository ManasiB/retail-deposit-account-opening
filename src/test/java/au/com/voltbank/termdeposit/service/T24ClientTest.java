package au.com.voltbank.termdeposit.service;

import au.com.voltbank.RetailDepositAccountOpeningApplication;
import au.com.voltbank.termdeposit.corebanking.T24Client;
import au.com.voltbank.t24.T24RequestBody;
import au.com.voltbank.t24.T24ResponseBody;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.util.CannedData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailDepositAccountOpeningApplication.class)
public class T24ClientTest {

  @MockBean(name = "secureRestTemplate")
  private RestTemplate mockedSecureRestTemplate;

  private String mockUrl;

  private HttpEntity<T24RequestBody> mockPayload;

  @Autowired private T24Client t24Client;

  @Before
  public void setUp() {
    mockUrl = "a_mocked_url";
    mockPayload = new HttpEntity<>(null, CannedData.getAzureTokenRequestHeader());
    reset(mockedSecureRestTemplate);
  }

  @Test(expected = VoltCustomException.class)
  public void should_handle_t24error_post() throws VoltCustomException {
    when(mockedSecureRestTemplate.postForEntity(anyString(), any(), any()))
        .thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST));
    t24Client.post(mockUrl, mockPayload);
  }

  @Test(expected = VoltCustomException.class)
  public void should_handle_t24error_get() throws VoltCustomException {
    when(mockedSecureRestTemplate.exchange(
            "a_mocked_url/t24customerId", HttpMethod.GET, mockPayload, T24ResponseBody.class))
        .thenThrow(new HttpClientErrorException(HttpStatus.UNAUTHORIZED));
    t24Client.get(mockUrl, "t24customerId", CannedData.getAzureTokenRequestHeader());
  }

  @Test(expected = VoltCustomException.class)
  public void should_handle_error_put() throws VoltCustomException {
    when(mockedSecureRestTemplate.exchange(
            "a_mocked_url/t24customerId", HttpMethod.PUT, mockPayload, T24ResponseBody.class))
        .thenThrow(new NullPointerException("Boom"));
    t24Client.put(mockUrl, mockPayload, "t24customerId");
  }
}
