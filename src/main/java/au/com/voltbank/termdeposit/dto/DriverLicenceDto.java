package au.com.voltbank.termdeposit.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(builderClassName = "DriverLicenceDtoBuilder")
@JsonDeserialize(builder = DriverLicenceDto.DriverLicenceDtoBuilder.class)
@NoArgsConstructor
@AllArgsConstructor
public class DriverLicenceDto {

  private String licenceNumber;
  private String state;
  private String expiry;

  @JsonPOJOBuilder(withPrefix = "")
  public static class DriverLicenceDtoBuilder {
    // Lombok will add constructor, setters, build method
  }
}
