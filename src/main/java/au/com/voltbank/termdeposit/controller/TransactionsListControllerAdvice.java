package au.com.voltbank.termdeposit.controller;

import au.com.voltbank.termdeposit.dto.ErrorResponse;
import au.com.voltbank.termdeposit.exception.TransactionAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import static au.com.voltbank.Constants.ERROR_SYSTEM;
import static au.com.voltbank.Constants.ERROR_TRANSACTION_ACCOUNT_NOT_FOUND;
import static au.com.voltbank.Constants.ERROR_VOLT_ID_NOT_FOUND;

@Slf4j
@ControllerAdvice(assignableTypes = TransactionsListController.class)
public class TransactionsListControllerAdvice {

  @ExceptionHandler
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ErrorResponse handle(TransactionAccountNotFoundException e) {
    log.error(ERROR_TRANSACTION_ACCOUNT_NOT_FOUND, e);
    return new ErrorResponse(ERROR_TRANSACTION_ACCOUNT_NOT_FOUND);
  }

  @ExceptionHandler
  public ResponseEntity<ErrorResponse> handle(VoltCustomException e) {
    log.error(e.getMessage(), e);
    return new ResponseEntity<ErrorResponse>(
        new ErrorResponse(ERROR_VOLT_ID_NOT_FOUND), HttpStatus.valueOf(e.getHttpErrorCode()));
  }

  @ExceptionHandler
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ErrorResponse handle(Exception exception) {
    log.error(ERROR_SYSTEM, exception);
    return new ErrorResponse(ERROR_SYSTEM);
  }
}
