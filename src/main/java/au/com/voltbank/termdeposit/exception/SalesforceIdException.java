package au.com.voltbank.termdeposit.exception;

public class SalesforceIdException extends RuntimeException {

  public SalesforceIdException(String message) {
    super(message);
  }

  public SalesforceIdException(String message, Throwable cause) {
    super(message, cause);
  }
}
