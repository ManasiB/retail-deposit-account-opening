package au.com.voltbank.termdeposit.service;

import au.com.voltbank.RetailDepositAccountOpeningApplication;
import au.com.voltbank.account.model.PagingInfoDto;
import au.com.voltbank.account.model.TransactionInfoArrayDto;
import au.com.voltbank.account.model.TransactionInfoDto;
import au.com.voltbank.account.model.TransactionListDto;
import au.com.voltbank.sis.model.VoltAuthIdPairDto;
import au.com.voltbank.sis.model.VoltT24IdPairDto;
import au.com.voltbank.t24.T24ResponseBody;
import au.com.voltbank.termdeposit.corebanking.service.T24AccountService;
import au.com.voltbank.termdeposit.exception.T24IdException;
import au.com.voltbank.termdeposit.exception.TransactionAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.util.CannedData;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.MultiValueMap;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailDepositAccountOpeningApplication.class)
public class TransactionListServiceTest {

    @Autowired private TransactionListService transactionListService;

    @MockBean
    private T24AccountService t24AccountService;

    @MockBean
    private SystemIdStoreService systemIdStoreService;

    @Test
    public void shouldGetListOfTransactionsForAnAccount() throws Exception {
        T24ResponseBody mockTransactionListDtoValidData =
                CannedData.getMockTransactionListDtoWIthValidData();

        VoltAuthIdPairDto voltAuthIdPairDto = CannedData.getVoltAuthIdPairDto();
        when(systemIdStoreService.getVoltId(anyString())).thenReturn(voltAuthIdPairDto);

        VoltT24IdPairDto voltT24IdPairDto = CannedData.getVoltT24IdPairDto();
        when(systemIdStoreService.getT24CustomerIdForVoltId(anyString())).thenReturn(voltT24IdPairDto);

        when(t24AccountService.getTransactionsListForAnAccount(
                any(), any()))
                .thenReturn(new ResponseEntity<>(mockTransactionListDtoValidData, HttpStatus.OK));

        when(t24AccountService.getAccounts(voltT24IdPairDto.getT24Id())).thenReturn(
                new ResponseEntity<>(
                        CannedData.getMockTransactionAccountListDtoWithValidData(), HttpStatus.OK));


        Map<String,String> testMap = new HashMap<>();
        testMap.put("hello","hello");
        testMap.put("fromDate","2019-03-01");
        testMap.put("toDate","2019-04-30");
        testMap.put("minAmount","356.4979239");
        testMap.put("maxAmount","3,37657.87");
        testMap.put("pageSize","23");
        testMap.put("pageStart","15");

        TransactionListDto transactions = transactionListService
                .getTransactions(voltT24IdPairDto.getT24Id(), CannedData.MOCKED_ACCT_NUMBER_SAVER, testMap);

        TransactionInfoDto transactionInfo = transactions.getData().get(0);
        Assert.assertTrue(transactionInfo.getBalance().equals("6507.99"));
        Assert.assertTrue(transactionInfo.getBankAccountNumber().equals("100000341"));
        Assert.assertTrue(transactionInfo.getDescription().equals("Interest - Capitalise"));
        Assert.assertTrue(transactionInfo.getNarrative().equals("Credit Interest"));
        Assert.assertTrue(transactionInfo.getTransactionAmount().equals("5.35"));
        Assert.assertTrue(transactionInfo.getDebitCreditIndicator().toString().equals("Credit"));
        Assert.assertTrue(transactionInfo.getTransactionDate().equals("2019-05-02"));
        Assert.assertTrue(transactionInfo.getTransactionType().equals(
                TransactionInfoDto.TransactionTypeEnum._300));

        Assert.assertTrue(transactions.getData().size()==2);
        TransactionInfoDto transactionInfoDto1 = transactions.getData().get(1);
        Assert.assertTrue(transactionInfoDto1.getBalance().equals("6502.64"));
        Assert.assertTrue(transactionInfoDto1.getBankAccountNumber().equals("100000341"));
        Assert.assertTrue(transactionInfoDto1.getDescription().equals("Debit Arrangement"));
        Assert.assertTrue(transactionInfoDto1.getNarrative().equals("Withholding Tax - Resident"));
        Assert.assertTrue(transactionInfoDto1.getTransactionAmount().equals("2.51"));
        Assert.assertTrue(transactionInfoDto1.getDebitCreditIndicator().toString().equals("Debit"));
        Assert.assertTrue(transactionInfoDto1.getTransactionDate().equals("2019-05-01"));
        Assert.assertTrue(transactionInfoDto1.getTransactionType().equals(
                TransactionInfoDto.TransactionTypeEnum._600));

        PagingInfoDto pagingInfo = transactions.getPagingInfo();
        Assert.assertTrue(pagingInfo.getPageSize().equals("50"));
        Assert.assertTrue(pagingInfo.getPageStart().equals("0"));
        Assert.assertTrue(pagingInfo.getTotalSize().equals("2"));
        Assert.assertTrue(pagingInfo.getPageToken().equals("d750782a-e998-47d8-ad6a-368153a45ab3"));

        ArgumentCaptor<MultiValueMap<String,String>> captor =
                ArgumentCaptor.forClass(MultiValueMap.class);
        verify(t24AccountService).getTransactionsListForAnAccount(
                contains(CannedData.MOCKED_ACCT_NUMBER_SAVER), captor.capture());

        MultiValueMap<String, String> value = captor.getValue();
        Assert.assertTrue(value.size()==7);
        Assert.assertTrue(value.get("fromDate").get(0).equals("20190301"));
        Assert.assertTrue(value.get("toDate").get(0).equals("20190430"));
        Assert.assertTrue(value.get("minimumTxnAmount").get(0).equals("356.4979239"));
        Assert.assertTrue(value.get("maximumTxnAmount").get(0).equals("3,37657.87"));
        Assert.assertTrue(value.get("page_start").get(0).equals("15"));
        Assert.assertTrue(value.get("page_size").get(0).equals("23"));
        Assert.assertTrue(value.get("hello").get(0).equals("hello"));
    }

    @Test
    public void shouldGetListOfTransactionsForAnAccounWithTransactionsSortedInDescendingOrder()
            throws Exception {
        T24ResponseBody mockTransactionListDtoWithValidData =
                CannedData.getMockTransactionListDtoWithUnorderedTransactionDate();

        VoltAuthIdPairDto voltAuthIdPairDto = CannedData.getVoltAuthIdPairDto();
        when(systemIdStoreService.getVoltId(anyString())).thenReturn(voltAuthIdPairDto);

        VoltT24IdPairDto voltT24IdPairDto = CannedData.getVoltT24IdPairDto();
        when(systemIdStoreService.getT24CustomerIdForVoltId(anyString())).thenReturn(voltT24IdPairDto);

        when(t24AccountService.getTransactionsListForAnAccount(anyString(),any())).
            thenReturn(new ResponseEntity<>(mockTransactionListDtoWithValidData, HttpStatus.OK));

        when(t24AccountService.getAccounts(voltT24IdPairDto.getT24Id())).thenReturn(
            new ResponseEntity<>(
                CannedData.getMockTransactionAccountListDtoWithValidData(), HttpStatus.OK));


        Map<String,String> testMap = new HashMap<>();

        TransactionListDto transactions = transactionListService.getTransactions(voltT24IdPairDto.getT24Id(),
            CannedData.MOCK_ACCT_NUMBER_SAVER_1, testMap);

        TransactionInfoArrayDto transactionsData = transactions.getData();
        Assert.assertTrue(transactionsData.size()==7);

        Assert.assertTrue(transactionsData.get(0).getTransactionDate().equals("2019-06-01"));
        Assert.assertTrue(transactionsData.get(1).getTransactionDate().equals("2019-05-17"));
        Assert.assertTrue(transactionsData.get(2).getTransactionDate().equals("2018-05-30"));
        Assert.assertTrue(transactionsData.get(3).getTransactionDate().equals("2018-05-01"));
        Assert.assertTrue(transactionsData.get(4).getTransactionDate().equals("2017-12-01"));

        Assert.assertTrue(transactionsData.get(5).getTransactionDate().equals("2017-11-17"));
        Assert.assertTrue(transactionsData.get(5).getTransactionType().equals(
                TransactionInfoDto.TransactionTypeEnum._600));

        Assert.assertTrue(transactionsData.get(6).getTransactionDate().equals("2017-11-17"));
        Assert.assertTrue(transactionsData.get(6).getTransactionType().equals(
                TransactionInfoDto.TransactionTypeEnum._000));


        PagingInfoDto pagingInfo = transactions.getPagingInfo();
        Assert.assertTrue(pagingInfo.getPageSize().equals("50"));
        Assert.assertTrue(pagingInfo.getPageStart().equals("0"));
        Assert.assertTrue(pagingInfo.getTotalSize().equals("2"));
        Assert.assertTrue(pagingInfo.getPageToken().equals("d750782a-e998-47d8-ad6a-368153a45ab3"));
    }

    @Test
    public void shouldReturn000TranstypeNullAndInvalidTransactionCode()
            throws Exception {
        T24ResponseBody mockTransactionListDtoWithValidData =
                CannedData.getMockTransactionListWithNullAndInValidTransactionCode();

        VoltAuthIdPairDto voltAuthIdPairDto = CannedData.getVoltAuthIdPairDto();
        when(systemIdStoreService.getVoltId(anyString())).thenReturn(voltAuthIdPairDto);

        VoltT24IdPairDto voltT24IdPairDto = CannedData.getVoltT24IdPairDto();
        when(systemIdStoreService.getT24CustomerIdForVoltId(anyString())).thenReturn(voltT24IdPairDto);

        when(t24AccountService.getTransactionsListForAnAccount(anyString(),any())).
            thenReturn(new ResponseEntity<>(mockTransactionListDtoWithValidData, HttpStatus.OK));

        when(t24AccountService.getAccounts(voltT24IdPairDto.getT24Id())).thenReturn(
                new ResponseEntity<>(
                        CannedData.getMockTransactionAccountListDtoWithValidData(), HttpStatus.OK));


        Map<String,String> testMap = new HashMap<>();

        TransactionListDto transactions = transactionListService
            .getTransactions(voltT24IdPairDto.getT24Id(), CannedData.MOCKED_ACCT_NUMBER_SAVER, testMap);

        TransactionInfoDto transactionDto = transactions.getData().get(0);
        Assert.assertTrue(transactionDto.getBalance().equals("6507.99"));
        Assert.assertTrue(transactionDto.getBankAccountNumber().equals("100000341"));
        Assert.assertTrue(transactionDto.getDescription().equals("Interest - Capitalise"));
        Assert.assertTrue(transactionDto.getNarrative().equals("Credit Interest"));
        Assert.assertTrue(transactionDto.getTransactionAmount().equals("5.35"));
        Assert.assertTrue(transactionDto.getDebitCreditIndicator().toString().equals("Credit"));
        Assert.assertTrue(transactionDto.getTransactionDate().equals("2019-05-02"));
        Assert.assertTrue(transactionDto.getTransactionType().equals(
                TransactionInfoDto.TransactionTypeEnum._000));

        Assert.assertTrue(transactions.getData().size()==2);
        TransactionInfoDto transactionDto1 = transactions.getData().get(1);
        Assert.assertTrue(transactionDto1.getBalance().equals("6502.64"));
        Assert.assertTrue(transactionDto1.getBankAccountNumber().equals("100000341"));
        Assert.assertTrue(transactionDto1.getDescription().equals("Debit Arrangement"));
        Assert.assertTrue(transactionDto1.getNarrative().equals("Withholding Tax - Resident"));
        Assert.assertTrue(transactionDto1.getTransactionAmount().equals("2.51"));
        Assert.assertTrue(transactionDto1.getDebitCreditIndicator().toString().equals("Debit"));
        Assert.assertTrue(transactionDto1.getTransactionDate().equals("2019-05-01"));
        Assert.assertTrue(transactionDto1.getTransactionType().equals(
                TransactionInfoDto.TransactionTypeEnum._000));

        PagingInfoDto pagingInfo = transactions.getPagingInfo();
        Assert.assertTrue(pagingInfo.getPageSize().equals("50"));
        Assert.assertTrue(pagingInfo.getPageStart().equals("0"));
        Assert.assertTrue(pagingInfo.getTotalSize().equals("2"));
        Assert.assertTrue(pagingInfo.getPageToken().equals("d750782a-e998-47d8-ad6a-368153a45ab3"));
    }

    @Test
    public void shouldReturnEmptyListWhenAccountHasNoTransactions() throws Exception {
        T24ResponseBody mockTransactionListDtoWIthEmptyData =
                CannedData.getMockTransactionListDtoWIthEmptyData();

        VoltAuthIdPairDto voltAuthIdPairDto = CannedData.getVoltAuthIdPairDto();
        when(systemIdStoreService.getVoltId(anyString())).thenReturn(voltAuthIdPairDto);

        VoltT24IdPairDto voltT24IdPairDto = CannedData.getVoltT24IdPairDto();
        when(systemIdStoreService.getT24CustomerIdForVoltId(anyString())).thenReturn(voltT24IdPairDto);

        when(t24AccountService.getTransactionsListForAnAccount(anyString(),any())).
            thenReturn(new ResponseEntity<>(mockTransactionListDtoWIthEmptyData, HttpStatus.OK));

        when(t24AccountService.getAccounts(voltT24IdPairDto.getT24Id())).thenReturn(
                new ResponseEntity<>(
                        CannedData.getMockTransactionAccountListDtoWithValidData(), HttpStatus.OK));

        Map<String,String> testMap = new HashMap<>();
        testMap.put("hello","hello");
        testMap.put("fromDate","2019-03-01");
        testMap.put("toDate","2019-04-30");
        testMap.put("minAmount","356.4979239");
        testMap.put("maxAmount","3,37657.87");
        testMap.put("pageSize","23");
        testMap.put("pageStart","15");

        TransactionListDto transactions = transactionListService
                .getTransactions(voltT24IdPairDto.getT24Id(), CannedData.MOCK_ACCT_NUMBER_SAVER_1, testMap);

        Assert.assertTrue(transactions.getData().size()==0);

        Assert.assertTrue(transactions.getPagingInfo().getPageSize().equals("50"));
        Assert.assertTrue(transactions.getPagingInfo().getPageStart().equals("0"));
        Assert.assertTrue(transactions.getPagingInfo().getTotalSize().equals("2"));
        Assert.assertTrue(transactions.getPagingInfo().getPageToken()
                .equals("d750782a-e998-47d8-ad6a-368153a45ab3"));

        ArgumentCaptor<MultiValueMap<String,String>> captor =
                ArgumentCaptor.forClass(MultiValueMap.class);
        verify(t24AccountService).getTransactionsListForAnAccount(
                contains(CannedData.MOCK_ACCT_NUMBER_SAVER_1), captor.capture());

        MultiValueMap<String, String> value = captor.getValue();
        Assert.assertTrue(value.size()==7);
        Assert.assertTrue(value.get("fromDate").get(0).equals("20190301"));
        Assert.assertTrue(value.get("toDate").get(0).equals("20190430"));
        Assert.assertTrue(value.get("minimumTxnAmount").get(0).equals("356.4979239"));
        Assert.assertTrue(value.get("maximumTxnAmount").get(0).equals("3,37657.87"));
        Assert.assertTrue(value.get("page_start").get(0).equals("15"));
        Assert.assertTrue(value.get("page_size").get(0).equals("23"));
        Assert.assertTrue(value.get("hello").get(0).equals("hello"));
    }

    @Test
    public void shouldReturnEmptyResponseWhenT24ReturnsEmptyResponse()
        throws Exception {

        VoltAuthIdPairDto voltAuthIdPairDto = CannedData.getVoltAuthIdPairDto();
        when(systemIdStoreService.getVoltId(anyString())).thenReturn(voltAuthIdPairDto);

        VoltT24IdPairDto voltT24IdPairDto = CannedData.getVoltT24IdPairDto();
        when(systemIdStoreService.getT24CustomerIdForVoltId(anyString())).thenReturn(voltT24IdPairDto);

        when(t24AccountService.getTransactionsListForAnAccount(anyString(),any())).
                thenReturn(new ResponseEntity<>(new T24ResponseBody(),HttpStatus.OK));

        when(t24AccountService.getAccounts(voltT24IdPairDto.getT24Id())).thenReturn(
                new ResponseEntity<>(
                        CannedData.getMockTransactionAccountListDtoWithValidData(), HttpStatus.OK));

        TransactionListDto transactions = transactionListService.getTransactions(
                voltT24IdPairDto.getT24Id(), CannedData.MOCKED_ACCT_NUMBER_TRANS, new HashMap<>());
        Assert.assertTrue(transactions.getData().size()==0);
        Assert.assertNull(transactions.getPagingInfo().getPageSize());
        Assert.assertNull(transactions.getPagingInfo().getPageStart());
        Assert.assertNull(transactions.getPagingInfo().getTotalSize());
        Assert.assertNull(transactions.getPagingInfo().getPageToken());
    }

    @Test(expected = VoltCustomException.class)
    public void shouldThrowExceptionWhenEmptyTransactionDateAndUnknownDebitCreditIndicator()
				throws Exception {

        VoltAuthIdPairDto voltAuthIdPairDto = CannedData.getVoltAuthIdPairDto();
        when(systemIdStoreService.getVoltId(anyString())).thenReturn(voltAuthIdPairDto);

        VoltT24IdPairDto voltT24IdPairDto = CannedData.getVoltT24IdPairDto();
        when(systemIdStoreService.getT24CustomerIdForVoltId(anyString())).thenReturn(voltT24IdPairDto);

        when(t24AccountService.getTransactionsListForAnAccount(anyString(),any())).
            thenReturn(new ResponseEntity<>(
            CannedData.getMockTransactionListDtoWIthEmptyDateAndUnknowndebitCreditIndicator(),
            HttpStatus.OK));

        when(t24AccountService.getAccounts(voltT24IdPairDto.getT24Id())).thenReturn(
                new ResponseEntity<>(
                        CannedData.getMockTransactionAccountListDtoWithValidData(), HttpStatus.OK));

        transactionListService.getTransactions(
                voltT24IdPairDto.getT24Id(), CannedData.MOCK_ACCT_NUMBER_SAVER_1, new HashMap<>());
    }

    @Test(expected = VoltCustomException.class)
    public void shouldThrowExceptionWhenT24IdDoesNotOwnAcc()
            throws Exception {
			when(t24AccountService.getAccounts(any())).thenReturn(
					new ResponseEntity<>(CannedData.getMockAccountListwithEmptyBody(), HttpStatus.OK));

			transactionListService.getTransactions("abcd", "1234", new HashMap<>());
			verifyNoMoreInteractions(t24AccountService);
    }

    @Test(expected = VoltCustomException.class)
    public void shoudThrowExceptionWhenT24IdNotInSystemIdStore() throws T24IdException, TransactionAccountNotFoundException, VoltCustomException {
      VoltAuthIdPairDto voltAuthIdPairDto = CannedData.getVoltAuthIdPairDto();
      when(systemIdStoreService.getVoltId(anyString())).thenReturn(voltAuthIdPairDto);

      when(systemIdStoreService.getT24CustomerIdForVoltId(anyString())).thenThrow(new T24IdException("T24 ID Missing"));
      transactionListService.getTransactions(voltAuthIdPairDto.getVoltId(), "1234", new HashMap<>());
    }
}
