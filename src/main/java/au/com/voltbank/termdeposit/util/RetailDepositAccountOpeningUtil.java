package au.com.voltbank.termdeposit.util;

import au.com.voltbank.Constants;
import au.com.voltbank.T24Constants;
import au.com.voltbank.account.model.PagingInfoDto;
import au.com.voltbank.t24.baas.arrangementservice.model.QueryHeaderDto;
import au.com.voltbank.t24.T24ResponseBody;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static au.com.voltbank.Constants.ERROR_SYSTEM;

@Slf4j
public class RetailDepositAccountOpeningUtil {

  public static String convertTransactionListDateToVoltDate(String date)
      throws VoltCustomException {
    if (StringUtils.isEmpty(date)) {
      log.error(ERROR_SYSTEM, "The transaction posting/value date is null");
      throw new VoltCustomException(
          ERROR_SYSTEM,
          "The transaction posting/value date is null",
          HttpStatus.BAD_REQUEST.value());
    }
    DateTimeFormatter sdf =
        DateTimeFormatter.ofPattern(T24Constants.T24_TRANSACTIONS_LIST_DATE_FORMAT);
    LocalDate ld = LocalDate.parse(date, sdf);
    return ld.toString();
  }

  public static String convertAccountListDateToVoltDate(String date) throws VoltCustomException {
    if (StringUtils.isEmpty(date)) {
      log.error(ERROR_SYSTEM, "The accounts value date is null");
      throw new VoltCustomException(
          ERROR_SYSTEM, "The accounts value date is null", HttpStatus.BAD_REQUEST.value());
    }
    DateTimeFormatter sdf = DateTimeFormatter.ofPattern(T24Constants.T24_ACCOUNTS_LIST_DATE_FORMAT);
    LocalDate ld = LocalDate.parse(date, sdf);
    return ld.toString();
  }

  public static String convertContractDateToFinServOpenDate(String date) throws VoltCustomException {
    if (StringUtils.isEmpty(date)) {
      log.error(ERROR_SYSTEM, "The contract date is null");
      throw new VoltCustomException(
          ERROR_SYSTEM, "The contract value date is null", HttpStatus.BAD_REQUEST.value());
    }
    LocalDate localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern(
        T24Constants.T24_ACCOUNTS_LIST_DATE_FORMAT));

    return localDate.format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT));
  }
  public static PagingInfoDto getHeaderInfo(
      T24ResponseBody t24ResponseBody, MappingJackson2HttpMessageConverter converter) {

    QueryHeaderDto header =
        converter
            .getObjectMapper()
            .convertValue(t24ResponseBody.getHeader(), new TypeReference<QueryHeaderDto>() {});

    PagingInfoDto pagingInfoDto = new PagingInfoDto();
    if (header != null) {
      pagingInfoDto.setPageSize(
          header.getPageSize() != null ? String.valueOf(header.getPageSize()) : "0");
      pagingInfoDto.setPageStart(
          header.getPageStart() != null ? String.valueOf(header.getPageStart()) : "0");
      pagingInfoDto.setPageToken(header.getPageToken() != null ? header.getPageToken() : "");
      pagingInfoDto.setTotalSize(
          header.getTotalSize() != null ? String.valueOf(header.getTotalSize()) : "0");
    }

    return pagingInfoDto;
  }
}
