package au.com.voltbank.termdeposit.service;

import static org.apache.commons.lang3.StringUtils.join;

import au.com.voltbank.VoltApplicationConfig;
import au.com.voltbank.salesforce.arrangement.model.SuccessResponseDto;
import au.com.voltbank.termdeposit.dto.SalesforceUpdateDto;
import au.com.voltbank.termdeposit.entity.SavingsAccountApplication;
import au.com.voltbank.termdeposit.entity.TermDepositAccountApplication;
import au.com.voltbank.termdeposit.exception.SalesforceArragementCreationException;
import au.com.voltbank.termdeposit.exception.SalesforceFinancialAccountException;
import au.com.voltbank.termdeposit.util.RetailDepositAccountOpeningUtil;
import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SalesforceFinancialAccountService {


	private static final String RECORD_TYPE_ID = "0126F000001o1N7QAI";
	final String SF_FINANCIAL_ACCOUNT_ENDPOINT = "/services/data/v45.0/sobjects/FinServ_FinancialAccount_c";
	final String SF_FINANCIAL_ACCOUNT_ERROR = "Could not update Salesforce for account: %s";
	final String SF_ID_NOTFOUND = "Could not find SalesforceId for VoltId: %s";

	@Value("${SALESFORCE_HOST}")
	private String salesforceHost;

	@Value("${salesforce.customer.api.resource.id}")
	private String resourceId;
	
	@Autowired
	private RestTemplate secureRestTemplate;

	@Autowired
	private AzureActiveDirectoryService azureActiveDirectoryService;
	
	@Autowired
	private SalesforceUpdateDto salesforceDto;

	public void updateSalesforce() throws SalesforceFinancialAccountException {

		try {
			System.out.println("updatingsalesforce");
			salesforceDto.setAccountNumber("100000058");
			salesforceDto.setAccountStatus("Active");
			HttpEntity<SalesforceUpdateDto> request = new HttpEntity<>(salesforceDto, getHttpHeaders());
			 final String url=salesforceHost+SF_FINANCIAL_ACCOUNT_ENDPOINT;
			 secureRestTemplate.put(url, request);
			/*
			 * return secureRestTemplate.postForEntity(join(salesforceHost,
			 * SF_FINANCIAL_ACCOUNT_ENDPOINT), request, SuccessResponseDto.class).getBody();
			 */
			
		} catch (Exception e) {
			throw new SalesforceFinancialAccountException(String.format(SF_FINANCIAL_ACCOUNT_ERROR), e);
		}
	}

	private HttpHeaders getHttpHeaders() {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("Authorization", "Bearer " + azureActiveDirectoryService.getAccessToken(resourceId));
		return httpHeaders;
	}

}
