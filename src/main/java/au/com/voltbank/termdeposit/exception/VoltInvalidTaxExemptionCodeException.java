package au.com.voltbank.termdeposit.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@NoArgsConstructor
@Getter
@ToString
@Slf4j
public class VoltInvalidTaxExemptionCodeException extends Exception {

  private String errCode;

  private String message;

  public VoltInvalidTaxExemptionCodeException(String errCode, String message) {
    log.error(message);
    this.errCode = errCode;
    this.message = message;
  }
}
