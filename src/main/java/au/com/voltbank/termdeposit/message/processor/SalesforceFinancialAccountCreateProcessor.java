package au.com.voltbank.termdeposit.message.processor;


import au.com.voltbank.termdeposit.dao.SavingsAccountApplicationRepository;
import au.com.voltbank.termdeposit.entity.SavingsAccountApplication;
import au.com.voltbank.termdeposit.exception.SalesforceArragementCreationException;
import au.com.voltbank.termdeposit.exception.SavingsAccountNotFoundException;
import au.com.voltbank.termdeposit.service.SalesforceArrangementService;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.queue.CloudQueueMessage;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SalesforceFinancialAccountCreateProcessor {

  @Autowired
  private SavingsAccountApplicationRepository savingsAccountApplicationRepository;

  @Autowired
  private SalesforceArrangementService salesforceArrangementService;

  public void createFinancialAccountForSalesforceCustomer(Message in, Exchange exchange) throws StorageException,
      SavingsAccountNotFoundException, SalesforceArragementCreationException
  {
    CloudQueueMessage cloudQueueMessage = (CloudQueueMessage) in.getBody();
    if (cloudQueueMessage != null) {
      String voltId = cloudQueueMessage.getMessageContentAsString();
      Optional<SavingsAccountApplication> oneByVoltId = savingsAccountApplicationRepository.findOneByVoltId(voltId);

      if (oneByVoltId.isPresent()) {
        log.debug("Found SavingsAccountApplication: {}", voltId);
        salesforceArrangementService.createSavingsAccount(oneByVoltId.get());
      } else {
        log.error("SavingsAccountApplication: {} does not exist", voltId);
        throw new SavingsAccountNotFoundException("SavingsAccountApplication does not exist for volt id " + voltId);
      }
    }
  }
}
