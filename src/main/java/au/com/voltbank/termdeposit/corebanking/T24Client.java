package au.com.voltbank.termdeposit.corebanking;

import au.com.voltbank.Constants;
import au.com.voltbank.t24.T24RequestBody;
import au.com.voltbank.t24.T24ResponseBody;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.service.AzureActiveDirectoryService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Data
@Component
public class T24Client {

  private static final String ERROR_LOG_PREFIX = "T24 endpoint returns error: ";
  @Value("${Ocp.Apim.Subscription.Key}")
  private String t24SubscriptionKey;
  @Value("${t24.api.credentials}")
  private String t24Credentials;
  @Value("${t24.api.resource.id}")
  private String azureT24ResourceId;
  @Autowired private AzureActiveDirectoryService azureActiveDirectoryService;
  @Autowired private RestTemplate secureRestTemplate;


  public void initT24AccessCredential(String t24SubscriptionKey, String t24Credentials, String azureT24ResourceId) {
    this.t24SubscriptionKey=t24SubscriptionKey;
    this.t24Credentials=t24Credentials;
    this.azureT24ResourceId=azureT24ResourceId;
  }

  public ResponseEntity<T24ResponseBody> post(String url, HttpEntity<T24RequestBody> payload)
      throws VoltCustomException {
    ResponseEntity<T24ResponseBody> response;
    try {
      response = secureRestTemplate.postForEntity(url, payload, T24ResponseBody.class);
      return response;
    } catch (HttpClientErrorException e) {
      log.error(e.getMessage(), e);
      handleT24ErrorResponse(e);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new VoltCustomException(
          Constants.ERROR_T24_GENERAL_ERROR,
          ERROR_LOG_PREFIX + e.getMessage(),
          HttpStatus.INTERNAL_SERVER_ERROR.value());
    }
    return null;
  }

  public ResponseEntity<T24ResponseBody> put(
      String url, HttpEntity<T24RequestBody> payload, String pathParam) throws VoltCustomException {
    try {
      url = new StringBuilder(url).append("/").append(pathParam).toString();
      return secureRestTemplate.exchange(url, HttpMethod.PUT, payload, T24ResponseBody.class);
    } catch (HttpClientErrorException e) {
      log.error(e.getMessage(), e);
      handleT24ErrorResponse(e);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new VoltCustomException(
          Constants.ERROR_T24_GENERAL_ERROR,
          ERROR_LOG_PREFIX + e.getMessage(),
          HttpStatus.INTERNAL_SERVER_ERROR.value());
    }
    return null;
  }

  public ResponseEntity<T24ResponseBody> get(String url, String pathParam, HttpHeaders httpHeaders)
      throws VoltCustomException {
    HttpEntity<String> requestEntity = new HttpEntity<>(null, httpHeaders);
    ResponseEntity<T24ResponseBody> response;
    try {
      url = new StringBuilder(url).append("/").append(pathParam).toString();
      response =
          secureRestTemplate.exchange(url, HttpMethod.GET, requestEntity, T24ResponseBody.class);
      return response;
    } catch (HttpClientErrorException e) {
      log.error(e.getMessage(), e);
      handleT24ErrorResponse(e);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new VoltCustomException(
          Constants.ERROR_T24_GENERAL_ERROR,
          ERROR_LOG_PREFIX + e.getMessage(),
          HttpStatus.INTERNAL_SERVER_ERROR.value());
    }
    return null;
  }

  public ResponseEntity<T24ResponseBody> get(
      String url,
      String pathParam,
      HttpHeaders httpHeaders,
      MultiValueMap<String, String> requestParams)
      throws VoltCustomException {
    HttpEntity<String> requestEntity = new HttpEntity<>(null, httpHeaders);
    ResponseEntity<T24ResponseBody> response;
    try {
      url = new StringBuilder(url).append("/").append(pathParam).toString();

      UriComponents build =
          UriComponentsBuilder.fromHttpUrl(url).queryParams(requestParams).build();
      response =
          secureRestTemplate.exchange(
              build.toUriString(), HttpMethod.GET, requestEntity, T24ResponseBody.class);
      return response;
    } catch (HttpClientErrorException e) {
      log.error(e.getMessage(), e);
      handleT24ErrorResponse(e);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new VoltCustomException(
          Constants.ERROR_T24_GENERAL_ERROR,
          ERROR_LOG_PREFIX + e.getMessage(),
          HttpStatus.INTERNAL_SERVER_ERROR.value());
    }
    return null;
  }

  private void handleT24ErrorResponse(HttpClientErrorException exception)
      throws VoltCustomException {
    if (exception.getRawStatusCode() == 400) {
      throw new VoltCustomException(
          Constants.ERROR_T24_RESPONSE_BAD_REQUEST,
          exception.getResponseBodyAsString(),
          exception.getRawStatusCode());
    } else if (exception.getRawStatusCode() == 403) {
      throw new VoltCustomException(
          Constants.ERROR_T24_RESPONSE_FORBBIDEN_REQUEST,
          exception.getResponseBodyAsString(),
          exception.getRawStatusCode());
    } else if (exception.getRawStatusCode() == 404) {
      throw new VoltCustomException(
          Constants.ERROR_NOT_FOUND,
          exception.getResponseBodyAsString(),
          exception.getRawStatusCode());
    } else {
      throw new VoltCustomException(
          Constants.ERROR_T24_GENERAL_ERROR,
          exception.getResponseBodyAsString(),
          exception.getRawStatusCode());
    }
  }

  public HttpHeaders getHttpRequestHeader() {
  	String azureToken = azureActiveDirectoryService.getAccessToken(azureT24ResourceId);
  	log.debug("Azure token before calling t24 endpoint: " + azureToken);
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");
    httpHeaders.set("Ocp-Apim-Subscription-Key", t24SubscriptionKey);
    httpHeaders.set("Credentials", t24Credentials);
    httpHeaders.add(
        "Authorization",
        "Bearer " + azureToken);
    return httpHeaders;
  }
}
