package au.com.voltbank.termdeposit.service;

import au.com.voltbank.termdeposit.dto.Auth0RequestParam;
import com.auth0.client.auth.AuthAPI;
import com.auth0.client.mgmt.ManagementAPI;
import com.auth0.client.mgmt.filter.UserFilter;
import com.auth0.exception.APIException;
import com.auth0.exception.Auth0Exception;
import com.auth0.json.mgmt.users.User;
import com.auth0.net.AuthRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class Auth0Service {

  public static final String MOBILE_NUMBER_METADATA = "mobile_number";

  @Autowired private Auth0RequestParam auth0RequestParam;

  public Optional<User> getUserDetailFromAuth(String authId) throws Auth0Exception {
    ManagementAPI managementAPI =
        new ManagementAPI(auth0RequestParam.getIssuer(), getAccessToken());
    User user;
    try {
      user = managementAPI.users().get(authId, filterFields()).execute();
    } catch (APIException auth0Exception) {
      if (HttpStatus.NOT_FOUND.value() == auth0Exception.getStatusCode()) {
        return Optional.empty();
      } else {
        throw auth0Exception;
      }
    }
    return Optional.of(user);
  }

  public User addUserAppMetadata(String authId, Map<String, Object> appMetadata)
      throws Auth0Exception {
    ManagementAPI managementAPI =
        new ManagementAPI(auth0RequestParam.getIssuer(), getAccessToken());
    User user = new User();
    user.setAppMetadata(appMetadata);
    return managementAPI.users().update(authId, user).execute();
  }

  private UserFilter filterFields() {
    UserFilter userFilter = new UserFilter();
    userFilter.withFields("email,user_metadata,app_metadata", true);
    return userFilter;
  }

  public String getAccessToken() throws Auth0Exception {
    AuthAPI authAPI =
        new AuthAPI(
            auth0RequestParam.getIssuer(),
            auth0RequestParam.getClientId(),
            auth0RequestParam.getClientSecret());
    AuthRequest authRequest = authAPI.requestToken(auth0RequestParam.getAudience());
    return authRequest.execute().getAccessToken();
  }
}
