package au.com.voltbank.termdeposit.exception;

public class UserInfoNotFoundException extends Exception {

  public UserInfoNotFoundException(String authId) {
    super(String.format("User not found for authId: %s", authId));
  }
}
