package au.com.voltbank.termdeposit.exception;

public class CloudQueueMessageTransformationException extends RuntimeException {

  public CloudQueueMessageTransformationException(String message, Throwable cause) {
    super(message, cause);
  }
}
