package au.com.voltbank.termdeposit.controller;

import au.com.voltbank.account.model.TransactionAccountApplicationDataDto;
import au.com.voltbank.sis.model.VoltAuthIdPairDto;
import au.com.voltbank.t24.account.AccountDetailBodyDto;
import au.com.voltbank.termdeposit.entity.TransactionAccountApplication;
import au.com.voltbank.termdeposit.exception.TransactionAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.service.SystemIdStoreService;
import au.com.voltbank.termdeposit.service.TransactionAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static au.com.voltbank.termdeposit.mapper.AccountMapper.mapToTransactionAccountResultDto;

@RestController
public class TransactionAccountController {

  @Autowired private TransactionAccountService transactionAccountService;

  @Autowired private SystemIdStoreService systemIdStoreService;

  @PostMapping("/transaction/account")
  public ResponseEntity createAccount(
      @Validated @RequestBody TransactionAccountApplicationDataDto accountApplication,
      Authentication authentication)
      throws VoltCustomException, TransactionAccountNotFoundException {
    VoltAuthIdPairDto voltAuthIdPairDto = systemIdStoreService.getVoltId(authentication.getName());
    TransactionAccountApplication result =
        transactionAccountService.create(accountApplication, voltAuthIdPairDto.getVoltId());
    AccountDetailBodyDto accountDetail = transactionAccountService.getAccountDetails(result);
    return ResponseEntity.status(HttpStatus.CREATED)
        .body(mapToTransactionAccountResultDto(accountDetail));
  }
}
