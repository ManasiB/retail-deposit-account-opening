package au.com.voltbank.termdeposit.controller;

import au.com.voltbank.termdeposit.dto.ErrorResponse;
import au.com.voltbank.termdeposit.exception.SavingsAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.T24IdException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.exception.VoltIdException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static au.com.voltbank.Constants.ERROR_SAVINGS_ACCOUNT_NOT_FOUND;
import static au.com.voltbank.Constants.ERROR_SYSTEM;
import static au.com.voltbank.Constants.ERROR_VOLT_ID_NOT_FOUND;

@Slf4j
@ControllerAdvice(assignableTypes = SavingsAccountController.class)
public class SavingsAccountControllerAdvice {

  @ExceptionHandler
  public ResponseEntity<ErrorResponse> handle(SavingsAccountNotFoundException e) {
    log.error("Error occurred fetching Savings Account.", e);
    return ResponseEntity.status(HttpStatus.NOT_FOUND)
        .body(new ErrorResponse(ERROR_SAVINGS_ACCOUNT_NOT_FOUND));
  }

  @ExceptionHandler
  public ResponseEntity<ErrorResponse> handle(VoltIdException e) {
    log.error(e.getMessage(), e);
    return ResponseEntity.badRequest().body(new ErrorResponse(ERROR_VOLT_ID_NOT_FOUND));
  }

  @ExceptionHandler
  public ResponseEntity<ErrorResponse> handle(MethodArgumentNotValidException e) {
    log.error(e.getMessage(), e);
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(ERROR_SYSTEM));
  }

  @ExceptionHandler
  public ResponseEntity<ErrorResponse> handle(VoltCustomException e) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(e.getErrCode()));
  }

  @ExceptionHandler
  public ResponseEntity<ErrorResponse> handle(Exception e) {
    log.error(e.getMessage(), e);
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .body(new ErrorResponse(ERROR_SYSTEM));
  }

  @ExceptionHandler
  public ResponseEntity<ErrorResponse> handle(T24IdException e) {
    log.error(e.getMessage(), e);
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(ERROR_SYSTEM));
  }
}
