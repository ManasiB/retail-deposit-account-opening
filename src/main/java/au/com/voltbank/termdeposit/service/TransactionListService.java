package au.com.voltbank.termdeposit.service;

import au.com.voltbank.Constants;
import au.com.voltbank.T24Constants;
import au.com.voltbank.account.model.TransactionInfoArrayDto;
import au.com.voltbank.account.model.TransactionInfoDto;
import au.com.voltbank.account.model.TransactionListDto;
import au.com.voltbank.t24.T24ResponseBody;
import au.com.voltbank.t24.account.AccountDetailBodyDto;
import au.com.voltbank.t24.baas.arrangementservice.model.GetTransactionListResponseBodyDto;
import au.com.voltbank.t24.baas.arrangementservice.model.GetTransactionListResponseBodyInnerDto;
import au.com.voltbank.t24.util.T24AccountUtil;
import au.com.voltbank.termdeposit.corebanking.service.T24AccountService;
import au.com.voltbank.termdeposit.exception.T24IdException;
import au.com.voltbank.termdeposit.exception.TransactionAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import au.com.voltbank.termdeposit.util.RetailDepositAccountOpeningUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TransactionListService {

  @Autowired private T24AccountService t24AccountService;

  @Autowired private MappingJackson2HttpMessageConverter converter;

  @Autowired private SystemIdStoreService systemIdStoreService;

  private static DateTimeFormatter sdf =
  DateTimeFormatter.ofPattern(T24Constants.T24_TRANSACTIONS_LIST_REQUEST_PARAM_DATE_FORMAT);

  private static final String FAILED_VERIFY_ACC_T24ID_TMP = "Failed to verify acc:t24id - %s:%s";

  private static final String FAILED_GET_T24_CUSTOMER_ID_FROM_SYS_STORE =
      "Failed to get t24 customer Id from systemIdStore service. - authId:%s";

  public TransactionListDto getTransactions(String authId, String accountNumber,
                                            Map<String, String> requestParams)
          throws VoltCustomException, TransactionAccountNotFoundException{

    String t24Id = getT24CustomerIdFromSysStore(authId);
    verifyAccountOwner(t24Id, accountNumber);

    MultiValueMap<String, String> newRequestParams = transformRequest(requestParams);
    log.debug(
        "Request parameters to get transaction list transformed to match t24 params {}",
        newRequestParams);
    // Transform to volt response object
    ResponseEntity<T24ResponseBody> transactionsListForAnAccount =
        t24AccountService.getTransactionsListForAnAccount(accountNumber, newRequestParams);
    T24ResponseBody t24ResponseBody = transactionsListForAnAccount.getBody();
    log.debug("Successfully got the response from T24 for list of transaction");

    TransactionListDto transactionListDto = new TransactionListDto();
    transactionListDto.setData(getTransactionInfoArrayDto(t24ResponseBody));
    transactionListDto.setPagingInfo(
        RetailDepositAccountOpeningUtil.getHeaderInfo(t24ResponseBody, converter));
    log.debug(
        "Successfully transformed the response header (paging info) from t24 to volt response for transactions list");

    return transactionListDto;
  }

  private String convertToVoltAmount(String amount) {
    if (amount != null) {
      String[] split = amount.split(",");
      return String.join("", split);
    }
    return amount;
  }

  private String convertVoltDateToT24DefaultDate(String voltDateString) {

    if (StringUtils.isEmpty(voltDateString)) {
      return null;
    }
    LocalDate voltDate = LocalDate.parse(voltDateString);
    return sdf.format(voltDate);
  }

  private MultiValueMap<String, String> transformRequest(Map<String, String> requestParams) {
    final MultiValueMap<String, String> newRequestParams = new LinkedMultiValueMap<>();
    for (String key : requestParams.keySet()) {
      if (key.equals(T24Constants.T24_FROMDATE_REQUEST_PARAM)
          || key.equals(T24Constants.T24_TODATE_REQUEST_PARAM)) {
        newRequestParams.put(
            key, Arrays.asList(convertVoltDateToT24DefaultDate(requestParams.get(key))));
      } else if (key.equals(T24Constants.VOLT_MINTXNAMOUNT_REQUEST_PARAM)) {
        newRequestParams.put(
            T24Constants.T24_MINTXNAMOUNT_REQUEST_PARAM, Arrays.asList(requestParams.get(key)));
      } else if (key.equals(T24Constants.VOLT_MAXTAXAMOUNT_REQUEST_PARAM)) {
        newRequestParams.put(
            T24Constants.T24_MAXTAXAMOUNT_REQUEST_PARAM, Arrays.asList(requestParams.get(key)));
      } else if (key.equals(T24Constants.VOLT_PAGESIZE_REQUEST_PARAM)) {
        newRequestParams.put(
            T24Constants.T24_PAGESIZE_REQUEST_PARAM, Arrays.asList(requestParams.get(key)));
      } else if (key.equals(T24Constants.VOLT_PAGESTART_REQUEST_PARAM)) {
        newRequestParams.put(
            T24Constants.T24_PAGESTART_REQUEST_PARAM, Arrays.asList(requestParams.get(key)));
      } else {
        newRequestParams.put(key, Arrays.asList(requestParams.get(key)));
      }
    }
    return newRequestParams;
  }

  private TransactionInfoArrayDto getTransactionInfoArrayDto(T24ResponseBody t24ResponseBody)
      throws VoltCustomException {

    GetTransactionListResponseBodyDto responseTransactionList =
        converter
            .getObjectMapper()
            .convertValue(
                t24ResponseBody.getBody(),
                new TypeReference<GetTransactionListResponseBodyDto>() {});

    TransactionInfoArrayDto transactionInfoDtosArray = new TransactionInfoArrayDto();
    if (!CollectionUtils.isEmpty(responseTransactionList)) {

      List<TransactionInfoDto> transactionInfoDtosList = new ArrayList<>();
      for (GetTransactionListResponseBodyInnerDto responseTransaction : responseTransactionList) {
        TransactionInfoDto transactionInfoDto = new TransactionInfoDto();
        transactionInfoDto.setBalance(convertToVoltAmount(responseTransaction.getBalance()));
        transactionInfoDto.setBankAccountNumber(responseTransaction.getBankAccountNumber());
        transactionInfoDto.setDebitCreditIndicator(
            TransactionInfoDto.DebitCreditIndicatorEnum.fromValue(
                responseTransaction.getDebitCreditIndicator()));
        transactionInfoDto.setDescription(responseTransaction.getTransactDescription());
        transactionInfoDto.setNarrative(responseTransaction.getTransactionNarative());
        transactionInfoDto.setTransactionAmount(
            convertToVoltAmount(responseTransaction.getTransactionAmount()));
        transactionInfoDto.setTransactionDate(
            RetailDepositAccountOpeningUtil.convertTransactionListDateToVoltDate(
                responseTransaction.getPostingDate()));
        String voltTransactionType =
            T24Constants.transactionCodeToTransactionTypeMap.get(
                responseTransaction.getTransactionCode());
        if (voltTransactionType != null) {
          transactionInfoDto.setTransactionType(
              TransactionInfoDto.TransactionTypeEnum.fromValue(voltTransactionType));
        } else {
          transactionInfoDto.setTransactionType(TransactionInfoDto.TransactionTypeEnum._000);
        }
        transactionInfoDtosList.add(transactionInfoDto);
      }

      List<TransactionInfoDto> collect =
          transactionInfoDtosList.stream()
              .sorted(
                  Comparator.comparing(
                      TransactionInfoDto::getTransactionDate, Comparator.reverseOrder()))
              .collect(Collectors.toList());
      transactionInfoDtosArray.addAll(collect);
    }
    log.debug(
        "Successfully transformed the response body from t24 to volt response for transaction list");
    return transactionInfoDtosArray;
  }

  private void verifyAccountOwner(String t24CustomerId, String sourceAccountNumber)
          throws VoltCustomException {
    boolean isRightfulAccOwner;
    try {
      ResponseEntity<T24ResponseBody> t24Response =
              t24AccountService.getAccounts(t24CustomerId);
      List<AccountDetailBodyDto> accountDetailList = converter.getObjectMapper()
              .convertValue(t24Response.getBody().getBody(),
                      new TypeReference<List<AccountDetailBodyDto>>() {});
      isRightfulAccOwner =
              T24AccountUtil.isAccountLinkedToT24Customer(sourceAccountNumber,
                      accountDetailList);
    } catch (Exception e) {
      String error = String.format(FAILED_VERIFY_ACC_T24ID_TMP, sourceAccountNumber,
              t24CustomerId);
      log.error(error, e);
      throw new VoltCustomException(Constants.ERROR_SYSTEM,
              error, HttpStatus.BAD_REQUEST.value());
    }
    if (!isRightfulAccOwner) {
      throw new VoltCustomException(Constants.ERROR_INCORRECT_ACCOUNT_OWNER,
              String.format(FAILED_VERIFY_ACC_T24ID_TMP, sourceAccountNumber,
                      t24CustomerId), HttpStatus.BAD_REQUEST.value());
    }
  }

  private String getT24CustomerIdFromSysStore(String authId) throws VoltCustomException {
    try {
      String voltId = systemIdStoreService.getVoltId(authId).getVoltId();
      return systemIdStoreService.getT24CustomerIdForVoltId(voltId).getT24Id();
    } catch (Exception e) {
      throw new VoltCustomException(Constants.ERROR_TRANSACTIONS_LISTING_ERROR, String.format(FAILED_GET_T24_CUSTOMER_ID_FROM_SYS_STORE, authId));
    }
  }
}
