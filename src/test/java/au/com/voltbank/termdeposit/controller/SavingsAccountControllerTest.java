package au.com.voltbank.termdeposit.controller;

import au.com.voltbank.account.model.SavingsAccountApplicationDataDto;
import au.com.voltbank.account.model.SavingsAccountApplicationResultDto;
import au.com.voltbank.sis.model.VoltAuthIdPairDto;
import au.com.voltbank.t24.account.AccountDetailBodyDto;
import au.com.voltbank.termdeposit.dto.ErrorResponse;
import au.com.voltbank.termdeposit.entity.SavingsAccountApplication;
import au.com.voltbank.termdeposit.exception.SavingsAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.T24IdException;
import au.com.voltbank.termdeposit.exception.VoltIdException;
import au.com.voltbank.termdeposit.service.SavingsAccountService;
import au.com.voltbank.termdeposit.service.SystemIdStoreService;
import au.com.voltbank.termdeposit.util.CannedData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tyro.oss.randomdata.RandomString;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.security.Principal;

import static au.com.voltbank.Constants.ERROR_SAVINGS_ACCOUNT_NOT_FOUND;
import static au.com.voltbank.Constants.ERROR_SYSTEM;
import static au.com.voltbank.Constants.ERROR_VOLT_ID_NOT_FOUND;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = SavingsAccountController.class, secure = false)
public class SavingsAccountControllerTest {

  @Autowired private MockMvc mockMvc;

  @Autowired private ObjectMapper objectMapper;

  @MockBean private SavingsAccountService savingsAccountService;

  @MockBean private SystemIdStoreService systemIdStoreService;

  @After
  public void reset() {
    Mockito.reset(savingsAccountService, systemIdStoreService);
  }

  @Test
  public void shouldReturn_201_IfNoErrorsOccur_ForCreateAccount() throws Exception {
    VoltAuthIdPairDto voltAuthIdPairDto = getVoltAuthIdPairDto();
    Principal principal = voltAuthIdPairDto::getAuthId;
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);

    SavingsAccountApplicationDataDto applicationDataDto =
        CannedData.getSavingsAccountApplicationDataDto();
    AccountDetailBodyDto accountDetailBodyDto = CannedData.getAccountDetailsBodyDto().get(0);
    SavingsAccountApplication result = CannedData.getSavingsAccountApplication();

    when(systemIdStoreService.getVoltId(voltAuthIdPairDto.getAuthId()))
        .thenReturn(voltAuthIdPairDto);
    when(savingsAccountService.create(any(SavingsAccountApplicationDataDto.class), anyString()))
        .thenReturn(result);
    when(savingsAccountService.getSavingsAccountDetails(result)).thenReturn(accountDetailBodyDto);

    String response =
        this.mockMvc
            .perform(
                post("/savings/account")
                    .principal(jwt)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(toJson(applicationDataDto)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString();

    verify(savingsAccountService).create(applicationDataDto, voltAuthIdPairDto.getVoltId());
    verify(savingsAccountService).getSavingsAccountDetails(any());

    SavingsAccountApplicationResultDto resultDto =
        fromJson(response, SavingsAccountApplicationResultDto.class);
    assertThat(resultDto.getAccountBSB(), is("517000"));
    assertThat(resultDto.getAccountNumber(), is("100007999"));
    assertThat(resultDto.getAccountName(), is("Saver Account Individual"));
    assertThat(resultDto.getPrincipalBalanceInWholeDollars(), is(0));
    assertThat(resultDto.getInterestRate(), is("2.5"));
  }

  @Test
  public void shouldReturn_404_IfCannotFindSavingsAccount_AfterCreateAccount() throws Exception {
    VoltAuthIdPairDto voltAuthIdPairDto = getVoltAuthIdPairDto();
    when(systemIdStoreService.getVoltId(voltAuthIdPairDto.getAuthId()))
        .thenReturn(voltAuthIdPairDto);

    Principal principal = voltAuthIdPairDto::getAuthId;
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);

    SavingsAccountApplicationDataDto applicationDataDto =
        CannedData.getSavingsAccountApplicationDataDto();
    SavingsAccountApplication expectedAccount =
        new SavingsAccountApplication()
            .toBuilder()
            .arrangementId(RandomString.randomNumericString())
            .build();
    when(savingsAccountService.create(applicationDataDto, voltAuthIdPairDto.getVoltId()))
        .thenReturn(expectedAccount);

    when(savingsAccountService.getSavingsAccountDetails(any()))
        .thenThrow(new SavingsAccountNotFoundException("WTF?!!"));

    this.mockMvc
        .perform(
            post("/savings/account")
                .principal(jwt)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(applicationDataDto))
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound())
        .andExpect(content().json(toJson(new ErrorResponse(ERROR_SAVINGS_ACCOUNT_NOT_FOUND))));
  }

  @Test
  public void shouldReturn_500_IfNoneJsonFormattedDataPassedIn_ForCreateAccountApplication()
      throws Exception {
    VoltAuthIdPairDto voltAuthIdPairDto = getVoltAuthIdPairDto();
    Principal principal = voltAuthIdPairDto::getAuthId;
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);

    this.mockMvc
        .perform(
            post("/savings/account").principal(jwt).contentType(APPLICATION_JSON).content("xxxx"))
        .andDo(print())
        .andExpect(status().isInternalServerError());

    verifyZeroInteractions(savingsAccountService);
  }

  @Test
  public void shouldReturn_400_If_voltId_not_exist() throws Exception {
    String authId = RandomString.randomNumericString();
    SavingsAccountApplicationDataDto applicationDataDto =
        CannedData.getSavingsAccountApplicationDataDto();
    when(systemIdStoreService.getVoltId(authId))
        .thenThrow(new VoltIdException("Did not find voltId by authId"));
    Principal principal = () -> authId;
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);
    this.mockMvc
        .perform(
            post("/savings/account")
                .principal(jwt)
                .contentType(APPLICATION_JSON)
                .content(toJson(applicationDataDto)))
        .andDo(print())
        .andExpect(status().isBadRequest())
        .andExpect(content().json(toJson(new ErrorResponse(ERROR_VOLT_ID_NOT_FOUND))));

		verifyZeroInteractions(savingsAccountService);
  }

  @Test
  public void shouldReturn_400_If_t24_id_exception() throws Exception {
    VoltAuthIdPairDto voltAuthIdPairDto = getVoltAuthIdPairDto();
    Principal principal = voltAuthIdPairDto::getAuthId;
    Authentication jwt = new PreAuthenticatedAuthenticationToken(principal, null);
    SavingsAccountApplicationDataDto applicationDataDto =
        CannedData.getSavingsAccountApplicationDataDto();
    SavingsAccountApplication expectedAccount =
        new SavingsAccountApplication()
            .toBuilder()
            .arrangementId(RandomString.randomNumericString())
            .build();
    when(savingsAccountService.create(any(), any())).thenReturn(expectedAccount);
    when(systemIdStoreService.getVoltId(voltAuthIdPairDto.getAuthId()))
        .thenReturn(voltAuthIdPairDto);
    when(savingsAccountService.getSavingsAccountDetails(any()))
        .thenThrow(new T24IdException("T24 BLOWS UP!"));
    this.mockMvc
        .perform(
            post("/savings/account")
                .principal(jwt)
                .contentType(APPLICATION_JSON)
                .content(toJson(applicationDataDto)))
        .andDo(print())
        .andExpect(status().isBadRequest())
        .andExpect(content().json(toJson(new ErrorResponse(ERROR_SYSTEM))));
  }

  private String toJson(Object toBeSerialized) throws JsonProcessingException {
    return objectMapper.writeValueAsString(toBeSerialized);
  }

  private <T> T fromJson(String json, Class<T> clazz) throws IOException {
    return objectMapper.readValue(json, clazz);
  }

  private VoltAuthIdPairDto getVoltAuthIdPairDto() {
    String authId = RandomString.randomNumericString(8);
    String voltId = RandomString.randomNumericString(8);

    VoltAuthIdPairDto voltAuthIdPairDto = new VoltAuthIdPairDto();
    voltAuthIdPairDto.setAuthId(authId);
    voltAuthIdPairDto.setVoltId(voltId);
    return voltAuthIdPairDto;
  }
}
