package au.com.voltbank.termdeposit.exception;

public class TransactionAccountNotFoundException extends Exception {

  public TransactionAccountNotFoundException(String message) {
    super(message);
  }
}
