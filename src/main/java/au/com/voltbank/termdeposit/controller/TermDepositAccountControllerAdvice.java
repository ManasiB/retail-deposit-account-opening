package au.com.voltbank.termdeposit.controller;

import au.com.voltbank.termdeposit.dto.ErrorResponse;
import au.com.voltbank.termdeposit.exception.TermDepositAccountNotFoundException;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static au.com.voltbank.Constants.ERROR_CUSTOMER_NOT_FOUND;
import static au.com.voltbank.Constants.ERROR_SYSTEM;
import static au.com.voltbank.Constants.ERROR_TERM_DEPOSIT_ACCOUNT_NOT_FOUND;

@Slf4j
@ControllerAdvice(assignableTypes = TermDepositAccountController.class)
public class TermDepositAccountControllerAdvice {

  @ExceptionHandler
  public ResponseEntity<ErrorResponse> handle(TermDepositAccountNotFoundException e) {
    log.error("Error occurred fetching Term Deposit Account.", e);
    return ResponseEntity.status(HttpStatus.NOT_FOUND)
        .body(new ErrorResponse(ERROR_TERM_DEPOSIT_ACCOUNT_NOT_FOUND));
  }

  @ExceptionHandler
  public ResponseEntity<ErrorResponse> handle(VoltCustomException e) {
    return ResponseEntity.badRequest().body(new ErrorResponse(ERROR_CUSTOMER_NOT_FOUND));
  }

  @ExceptionHandler
  public ResponseEntity<ErrorResponse> handle(RuntimeException e) {
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .body(new ErrorResponse(ERROR_SYSTEM));
  }
}
