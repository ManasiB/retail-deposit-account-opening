package au.com.voltbank.termdeposit.config;

import au.com.voltbank.VoltApplicationConfig;
import au.com.voltbank.termdeposit.dto.Auth0RequestParam;
import au.com.voltbank.termdeposit.dto.AzureTokenRequestParam;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
/* @Profile(value = {"local", "build", "dev", "sit", "staging", "prod"}) */

@Configuration
public class ApplicationConfig extends VoltApplicationConfig {

  @Value("${ret.dep.acc.open.spring.datasource.url}")
  private String dbUrl;

  @Value("${ret.dep.acc.open.spring.datasource.username}")
  private String dbUsername;

  @Value("${ret.dep.acc.open.spring.datasource.password}")
  private String dbPassword;

  @Value("${ret.dep.acc.open.azure.oauth.client.id}")
  private String aadClientId;

  @Value("${ret.dep.acc.open.azure.oauth.client.secret}")
  private String aadKey;

  @Value("${Ocp.Apim.Subscription.Key}")
  private String t24SubscriptionKey;

  @Value("${auth0.m2m.api.issuer}")
  private String auth0Issuer;

  @Value("${auth0.m2m.api.audience}")
  private String auth0Audience;

  @Value("${auth0.m2m.client.id}")
  private String auth0ClientId;

  @Value("${auth0.m2m.client.secret}")
  private String auth0ClientSecret;

  @Value("${spring.datasource.hikari.minimum-idle}")
  private Integer minimumIdle;

  @Value("${spring.datasource.hikari.maximum-pool-size}")
  private Integer maxPoolSize;

  
  

  public HikariDataSource instantiateDataSource(
      String dbUrl, String dbUsername, String dbPassword) {
    HikariConfig config = new HikariConfig();
    config.setJdbcUrl(dbUrl);
    config.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
    config.setUsername(dbUsername);
    config.setPassword(dbPassword);
    config.setMinimumIdle(minimumIdle);
    config.setMaximumPoolSize(maxPoolSize);
    return new HikariDataSource(config);
  }

  @Bean
  public HikariDataSource instantiateDataSource() {
    return this.instantiateDataSource(dbUrl, dbUsername, dbPassword);
  }

  @Bean
  public AzureTokenRequestParam initAzureTokenRequestParam() {
    return new AzureTokenRequestParam("client_credentials", aadClientId, aadKey);
  }

  @Bean
  public Auth0RequestParam initAuth0RequestParam() {
    return new Auth0RequestParam(auth0Issuer, auth0Audience, auth0ClientId, auth0ClientSecret);
  }
  

}
