package au.com.voltbank.termdeposit.controller;

import au.com.voltbank.termdeposit.dto.ErrorResponse;
import au.com.voltbank.termdeposit.exception.VoltCustomException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static au.com.voltbank.Constants.ERROR_SYSTEM;

@ControllerAdvice(assignableTypes = RetailTermDepositRatesController.class)
public class RetailTermDepositRatesControllerAdvice {

  @ExceptionHandler
  public ResponseEntity<ErrorResponse> handle(VoltCustomException e) {
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .body(new ErrorResponse(ERROR_SYSTEM));
  }
}
