package au.com.voltbank.termdeposit.config;

public enum VoltTaxExemptionCode {
  AGE_SERVICE_INVALID_PENSION_GROUP("001"),
  WIDOW_CARER_SOLEPARENT_PENSION_GROUP("002");
  private final String code;

  VoltTaxExemptionCode(String code) {
    this.code = code;
  }

  public String getCode() {
    return this.code;
  }
}
