package au.com.voltbank.termdeposit.dao;

import au.com.voltbank.termdeposit.entity.RetailTermDepositRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/** Spring Data JPA repository for the RetailTermDepositRate entity. */
@Repository
public interface RetailTermDepositRatesRepository
    extends JpaRepository<RetailTermDepositRate, Long> {

  @Query(
      value =
          "select * from RetailTermDepositRate r where r.effectiveDateTime = (select max(rsub.effectiveDateTime) from RetailTermDepositRate rsub where rsub.effectiveDateTime <=  :currentDateTime) order by r.effectiveDateTime desc",
      nativeQuery = true)
  List<RetailTermDepositRate> findLatestRates(@Param("currentDateTime") Date currentDateTime);

  @Query(
      value =
          "select * from RetailTermDepositRate r where r.termInMonths=:termInMonths and r.indexCode = :indexCode and r.effectiveDateTime <= :currentDateTime order by r.effectiveDateTime desc",
      nativeQuery = true)
  List<RetailTermDepositRate> findTargetRateByTermAndIndexCode(
      @Param("termInMonths") int termInMonths,
      @Param("indexCode") String indexCode,
      @Param("currentDateTime") Date currentDateTime);
}
